import asyncio
import json
import logging
import pathlib
import ssl

import websockets
import datetime

active_connections = {}


async def echo(websocket: websockets.WebSocketServerProtocol):
    async for message in websocket:
        await websocket.send(message)
        try:
            json_msg = json.loads(message)
        except Exception as exc:
            print(f"{datetime.datetime.now()} - DBG>>> Exception: {exc}")
            print(
                f"{datetime.datetime.now()} - DBG>>> ID: {websocket.id} websocket.remote_address: "
                f"{websocket.remote_address} - X-Forwarded-For: {websocket.request_headers.get('X-Forwarded-For')}"
            )
        else:
            print(
                f"{datetime.datetime.now()} - JSON message: {json_msg} --- ID: {websocket.id}"
            )
            if websocket.id not in active_connections:
                print(f"Adding {websocket.id} to active connections")
                active_connections[websocket.id] = websocket

            # await parse_ws_msg(json_msg, websocket)


async def parse_ws_msg(msg, ws: websockets.WebSocketServerProtocol):
    # {'event': 'auth', 'data': {'user': 'zmninja', 'password': 'p%6Rmpuz#Q!5DeVthdsD62inXTQdD&ef362D$weW7rRnn',
    # 'monlist': '1,4,6,2', 'intlist': '0,0,0,0', 'appversion': '1.6.009'},
    # 'token': 'eYwBPCXCRUOdwvMOIwyxBe:APA91bGKGxI82x-IB2lxDUlskCx9hiNxFINsDrd2Z7JPU4CFtCDAbWylHBlRPcF-nbhJ37nUtfugXsk
    # yOu9DmqhaS69LlbiuN98ECqL43nnZTZrUUsFAeAhNNt0TWPBSPEfig1FVIrOH'}
    category = msg.get("category", "normal")
    if msg.get("event"):
        msg_event = msg["event"]
        msg_data = msg.get("data")
        msg_token = msg.get("token")
        if msg_event == "auth":
            username = msg_data.get("user")
            password = msg_data.get("password")
            app_version = msg_data.get("appversion")
            monlist = msg_data.get("monlist", "")
            intlist = msg_data.get("intlist", "")
            # if ws.id in active_connections:


ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
fullchain_pem = pathlib.Path(__file__).with_name("fullchain.pem")
ssl_context.load_cert_chain(fullchain_pem)
print(f"{ssl_context.options = }")

async def main():
    ws_address = "0.0.0.0"
    ws_port = 8765
    print(
        f"{datetime.datetime.now()} - Starting websocket server on {ws_address}:{ws_port}"
    )
    async with websockets.serve(echo, ws_address, ws_port, ssl=ssl_context):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    print(f"{datetime.datetime.now()} - This is the beginning of main()")
    asyncio.run(main())
