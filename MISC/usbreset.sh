#!/bin/bash
# By default this script will reset each and every USB Controller it has access too.
# One way I solved this issue is to run mlapi in an LXC and only passthrough the TPU on a seperate USB 3.1 card.
# I only have 1 USB 3.x device in my system and XHCI is for USB 3.x+ So if I only reset the XHCI driver inside
# the LXC and the only thing attached to that USB 3.1 Controller is the TPU, Thats as safe as it can get. Otherwise we
# need to find a way to figure out which <x>hci device the TPU belongs too and only reset that USB controller.
# There may be other devices attached to that USB Controller though and this script will not wait for a USB drive to
# finish writing data and unmount, it will just unbind it in the middle of writing
# So, in short, this script can be dangerous if you dont understand what it is doing. the /sys/bus/pci/drivers/?hci_hcd
# line means it will look in the uhci ehci and xhci directories, you can change that to specifically only be xhci or uhci or ehci
# to go even further if you use lsusb and lspci to figure out what is what you could just modify this script to point to
# the proper <x>hci dir and then unbind and bind a specific pci address instead of all of them
if [[ $EUID != 0 ]] ; then
  echo This must be run as root!
  exit 1
fi

[ "$1" ] && SPCIADDY="$1"

for xhci in /sys/bus/pci/drivers/?hci_hcd ; do

  if ! cd $xhci ; then
    echo Weird error. Failed to change directory to $xhci
    exit 1
  fi

  [[ -z $SPCIADDY ]] && echo "Resetting devices from $xhci..."
  [[ $SPCIADDY ]] && echo "Looking for \'$SPCIADDY\' in $xhci..."


  for i in ????:??:??.? ; do
    [[ $SPCIADDY && "$i" == "$SPCIADDY" ]] && echo -n "Found the specified PCI address $SPCIADDY in $xhci" && echo -n "$i" > unbind &&  echo -n "$i" > bind
    [[ -z $SPCIADDY ]] && echo -n "$i" > unbind && echo -n "$i" > bind
  done
done
