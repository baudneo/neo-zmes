#!/bin/bash

#-----------------------------------------------------
# Download required ML models
#
#
#-----------------------------------------------------

# --- Change these if you want --

PYTHON=python3
PIP=pip3

INSTALL_YOLOV3=${INSTALL_YOLOV3:-yes}
INSTALL_TINYYOLOV3=${INSTALL_TINYYOLOV3:-yes}
INSTALL_YOLOV4=${INSTALL_YOLOV4:-yes}
INSTALL_YOLOV5=${INSTALL_YOLOV5:-yes}
INSTALL_TINYYOLOV4=${INSTALL_TINYYOLOV4:-yes}
INSTALL_CORAL_EDGETPU=${INSTALL_CORAL_EDGETPU:-yes}

TARGET_DIR=${TARGET_DIR:-./models}
WGET=$(which wget)

# utility functions for color coded pretty printing
print_error() {
    COLOR="\033[1;31m"
    NOCOLOR="\033[0m"
    echo -e "${COLOR}ERROR:${NOCOLOR}$1"
}

print_important() {
    COLOR="\033[0;34m"
    NOCOLOR="\033[0m"
    echo -e "${COLOR}IMPORTANT:${NOCOLOR}$1"
}

print_warning() {
    COLOR="\033[0;33m"
    NOCOLOR="\033[0m"
    echo -e "${COLOR}*** WARNING:${NOCOLOR}$1"
}

print_success() {
    COLOR="\033[1;32m"
    NOCOLOR="\033[0m"
    echo -e "${COLOR}Success:${NOCOLOR}$1"
}

get_models() {
  if [ -z "$WGET" ]; then
    print_error "wget was not found, it is needed to download the models! Please install wget using your package manager."
    exit 1
  fi
  mkdir -p "$TARGET_DIR/classes" 2>&1
  wget "https://raw.githubusercontent.com/baudneo/ZMES-models/main/coco.names" -O "$TARGET_DIR/classes/coco.names"
  # create a hashmap of the models and their status
  local MODEL_MAP=(
    ["yolov3"]="$INSTALL_YOLOV3"
    ["tinyyolov3"]="$INSTALL_TINYYOLOV3"
    ["yolov4"]="$INSTALL_YOLOV4"
    ["tinyyolov4"]="$INSTALL_TINYYOLOV4"
    ["yolov5"]="$INSTALL_YOLOV5"
    ["coral_edgetpu"]="$INSTALL_CORAL_EDGETPU"
  )
    # hashmap of the models and their display names
  local MODEL_NAMES_MAP=(
    ["yolov3"]="YOLOv3"
    ["tinyyolov3"]="Tiny YOLOv3"
    ["yolov4"]="YOLOv4"
    ["tinyyolov4"]="Tiny YOLOv4"
    ["yolov5"]="Ultralytics YOLOv5 ONNX"
    ["coral_edgetpu"]="Google Coral Edge TPU"
  )
  local exclude=""
  local target_install_dir=""
  local targets=()
  local sources=()
  for model_name in "${!MODEL_MAP[@]}" ; do
    exclude=""
    if [ "${MODEL_MAP[$model_name]}" == "yes" ]; then
      echo
      print_important "Checking for ${MODEL_NAMES_MAP[$model_name]} data files..."
      # YOLO version 3
      if [ "$model_name" == "yolov3" ]; then
        targets=('yolov3.cfg' 'yolov3.weights')
        sources=('https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3.cfg'
          'https://pjreddie.com/media/files/yolov3.weights')
        target_install_dir="$TARGET_DIR/yolov3"
        exclude="${target_install_dir}/yolov3_classes.txt"
      # Tiny YOLO version 3
      elif [ "$model_name" == "tinyyolov3" ]; then
        targets=(
          'yolov3-tiny.cfg'
          'yolov3-tiny.weights'
            )
        sources=(
          'https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3-tiny.cfg'
          'https://pjreddie.com/media/files/yolov3-tiny.weights'
          )
        target_install_dir="$TARGET_DIR/tinyyolov3"
        exclude="${target_install_dir}/yolov3-tiny.txt"
      # YOLO version 4
      elif [ "$model_name" == "yolov4" ]; then
        print_warning 'Note, you need OpenCV > 4.3 for Yolov4 to work'
        targets=('yolov4.cfg' 'yolov4.weights')
        sources=('https://raw.githubusercontent.com/AlexeyAB/darknet/master/cfg/yolov4.cfg'
          'https://github.com/AlexeyAB/darknet/releases/download/yolov4/yolov4.weights'
        )
        target_install_dir="$TARGET_DIR/yolov4"
      # Tiny YOLO version 4
      elif [ "$model_name" == "tinyyolov4" ]; then
        targets=('yolov4-tiny.cfg' 'yolov4-tiny.weights')
        sources=('https://raw.githubusercontent.com/AlexeyAB/darknet/master/cfg/yolov4-tiny.cfg'
          'https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v4_pre/yolov4-tiny.weights')
        target_install_dir="${TARGET_DIR}/tinyyolov4"
      # ONNX converted Ultralytics YOLO version 5
      elif [ "$model_name" == "yolov5" ]; then
        target_install_dir="$TARGET_DIR/yolov5"
        targets=('yolov5s.onnx')
        sources=('https://github.com/baudneo/ZMES-models/blob/main/yolov5/yolov5s.onnx')
      elif [ "$model_name" == "coral_edgetpu" ]; then
        # Coral TPU
        target_install_dir="$TARGET_DIR/coral_edgetpu"
        targets=(
          'coco_indexed.names'
          'ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite'
          'ssdlite_mobiledet_coco_qat_postprocess_edgetpu.tflite'
          'ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite'
          # EfficeintDet lite 3
          'efficientdet_lite3_512_ptq_edgetpu.tflite'
          # MobileNetv2 but TF2 trained
          'tf2_ssd_mobilenet_v2_coco17_ptq_edgetpu.tflite'
        )
        sources=(
          'https://dl.google.com/coral/canned_models/coco_labels.txt'
          'https://github.com/google-coral/edgetpu/raw/master/test_data/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite'
          'https://github.com/google-coral/test_data/raw/master/ssdlite_mobiledet_coco_qat_postprocess_edgetpu.tflite'
          'https://github.com/google-coral/test_data/raw/master/ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite'
          'https://github.com/google-coral/test_data/raw/master/tf2_ssd_mobilenet_v2_coco17_ptq_edgetpu.tflite'
          'https://github.com/google-coral/test_data/raw/master/efficientdet_lite3_512_ptq_edgetpu.tflite'
        )
    fi

    # begin download model and associated files, first delete excluded files
    if [[ -n "$exclude" ]]; then
      [ -f "${exclude}" ] && rm "${exclude}"
    fi

    for ((i = 0; i < "${#targets[@]}"; ++i)); do
      if [ ! -f "${target_install_dir}/${targets[i]}" ]; then
        ${WGET} "${sources[i]}" -O "${target_install_dir}/${targets[i]}"
      else
        echo "${targets[i]} exists, no need to download"
      fi
    done
    # end of downloading
    else
      print_warning "Model ${MODEL_NAMES_MAP[$model_name]} download has been set to '${MODEL_MAP[$model_name]}', skipping..."
    fi
  done
}
