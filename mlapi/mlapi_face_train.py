#!/usr/bin/env python3
import argparse

import pyzm.ml.face_train_dlib as train
from pyzm.helpers.pyzm_utils import LogBuffer
from pyzm.helpers.pyzm_utils import process_config as proc_conf
from pyzm.helpers.GlobalConfig import GlobalConfig

g = GlobalConfig()
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--config", default="./mlapiconfig.yml", help="config file with path (default ./mlapiconfig.yml)")

ap.add_argument("-s", "--size", type=int, help="resize amount (if you run out of memory)")

ap.add_argument("-d", "--debug", help="enables debug on console", action="store_true")
ap.add_argument("-bd", "--baredebug", help="enables debug on console", action="store_true")

args, u = ap.parse_known_args()
args = vars(args)

g.log = LogBuffer()
mlc, g = proc_conf(args, conf_globals=g, type_="mlapi")
g.config = mlc.config

train.FaceTrain(globs=g).train(size=args["size"])
