import re
from ast import literal_eval
from collections import namedtuple
from traceback import format_exc
from typing import AnyStr, Dict, List, Optional, Union, Tuple

import numpy as np
from shapely.geometry import Polygon

import pyzm.ml.alpr as alpr_detect
import pyzm.ml.face as face_detect
from pyzm.helpers.GlobalConfig import GlobalConfig

# from pyzm.helpers.Media import MediaStream
from pyzm.helpers.New_Media import MediaStream
from pyzm.helpers.pyzm_utils import Timer, mpd_pkl, str2bool
from pyzm.ml.aws_rekognition import AwsRekognition
from pyzm.ml.coral_edgetpu import Tpu

# from pyzm.ml.yolo import Yolo
from pyzm.ml.new_yolo import Yolo

lp: str = "detect:"
DetectedObject = namedtuple("DetectedObject", ["labels", "confidences", "b_boxes"])

g: GlobalConfig

# FUCK YOU
min_conf: Optional[Union[str, float]] = None
min_conf_found: str = ""
mds_image: Optional[str] = None
mds_image_found: str = ""
ioa: Optional[str] = None
ioa_found: str = ""
mda: Optional[str] = None
mda_found: str = ""
conf_upper: Optional[Union[str, int, float]] = None
conf_upper_found: str = ""


def _pre_existing(pe_labels, labels_, origin, model_name):
    ret_val = False
    if pe_labels:
        g.log.debug(2, f"pre_existing_labels: inside {origin}")
        if pe_labels == "pel_any" and not len(labels_):
            # only run if this is the 1st sequence or there were no filtered
            # detections after previous sequence
            g.log.debug(
                f"pre existing labels: configured to 'pel_any' and there are not any detections "
                f"as of yet, skipping model -> '{model_name}'"
            )
            ret_val = True
        elif pe_labels == "pel_none" and len(labels_):
            g.log.debug(
                f"pre existing labels: configured to 'pel_none' and there are detections"
                f", skipping model -> '{model_name}'"
            )
            ret_val = True
        elif not any(x in labels_ for x in pe_labels):
            g.log.debug(
                f"pre_existing_labels: did not find {pe_labels} in {labels_},"
                f" skipping this model..."
            )
            ret_val = True
    return ret_val


def _is_unconverted(expr: str) -> bool:
    """Evaluate a string to see if it is an unconverted secret or substitution variable. If the string begins with
    `{{` or `{[` then it is an unconverted secret or substitution variable.

    >>> _is_unconverted('string')
    """
    expr = expr.strip()
    if expr.startswith("{{"):
        g.log.debug(f"{expr} seems to be an unconverted substitution variable!")
    elif expr.startswith("{["):
        g.log.debug(f"{expr} seems to be an unconverted secret!")
    else:
        return True
    return False


def parse_per_keys(k, v, found_msg):
    global min_conf, min_conf_found, mds_image, mds_image_found, ioa, ioa_found, mda, mda_found, conf_upper, conf_upper_found
    if k == "object_min_confidence" and v:
        min_conf = v
        min_conf_found = found_msg
        g.log.debug(f"DBG => setting {min_conf=} and {min_conf_found=}")
    elif k == "confidence_upper" and v:
        conf_upper = v
        conf_upper_found = found_msg
        g.log.debug(f"DBG => setting {conf_upper=} and {conf_upper_found=}")
    elif k == "max_detection_size" and v:
        mds_image = v
        mds_image_found = found_msg
        g.log.debug(f"DBG => setting {mds_image=} and {mds_image_found=}")
    elif k == "contained_area" and v:
        ioa = v
        ioa_found = found_msg
        g.log.debug(f"DBG => setting {ioa=} and {ioa_found=}")
    elif k == "past_det_max_diff_area" and v:
        mda = v
        mda_found = found_msg
        g.log.debug(f"DBG => setting {mda=} and {mda_found=}")


class DetectSequence:
    def __init__(self, options: Optional[dict] = None):
        self.current_frame: Optional[np.ndarray] = None
        self.sequence_options: dict = {}
        self.zones: list = []
        self.pkl_data: dict = {}
        global g
        g = GlobalConfig()
        if options is None:
            options = {}
        self.has_rescaled: bool = False  # only once in init
        self.model_sequence: Optional[list[str]] = None
        self.ml_sequence: dict = {}
        self.stream_sequence: Optional[dict] = None
        self.global_filters: dict = {}
        self.model_filters: dict = {}
        self.sequence_filters: dict = {}
        self.media: Optional[MediaStream] = None
        self.models: Dict[
            AnyStr, List[Union[Tpu, AwsRekognition, Yolo, face_detect, alpr_detect]]
        ] = {}
        self.model_name: str = ""
        self.model_valid: bool = True
        self.set_ml_options(options, force_reload=True)

    def get_ml_options(self) -> dict:
        """Return the ml_options (ml_sequence) dict"""
        return self.ml_sequence

    def set_ml_options(self, options: dict, force_reload: bool = False):
        """Use this to change ml options later. Note that models will not be reloaded
        unless you add force_reload=True
        """
        lp: str = "detect:set ml opts:"
        if force_reload:
            if self.models:
                g.log.debug(f"{lp} resetting the loaded models!")
            self.models = {}
        if not options:
            return
        model_sequence: Optional[str] = options.get("general", {}).get(
            "model_sequence", "object"
        )
        # convert model_sequence to a list if it is a string
        if model_sequence and isinstance(model_sequence, str):
            self.model_sequence = model_sequence.strip().split(",")
        else:
            raise ValueError(
                f"model_sequence ({model_sequence}) must be a comma seperated string, current type is "
                f"{type(model_sequence)}"
            )
        self.ml_sequence: dict = options
        self.global_filters = options.get("global_filters")
        if self.global_filters is None:
            self.global_filters = {}
        self.stream_sequence = {}
        self.media = None

    def _load_models(self, models=None):
        def get_correct_object_framework(frame_work):
            if frame_work == "opencv":
                return Yolo(options=model_sequence)
            elif frame_work == "coral_edgetpu":
                return Tpu(options=model_sequence)
                # AWS Rekognition
            elif frame_work == "aws_rekognition":
                return AwsRekognition(options=model_sequence)
            else:
                raise ValueError(
                    f"Invalid object_framework: {frame_work}. Only opencv, coral_edgetpu "
                    f"(pycoral) and aws_rekognition supported as of now"
                )

        lp: str = "detect:load models:"
        if not models:
            # convert model_sequence to a list if it is a string
            if self.model_sequence and isinstance(self.model_sequence, str):
                models = self.model_sequence.strip().split(",")
            else:
                models = self.model_sequence
        if not isinstance(models, list):
            g.log.warning(
                f"{lp} models supplied must be a list of models (object, face, alpr)"
            )

        accepted_models: tuple = ("object", "face", "alpr")
        for model in models:
            if model in accepted_models:
                # create an empty list in the model dict for current valid model
                self.models[model] = []
                sequences = self.ml_sequence.get(model, {}).get("sequence")
                if sequences:
                    total_sequences: int = len(sequences)
                    for sequence_idx, model_sequence in enumerate(sequences):
                        sequence_idx += 1
                        obj = None
                        seq_name = model_sequence.get(
                            "name", f"sequence:{sequence_idx}"
                        )
                        if not str2bool(model_sequence.get("enabled", "yes")):
                            g.log.debug(
                                2,
                                f"{lp} skipping sequence '{seq_name}' ({sequence_idx} of "
                                f"{total_sequences}) as it is disabled",
                            )
                            continue
                        model_sequence["disable_locks"] = self.ml_sequence.get(
                            "general", {}
                        ).get("disable_locks", "no")
                        g.log.debug(
                            2,
                            f"{lp} loading '{model}' sequence '{seq_name}' ({sequence_idx} of {total_sequences})",
                        )
                        try:
                            if model == "object":
                                framework = model_sequence.get("object_framework")
                                obj = get_correct_object_framework(framework)
                            elif model == "alpr":
                                obj = alpr_detect.Alpr(options=model_sequence)
                            elif model == "face":
                                obj = face_detect.Face(options=model_sequence)
                            self.models[model].append(obj)
                        except Exception as exc:
                            g.log.error(
                                f"{lp} error while trying to construct '{model}' sequence "
                                f"pipeline for '{seq_name}', skipping..."
                            )
                            g.log.error(f"{lp} EXCEPTION>>>> {exc}")
                            g.log.debug(format_exc())
                else:
                    g.log.warning(
                        f"{lp} '{model}' configured, but there are NO 'sequence' sections defined! skipping..."
                    )
            else:
                g.log.error(
                    f"{lp} the configured model '{model}' is not a valid type! skipping..."
                )

    @staticmethod
    def rescale_polygons(polygons: List[dict], x_factor: float, y_factor: float):
        g.log.debug(
            f"{lp} rescaling polygons: {polygons} using x_factor: {x_factor} and y_factor: {y_factor}"
        )
        for p in polygons:
            for x, y in p["value"]:
                x = int(x) * x_factor
                y = int(y) * y_factor
        g.log.debug(
            2,
            f"{lp} rescaled polygons! {polygons}",
        )
        return polygons

    @staticmethod
    def _polygon_scale_by_resolution(
        polygon: Polygon,
        frame_res: Optional[Union[str, tuple]] = None,
        polygon_res: Optional[Union[str, tuple]] = None,
        x_factor: float = 1.0,
        y_factor: float = 1.0,
    ):
        lp: str = "poly scale:"
        poly_coords = polygon.exterior.coords
        if frame_res and isinstance(frame_res, str):
            frame_res = frame_res.strip().lower()
            frame_res = tuple(int(x) for x in frame_res.split("x"))
        if polygon_res and isinstance(polygon_res, str):
            polygon_res = polygon_res.strip().lower()
            polygon_res = tuple(int(x) for x in polygon_res.split("x"))

        if polygon_res and frame_res:
            x_factor = frame_res[0] / polygon_res[0]
            y_factor = frame_res[1] / polygon_res[1]

        # scale the polygon coordinates using x_factor and y_factor
        scaled_coords = []
        for x, y in poly_coords:
            x = x * x_factor
            y = y * y_factor
            scaled_coords.append((x, y))
        ret_ = Polygon(scaled_coords)
        x_way = "same"
        y_way = "same"
        if x_factor > 1.0:
            x_way = "up"
        elif x_factor < 1.0:
            x_way = "down"
        if y_factor > 1.0:
            y_way = "up"
        elif y_factor < 1.0:
            y_way = "down"
        g.log.debug(
            f"{lp} scaling using factors: x={x_factor:.4f} ({x_way})  y={y_factor:.4f} ({y_way})"
        )
        # g.log.debug(f"DEBUG <>>> polygon = {list(ret_.exterior.coords)[:-1]}")
        return ret_

    @staticmethod
    def _get_polygon_points(
        p: Polygon, as_str: bool = False
    ) -> Union[str, List[Tuple[float, float]]]:
        ret_: List[Tuple[float, float]] = list(zip(*p.exterior.coords.xy))[:-1]
        if not as_str:
            return ret_
        return " ".join(str(x) for x in ret_)

    @staticmethod
    def _bbox2poly(bbox):
        """Convert bounding box coords to a Polygon acceptable input for shapely."""
        if isinstance(bbox[0], int):
            it = iter(bbox)
            bbox = list(zip(it, it))
        bbox.insert(1, (bbox[1][0], bbox[0][1]))
        bbox.insert(3, (bbox[0][0], bbox[2][1]))
        return bbox
        # return Polygon([(bbox[0], bbox[1]), (bbox[0], bbox[3]), (bbox[2], bbox[3]), (bbox[2], bbox[1])])

    def _filter_detections(
        self,
        detections: DetectedObject,
        model_names,
    ):
        """INTERNAL METHOD. Filter out detected objects based upon configured options."""
        # Try to validate options
        lp: str = "detect:filtering:"
        if not self.ml_sequence:
            raise ValueError(
                f"_filter_detections -> ml_options (machine learning options) is Empty or None!"
            )
        elif "general" not in self.ml_sequence:
            raise ValueError(
                f"_filter_detections -> ml_options:general is not configured!"
            )
        global min_conf, min_conf_found, mds_image, mds_image_found, ioa, ioa_found, mda, mda_found, conf_upper, conf_upper_found
        raw_labels: list
        raw_confs: list
        raw_bboxes: list
        # Unpack the NamedTuple
        raw_labels, raw_confs, raw_bboxes = detections

        h, w = self.current_frame.shape[:2]
        saved_ls: Optional[List[str]] = None
        saved_bs: Optional[List[str]] = None
        saved_cs: Optional[List[str]] = None
        saved_event: Optional[str] = None
        mpd: Optional[Union[str, bool]] = None
        mpd_b: list = []
        mpd_l: list = []
        mpd_c: list = []
        if self.pkl_data:
            saved_bs = self.pkl_data["saved_bs"]
            saved_ls = self.pkl_data["saved_ls"]
            saved_cs = self.pkl_data["saved_cs"]
            saved_event = self.pkl_data["saved_event"]
            mpd = self.pkl_data["mpd"]
            mpd_b = self.pkl_data["mpd_b"]
            mpd_l = self.pkl_data["mpd_l"]
            mpd_c = self.pkl_data["mpd_c"]
        new_label, new_bbox, new_conf, new_err, new_model_names = [], [], [], [], []
        new_bbox_to_poly, error_bbox_to_poly = [], []

        tot_labels: int = len(raw_labels) or 0
        poly_total: int = len(self.zones) or 0
        detection_pattern: str = self.ml_sequence.get(self.model_name, {}).get(
            "detection_pattern", ".*"
        )
        if "detection_pattern" in self.sequence_options.keys():
            seq_det_patt = self.sequence_options["detection_pattern"]
            g.log.dbg(
                f"{lp} FOUND! >>> sequence detection pattern: {seq_det_patt}, changing from {detection_pattern}"
            )
            detection_pattern = seq_det_patt

        sequence_name: str = self.sequence_options.get(
            "name", "undefined - placeholder from filter_detections()"
        )
        appended: bool = False

        contained_area: float = 10.0
        failed: bool = False
        failed_reason: str = ""

        # min_conf: Optional[Union[str, float]] = None
        # min_conf_found: str = ""
        # mds_image: Optional[str] = None
        # mds_image_found: str = ""
        # ioa: Optional[str] = None
        # ioa_found: str = ""
        # mda: Optional[str] = None
        # mda_found: str = ""
        # conf_upper: Optional[Union[str, int, float]] = None
        # conf_upper_found: str = ""

        conf_upper_break: bool = False
        p_mpd: Optional[dict] = None

        ######### MAIN LOOP #########
        for idx, b in enumerate(raw_bboxes):
            lp = "detect:filter:"
            if failed:
                # for each object that failed before loop end
                g.log.debug(
                    2,
                    f"--- '{raw_labels[idx - 1]} ({idx}/{tot_labels})' has FAILED filtering. Reason: {failed_reason}",
                )
                failed = False
                failed_reason = ""
            elif conf_upper_break and appended:
                g.log.debug(
                    f"confidence_upper: the configured limit has been hit and a filtered "
                    f"match is present, short-circuiting!"
                )
                # todo: add mpd stuff if enabled as right now conf_upper skips it
                break
            elif conf_upper_break and not appended:
                conf_upper_break = False
                g.log.debug(
                    f"confidence_upper: the configured limit was hit BUT the object was filtered out, "
                    f"not short circuiting"
                )
            appended = False
            current_label = raw_labels[idx]

            conf_upper, conf_upper_found = None, None
            ioa, ioa_found = None, None
            mda, mda_found = None, None
            mds_image, mds_image_found = None, None
            min_conf, min_conf_found = None, None

            show_label = f"{current_label} ({idx + 1}/{tot_labels})"
            g.log.debug(f">>> detected '{show_label}' confidence: {raw_confs[idx]:.2f}")
            poly_b = self._bbox2poly(b)
            # bbox 2 polygon
            current_label_polygon = Polygon(poly_b)
            g.log.debug(
                f"{lp} {show_label} bbox area: {current_label_polygon.area} pixels "
                f"({(current_label_polygon.area / (h * w) * 100):.2f}% of total image area)"
            )
            per_keys_: tuple = (
                "object_min_confidence",
                "max_detection_size",
                "contained_area",
                "confidence_upper",
            )

            for key_ in per_keys_:
                # todo: find a way to recursion or whatever this
                # TOP LEVEL as sequence takes precedence, then model, then global
                if self.global_filters:
                    if key_ in self.global_filters and (
                        (global_filter_val := self.global_filters[key_]) is not None
                    ):
                        if isinstance(
                            global_filter_val, str
                        ) and global_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {key_} ({global_filter_val}) is a template that was not converted! "
                                f"skipping..."
                            )
                            continue
                        g.log.debug(f"DBG => global_filters: parsing {key_}")
                        parse_per_keys(
                            key_,
                            global_filter_val,
                            f"ml_sequence:global_filters: -> {key_}",
                        )
                    elif f"{current_label}_{key_}" in self.global_filters and (
                        (
                            global_filter_val := self.global_filters[
                                f"{current_label}_{key_}"
                            ]
                        )
                        is not None
                    ):
                        if isinstance(
                            global_filter_val, str
                        ) and global_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {raw_labels}_{key_} ({global_filter_val}) is a template that was "
                                f"not converted! skipping..."
                            )
                            continue
                        g.log.debug(
                            f"DBG => global_filters: parsing {raw_labels}_{key_}"
                        )
                        parse_per_keys(
                            key_,
                            global_filter_val,
                            f"ml_sequence:global_filters: -> {raw_labels}_{key_}",
                        )

                if self.model_filters:
                    if key_ in self.model_filters and (
                        (model_filter_val := self.model_filters[key_]) is not None
                    ):
                        if isinstance(
                            model_filter_val, str
                        ) and model_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {key_} ({model_filter_val}) is a template that was not converted! "
                                f"skipping..."
                            )
                            continue
                        g.log.debug(f"DBG => model_filters: parsing {key_}")
                        parse_per_keys(
                            key_, model_filter_val, f"Model {self.model_name} -> {key_}"
                        )
                    elif f"{current_label}_{key_}" in self.model_filters and (
                        (
                            model_filter_val := self.model_filters[
                                f"{current_label}_{key_}"
                            ]
                        )
                        is not None
                    ):
                        if isinstance(
                            model_filter_val, str
                        ) and model_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {raw_labels}_{key_} ({model_filter_val}) is a template that was "
                                f"not converted! skipping..."
                            )
                            continue
                        g.log.debug(
                            f"DBG => model_filters: parsing {raw_labels}_{key_}"
                        )
                        parse_per_keys(
                            key_,
                            model_filter_val,
                            f"Model '{self.model_name}' filter -> {raw_labels}_{key_}",
                        )

                if self.sequence_filters:
                    if key_ in self.sequence_filters and (
                        (seq_filter_val := self.sequence_filters[key_]) is not None
                    ):
                        if isinstance(
                            seq_filter_val, str
                        ) and seq_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {key_} ({seq_filter_val}) is a template that was not converted! "
                                f"skipping..."
                            )
                            continue
                        parse_per_keys(
                            key_,
                            seq_filter_val,
                            f"Sequence {sequence_name} filters -> {key_}",
                        )

                    elif self.sequence_filters.get(f"{current_label}_{key_}") and (
                        (
                            seq_filter_val := self.sequence_filters[
                                f"{current_label}_{key_}"
                            ]
                        )
                        is not None
                    ):
                        if isinstance(
                            seq_filter_val, str
                        ) and seq_filter_val.startswith("{{"):
                            g.log.warning(
                                f"{lp} the value of {raw_labels}_{key_} ({seq_filter_val}) is a template that was "
                                f"not converted! skipping..."
                            )
                            continue
                        g.log.debug(
                            f"DBG => sequence_filters: parsing {raw_labels}_{key_}"
                        )
                        parse_per_keys(
                            key_,
                            seq_filter_val,
                            f"Sequence {sequence_name} filters -> {raw_labels}_{key_}",
                        )

            ######################
            ##### ZONE LOOP ######
            ######################
            for poly_attempt, poly_zone_info in enumerate(self.zones):
                poly_attempt += 1
                zone_name = poly_zone_info.get(
                    "name", "undefined - placeholder via logic"
                )
                polygon_zone: Polygon = Polygon(poly_zone_info["value"])
                g.log.dbg(
                    f"{lp} PARSING ZONE '{zone_name}' ({poly_attempt}/{poly_total})"
                )
                p_exc: Optional[List[str]] = poly_zone_info.get("exclusion", [])

                def normalize(obj, obj_name: str):
                    if obj and isinstance(obj, str):
                        # Evaluate into a list
                        g.log.dbg(
                            f"DBG<>> {obj_name} is a string, evaluating into a python class"
                        )
                        obj = literal_eval(obj)
                        # strip whitespace and convert to lowercase, normalize the input
                        if obj and len(obj):
                            obj = [obj_.strip().lower() for obj_ in obj]
                            g.log.dbg(
                                f"DBG<>> {obj_name}: after stripping and lower() (normalizing) each element -> {obj = }"
                            )
                    elif obj and not isinstance(obj, str):
                        g.log.dbg(
                            f"DBG<>> {obj_name} is not a string, cannot convert to a python class "
                            f"and normalize. Current type is {type(obj)}"
                        )
                    elif obj is None:
                        obj = []
                    return obj

                p_exc = normalize(p_exc, "zone_exclusion")

                p_ioa: Optional[dict] = poly_zone_info.get("contains", {})  # min size
                p_mds: Optional[dict] = poly_zone_info.get("max_size", {})
                p_min_conf: Optional[dict] = poly_zone_info.get("min_conf", {})
                p_mpd: Optional[dict] = poly_zone_info.get("past_area_diff", {})
                p_conf_upper: Optional[dict] = poly_zone_info.get("conf_upper", {})
                p_ioa = {} if p_ioa is None else p_ioa
                p_mds = {} if p_mds is None else p_mds
                p_min_conf = {} if p_min_conf is None else p_min_conf
                p_mpd = {} if p_mpd is None else p_mpd
                p_conf_upper = (
                    {} if p_min_conf is None or not p_conf_upper else p_conf_upper
                )
                p_exc = [] if p_exc is None else p_exc

                # Polygon zone conf_upper: section
                if "all" in p_conf_upper and p_conf_upper["all"]:
                    conf_upper = p_conf_upper["all"]
                    conf_upper_found = f"Defined Zone:{zone_name} -> ALL"
                if (
                    current_label in p_conf_upper
                    and p_conf_upper[current_label]
                ):
                    conf_upper = p_conf_upper[current_label]
                    conf_upper_found = f"Defined Zone:{zone_name} -> {current_label}"

                if "all" in p_mpd and poly_zone_info["past_area_diff"]["all"]:
                    mda = poly_zone_info["past_area_diff"]["all"]
                    mda_found = f"Defined Zone:{zone_name} -> ALL"
                if current_label in p_mpd:
                    mda = poly_zone_info["past_area_diff"][current_label]
                    mda_found = f"Defined Zone:{zone_name} -> {current_label}"
                if "all" in p_ioa:
                    ioa = poly_zone_info.get("contains").get("all")
                    ioa_found = f"Defined Zone:{zone_name} -> ALL"
                if current_label in p_ioa:
                    ioa = poly_zone_info.get("contains").get(current_label)
                    ioa_found = f"Defined Zone:{zone_name} -> {current_label}"
                if "all" in p_mds:
                    mds_image = poly_zone_info.get("max_detection_size").get("all")
                    mds_zone = poly_zone_info.get("max_detection_size").get("all")
                    mds_zone_found = f"Defined Zone:{zone_name} -> ALL"
                    mds_image_found = f"Defined Zone:{zone_name} -> ALL"
                if current_label in p_mds:
                    mds_zone = poly_zone_info.get("max_detection_size").get(
                        current_label
                    )
                    mds_zone_found = f"Defined Zone:{zone_name}:{current_label}:"
                if "all" in p_min_conf:
                    min_conf = poly_zone_info["min_conf"]["all"]
                    min_conf_found = f"Defined Zone:{zone_name} -> ALL"
                if current_label in p_min_conf:
                    min_conf = poly_zone_info.get("min_conf").get(current_label)
                    min_conf_found = f"Defined Zone:{zone_name}:{current_label}:"

                if not min_conf:
                    # min_conf IS REQUIRED
                    g.log.warning(f"{lp} {show_label} min_conf not found! Using 50%")
                    min_conf = 0.5
                    min_conf_found = "NOT FOUND - DEFAULT->50%"

                def perc_pixel(
                    input_, filter_name: str, filter_default=None, in_msg: str = ""
                ):
                    output_ = None
                    out_msg = in_msg
                    _m = re.match(r"(0*?\.?\d*)(%)?$", str(input_), re.IGNORECASE)
                    if _m:
                        try:
                            starts_with: Optional[re.Match] = None
                            if _m.group(1):
                                starts_with = re.search(
                                    r"(0*\.?)(\d*)(%|px)?$", _m.group(1), re.IGNORECASE
                                )
                            if _m.group(2) == "%":
                                # Explicit %
                                output_ = float(_m.group(1)) / 100.0
                            elif starts_with and not starts_with.group(1):
                                # there is no % at end and the string does not start with 0*. or .
                                # consider it a percentile input
                                output_ = float(_m.group(1)) / 100.0
                            else:
                                output_ = float(_m.group(1))
                        except TypeError or ValueError:
                            if filter_default is not None:
                                g.log.warning(
                                    f"{lp} {show_label} {filter_name} could not be converted to a FLOAT! Using default: {filter_default}"
                                )
                                output_ = filter_default
                                out_msg = f"FLOAT ERROR - DEFAULT->{filter_default}"
                            else:
                                g.log.warning(
                                    f"{lp} {show_label} {filter_name} could not be converted to a FLOAT! "
                                    f"skipping this filter"
                                )
                    else:
                        if filter_default is not None:
                            g.log.warning(
                                f"{lp} {show_label} {filter_name} malformed! ({output_}) Using default: {filter_default}"
                            )
                            output_ = filter_default
                            out_msg = f"MALFORMED - DEFAULT->{filter_default}"
                        else:
                            g.log.warning(
                                f"{lp} {show_label} {filter_name} malformed! ({output_}) skipping this filter"
                            )
                    return output_, out_msg

                min_conf, min_conf_found = perc_pixel(
                    min_conf, "min_conf", 0.5, in_msg=min_conf_found
                )
                g.log.debug(
                    f"'{show_label}' minimum confidence found: ({min_conf_found}) -> '{min_conf}'"
                )
                if raw_confs[idx] < min_conf:  # confidence filter
                    g.log.debug(
                        2,
                        f"confidence: {raw_confs[idx] * 100:.2f} is lower than minimum of {min_conf * 100:.2f}, "
                        f"removing...",
                    )
                    error_bbox_to_poly.append(b)
                    new_err.append(b)
                    # set failed if there are no more zones to process
                    failed = True if poly_attempt >= poly_total else False
                    failed_reason = (
                        f"confidence lower than configured minimum of {min_conf}"
                    )
                    continue
                # confidence shortcut - if confidence is above this, set it as a match and stop processing
                if conf_upper:
                    conf_upper, conf_upper_found = perc_pixel(
                        conf_upper, "conf_upper", in_msg=conf_upper_found
                    )
                    if conf_upper:
                        g.log.debug(
                            f"'{show_label}' UPPER confidence found: ({conf_upper_found}) -> '{conf_upper}'"
                        )
                        if (
                            raw_confs[idx] >= conf_upper
                        ):  # upper confidence filter (satisfactory/good enough logic)
                            g.log.debug(
                                2,
                                f"confidence: {raw_confs[idx] * 100:.2f} is equal to or higher than "
                                f"{conf_upper * 100:.2f}, accepting as a match..",
                            )
                            conf_upper_break = True
                            break

                if (
                    mds_image
                    and mds_image != "100%"
                    and mds_image_found.find(current_label) > 0
                ):
                    g.log.debug(
                        4,
                        f"'{show_label}' max area of detected object found ({mds_image_found}) -> '{mds_image}'",
                    )

                    # Let's make sure it's the right size
                    _m = re.match(r"(\d*\.?\d*)(px|%)?$", str(mds_image), re.IGNORECASE)
                    if _m:
                        max_object_area_of_zone = max_object_area_of_image = float(
                            _m.group(1)
                        )

                        if _m.group(2) == "%":
                            max_object_area_of_image = (
                                float(_m.group(1)) / 100.0 * (h * w)
                            )
                            max_object_area_of_zone = (
                                float(_m.group(1)) / 100.0 * polygon_zone.area
                            )

                            g.log.debug(
                                2,
                                f"max size: converted {_m.group(1)}% of {w}*{h}->{round(w * h)} to "
                                f"{max_object_area_of_image} pixels",
                            )
                            if (
                                max_object_area_of_image
                                and max_object_area_of_image > (h * w)
                            ):
                                max_object_area_of_image = h * w
                        if (
                            max_object_area_of_image
                            and current_label_polygon.area > max_object_area_of_image
                        ):
                            g.log.debug(
                                f"max size: {current_label_polygon.area:.2f} is larger then the max allowed: {max_object_area_of_image:.2f},"
                                f"removing..."
                            )
                            failed = True if poly_attempt >= poly_total else False
                            failed_reason = (
                                f"size of detected objects bounding box is larger then the configured "
                                f"max: {max_object_area_of_image:.2f}"
                            )
                            error_bbox_to_poly.append(b)
                            new_err.append(b)
                            continue

                if zone_name != "full_image":
                    # (W[x], H[y])
                    frame_res: Optional[Tuple[int, int]] = self.current_frame.shape[:2][
                        ::-1
                    ]
                    polygon_res: Optional[Tuple[int, int]] = poly_zone_info.get(
                        "resolution"
                    )
                    if polygon_res and (frame_res != polygon_res):
                        g.log.debug(
                            f"{lp}:zone: scaling polygon for zone {zone_name} - Frame dimensions are {frame_res} "
                            f"but the zone was configured for {polygon_res = }"
                        )
                        polygon_zone = self._polygon_scale_by_resolution(
                            polygon_zone, frame_res, polygon_res
                        )

                    g.log.debug(
                        2,
                        f"checking if '{show_label}' @ {self._get_polygon_points(current_label_polygon, as_str=True)} is inside polygon/zone '{zone_name}' located at "
                        f"{self._get_polygon_points(polygon_zone, as_str=True)}",
                    )

                    if current_label_polygon.intersects(polygon_zone):
                        g.log.debug(
                            f"'{show_label}' INTERSECTS polygon/zone '{zone_name}'"
                        )
                        if p_exc and current_label in p_exc:
                            g.log.debug(
                                f"{show_label} is an excluded object for zone '{zone_name}', marking FAILED "
                                f"and breaking out of polygon loop..."
                            )
                            error_bbox_to_poly.append(b)
                            new_err.append(b)
                            failed = True
                            failed_reason = f"exclusion filter from {zone_name}"
                            break
                        pixels_inside = current_label_polygon.intersection(
                            polygon_zone
                        ).area
                        percent_inside = (
                            pixels_inside / current_label_polygon.area
                        ) * 100
                        # todo: exclusion zone use contained filter as well
                        g.log.debug(
                            2,
                            f"'{show_label}' has {pixels_inside:.2f} pixels ({percent_inside:.2f}%) inside"
                            f" '{zone_name}'",
                        )
                        if ioa:
                            g.log.debug(
                                4,
                                f"minimum area of detected object inside of polygon/zone: found ({ioa_found})"
                                f" -> '{ioa}'",
                            )
                            g.log.debug(f"DEBUG!!! >>> {type(ioa) = }")
                            # Let's make sure it's the right size
                            _m = re.match(r"(\d*\.?\d*)(px|%)?$", str(ioa), re.IGNORECASE)
                            if _m:
                                contained_area = float(_m.group(1))
                                if _m.group(2) == "%":
                                    contained_area = (
                                        float(_m.group(1))
                                        / 100.0
                                        * current_label_polygon.area
                                    )
                                    g.log.debug(
                                        2,
                                        f"contained within zone: converted {_m.group(1)}% of {show_label}'s area "
                                        f"({current_label_polygon.area}) to {contained_area} pixels",
                                    )
                        if pixels_inside < contained_area:
                            g.log.debug(
                                f"'{show_label}' only has {pixels_inside:.2f} pixels ({percent_inside:.2f}%) inside "
                                f"of {zone_name} minimum is -> {contained_area} ("
                                f"{round((contained_area / current_label_polygon.area) * 100, 2)}%)"
                            )
                            error_bbox_to_poly.append(b)
                            new_err.append(b)
                            failed = True
                            failed_reason = (
                                f"area of detected object inside of polygon/zone is less then "
                                f"configured: {contained_area:.2f}"
                            )
                            continue
                    else:
                        g.log.dbg(
                            f"intersection: '{show_label}' does not intersect zone: {zone_name}"
                        )
                        error_bbox_to_poly.append(b)
                        new_err.append(b)
                        failed = True
                        failed_reason = f"intersection: '{show_label}' does not intersect zone: {zone_name}"
                        continue

                if poly_zone_info.get("pattern"):
                    # zone has its own detection pattern
                    detection_pattern = poly_zone_info["pattern"]
                    g.log.debug(
                        3,
                        f"detection pattern: has been overridden by defined zone '{zone_name}' to '{detection_pattern}'",
                    )
                else:
                    g.log.debug(
                        2, f"detection pattern: set globally as {detection_pattern}"
                    )

                r = re.compile(detection_pattern)
                # ONLY RETURN LABELS THAT MATCH THE PATTERN
                match = list(filter(r.match, raw_labels))
                if current_label not in match:
                    error_bbox_to_poly.append(b)
                    new_err.append(b)
                    g.log.debug(
                        3,
                        f"detection pattern: '{show_label}' does not match pattern filter, removing...",
                    )
                    failed = True if poly_attempt >= poly_total else False
                    failed_reason = f"detection pattern filter from zone '{zone_name}'"
                    continue

            #   OUT OF POLYGON/ZONE LOOP 
            if failed:
                continue

            #   MATCH PAST DETECTIONS 
            if self.sequence_options.get("match_past_detections") is not None:
                g.log.debug(f"{lp} match_past_detections set in sequence options")
                mpd = self.sequence_options.get("match_past_detections")
            if (
                (str2bool(mpd))
                and (
                    not g.past_event
                    or (g.past_event and str2bool(g.config.get("force_mpd")))
                )
                and (
                    (not g.eid == saved_event)
                    or (g.eid == saved_event and str2bool(g.config.get("force_mpd")))
                )
            ):
                lp = "mpd:"
                g.log.dbg(
                    f'MPD DEBUG => {g.past_event = } -- {str2bool(g.config.get("force_mpd")) = } -- {mpd = }'
                )
                if not saved_bs:
                    g.log.debug(
                        f"{lp} there are no saved detections to evaluate, skipping match past detection filter"
                    )
                else:
                    max_diff_area: Optional[Union[str, float]]
                    use_percent: bool = False
                    ignore_mpd: bool = False
                    # Check if the pre-defined zone filter was set as it takes precedence
                    if not p_mpd and not mda:
                        # Start in the global filters
                        if self.global_filters.get("past_det_max_diff_area"):
                            if _is_unconverted(
                                self.global_filters["past_det_max_diff_area"]
                            ):
                                mda = self.global_filters["past_det_max_diff_area"]
                                mda_found = f"ml_sequence:global_filters -> past_det_max_diff_area"
                        if self.global_filters.get(
                            f"{current_label}_past_det_max_diff_area"
                        ):
                            if _is_unconverted(
                                self.global_filters[
                                    f"{current_label}_past_det_max_diff_area"
                                ]
                            ):
                                mda = self.global_filters[
                                    f"{current_label}_past_det_max_diff_area"
                                ]
                                mda_found = f"ml_sequence:global_filters -> {current_label}_past_det_max_diff_area"
                        # MODEL_NAME>general
                        if self.model_filters.get("past_det_max_diff_area"):
                            if _is_unconverted(
                                self.model_filters["past_det_max_diff_area"]
                            ):
                                mda = self.model_filters["past_det_max_diff_area"]
                                mda_found = f"Model {self.model_name}:model_filters -> past_det_max_diff_area"
                        if self.model_filters.get(
                            f"{current_label}_past_det_max_diff_area"
                        ):
                            if _is_unconverted(
                                self.model_filters[
                                    f"{current_label}_past_det_max_diff_area"
                                ]
                            ):
                                mda = self.model_filters["general"][
                                    f"{current_label}_past_det_max_diff_area"
                                ]
                                mda_found = f"Model {self.model_name}:model_filters -> {current_label}_past_det_max_diff_area"
                        # Sequence options
                        if self.sequence_options.get("past_det_max_diff_area"):
                            mda = self.sequence_options.get("past_det_max_diff_area")
                            mda_found = (
                                f"Sequence {sequence_name} -> past_det_max_diff_area"
                            )
                        if self.sequence_options.get(
                            f"{current_label}_past_det_max_diff_area"
                        ):
                            mda = self.sequence_options.get(
                                f"{current_label}_past_det_max_diff_area"
                            )
                            mda_found = f"Sequence {sequence_name} -> {current_label}_past_det_max_diff_area"
                    # Check if the mpd_ignore option is configured in general or in the per sequence
                    mpd_ig = self.global_filters.get("ignore_past_detection_labels")
                    # If it is still a string it needs to be evaluated into a list
                    if isinstance(mpd_ig, str) and len(mpd_ig):
                        try:
                            mpd_ig = literal_eval(mpd_ig)
                            if not isinstance(mpd_ig, list):
                                g.log.warning(
                                    f"{lp} ignore_past_detection_labels is not a list after evaluation! "
                                    f"raising ValueError"
                                )
                                raise ValueError(
                                    "ignore_past_detection_labels is malformed!"
                                )
                        except ValueError or SyntaxError as e:
                            g.log.warning(
                                f"{lp} ignore_past_detection_labels ("
                                f"{self.global_filters['ignore_past_detection_labels']}) "
                                f"is malformed, ignoring..."
                            )
                            g.log.debug(
                                f"{lp} ignore_past_detection_labels EXCEPTION MESSAGE: {e}"
                            )
                            mpd_ig = []
                    if mpd_ig and current_label in mpd_ig:
                        g.log.debug(
                            4,
                            f"{lp} {current_label} is in ignore_past_detection_labels: {mpd_ig}, skipping",
                        )
                        ignore_mpd = True

                    if not ignore_mpd:
                        g.log.debug(
                            f"{lp} past detection max difference area found! ({mda_found}) -> {mda}"
                        )
                        # Format max difference area
                        if mda:
                            _m = re.match(r"(\d*\.?\d*)(px|%)?$", str(mda), re.IGNORECASE)

                            if _m:
                                max_diff_area = float(_m.group(1))
                                use_percent = (
                                    True
                                    if _m.group(2) is None or _m.group(2) == "%"
                                    else False
                                )
                            else:
                                g.log.error(
                                    f"{lp}  malformed -> {mda}, setting to 5%..."
                                )
                                use_percent = True
                                max_diff_area = 0.05

                            # it's very easy to forget to add 'px' when using pixels
                            if use_percent and (
                                max_diff_area < 0 or max_diff_area > 100
                            ):
                                g.log.error(
                                    f"{lp} {max_diff_area} must be in the range 0-100 when "
                                    f"using percentages: {mda}, setting to 5%..."
                                )
                                max_diff_area = 0.05
                        else:
                            g.log.debug(
                                f"{lp} no past_det_max_diff_area or per label overrides configured while "
                                f"match_past_detections=yes, setting to 5% as default"
                            )
                            max_diff_area = 0.05
                            use_percent = True

                        g.log.debug(
                            4,
                            f"{lp} max difference in area configured ({mda_found}) -> '{mda}', comparing past "
                            f"detections to current",
                        )

                        # Compare current detection to past detections AREA
                        for saved_idx, saved_b in enumerate(saved_bs):
                            past_label = saved_ls[saved_idx]
                            # compare current detection element with saved list from file
                            found_alias_grouping = False
                            aliases: Union[str, dict] = self.ml_sequence.get(
                                "aliases", {}
                            )
                            # parse from a str into a dict
                            if isinstance(aliases, str) and len(aliases):
                                aliases = literal_eval(aliases)
                            if past_label != current_label:
                                if aliases and isinstance(aliases, dict):
                                    g.log.debug(
                                        f"{lp} currently detected object does not match saved object, "
                                        f"checking aliases for a group match"
                                    )
                                    for alias_name, alias_values in aliases.items():
                                        if found_alias_grouping:
                                            break
                                        elif (
                                            past_label in alias_values
                                            and current_label in alias_values
                                        ):
                                            found_alias_grouping = True
                                            g.log.debug(
                                                2,
                                                f"{lp} aliases: current detected label -> '{current_label}' and past label "
                                                f"-> '{past_label}' are in an alias group named -> '{alias_name}'",
                                            )
                                elif aliases and not isinstance(aliases, dict):
                                    g.log.debug(
                                        f"{lp} aliases are configured but the format is incorrect, check the example "
                                        f"config for formatting and reformat aliases to a dictionary type setup"
                                    )

                            elif past_label == current_label:
                                found_alias_grouping = True
                            if not found_alias_grouping:
                                continue
                            # Found a match by label, now compare the area using Polygon
                            saved_poly = self._bbox2poly(saved_b)
                            past_label_polygon = Polygon(saved_poly)
                            max_diff_pixels = None
                            diff_area = None
                            g.log.debug(
                                4,
                                f"{lp} comparing '{current_label}' PAST->{saved_b} to CURR->{b}",
                            )
                            if past_label_polygon.intersects(
                                current_label_polygon
                            ) or current_label_polygon.intersects(past_label_polygon):
                                if past_label_polygon.intersects(current_label_polygon):
                                    g.log.debug(
                                        4,
                                        f"{lp} the PAST object INTERSECTS the new object",
                                    )
                                else:
                                    g.log.debug(
                                        4,
                                        f"{lp} the current object INTERSECTS the PAST object",
                                    )
                                if current_label_polygon.contains(past_label_polygon):
                                    diff_area = current_label_polygon.difference(
                                        past_label_polygon
                                    ).area
                                    if use_percent:
                                        max_diff_pixels = (
                                            current_label_polygon.area
                                            * max_diff_area
                                            / 100
                                        )
                                else:
                                    diff_area = past_label_polygon.difference(
                                        current_label_polygon
                                    ).area
                                    if use_percent:
                                        max_diff_pixels = (
                                            past_label_polygon.area
                                            * max_diff_area
                                            / 100
                                        )
                                if (
                                    diff_area is not None
                                    and diff_area <= max_diff_pixels
                                ):
                                    g.log.debug(
                                        f"{lp} removing '{show_label}' as it seems to be approximately in the same spot"
                                        f" as it was detected last time based on '{mda}' -> Difference in pixels: {diff_area} "
                                        f"- Configured maximum difference in pixels: {max_diff_pixels}"
                                    )
                                    if saved_b not in mpd_b:
                                        g.log.debug(
                                            f"{lp} appending this saved object to the mpd "
                                            f"buffer as it has removed a detection and should be propagated "
                                            f"to the next event"
                                        )
                                        mpd_b.append(saved_bs[saved_idx])
                                        mpd_l.append(past_label)
                                        mpd_c.append(saved_cs[saved_idx])
                                    new_err.append(b)
                                    failed = True
                                    break
                                elif (
                                    diff_area is not None
                                    and diff_area > max_diff_pixels
                                ):
                                    g.log.debug(
                                        4,
                                        f"{lp} allowing '{show_label}' -> the difference in the area of last detection "
                                        f"to this detection is '{diff_area:.2f}', a minimum of {max_diff_pixels:.2f} "
                                        f"is needed to not be considered 'in the same spot'",
                                    )
                                elif diff_area is None:
                                    g.log.debug(
                                        f"DEBUG>>>'MPD' {diff_area = } - whats the issue?"
                                    )
                                else:
                                    g.log.debug(
                                        f"WHATS GOING ON? {diff_area = } -- {max_diff_pixels = }"
                                    )
                            # Saved does not intersect the current object/label
                            else:
                                g.log.debug(
                                    f"{lp} current detection '{current_label}' is not near enough to '"
                                    f"{past_label}' to evaluate for match past detection filter"
                                )
                                continue
                        if failed:
                            continue
                        # out of past detection bounding box loop, still inside if mpd

            elif (g.past_event and not str2bool(g.config.get("force_mpd"))) and (
                str2bool(mpd)
            ):
                g.log.debug(
                    f"{lp} this is a PAST event, skipping match past detections filter... override with "
                    f"'mpd_force=yes'"
                )
            elif (g.eid == saved_event) and (str2bool(mpd)):
                g.log.debug(
                    f"{lp} the current event is the same event as the last time this monitor processed an"
                    f" event, skipping match past detections filter... override with "
                    f"'mpd_force=yes'"
                )

            # end of main loop, if we made it this far current_label has passed filtering
            new_bbox.append(raw_bboxes[idx])
            new_label.append(current_label)
            new_conf.append(raw_confs[idx])
            new_model_names.append(model_names[idx])
            new_bbox_to_poly.append(b)
            appended = True
            g.log.debug(2, f"+++ '{show_label}' has PASSED filtering")
            failed = False
        # out of primary bounding box loop
        if failed:
            g.log.debug(
                2,
                f"<<< '{raw_labels[-1]} ({tot_labels}/{tot_labels})' has FAILED filtering. Reason: {failed_reason}",
            )
            failed = False
            failed_reason = ""
        data = {
            "_b": new_bbox,
            "_l": new_label,
            "_c": new_conf,
            "_e": new_err,
            "_m": new_model_names,
        }
        extras = {
            "mpd_data": {
                "mpd_b": mpd_b,
                "mpd_c": mpd_c,
                "mpd_l": mpd_l,
            },
            "confidence_upper_break": conf_upper_break,
            "bbox_to_poly": {
                "new_bbox_to_poly": new_bbox_to_poly,
                "error_bbox_to_poly": error_bbox_to_poly,
            },
        }
        return data, extras

    #############################
    # RUN DETECTION ON A STREAM #
    #############################
    def detect_stream(
        self,
        stream,
        options: Optional[dict] = None,
        in_file=False,
    ):

        filtered_extras: dict = {}
        if options is None:
            options = {}
        saved_bs: list = []
        saved_ls: list = []
        saved_cs: list = []
        all_frames: list = []
        all_matches: list = []
        matched_b: list = []
        matched_e: list = []
        matched_l: list = []
        matched_c: list = []
        mpd_b: list = []
        mpd_c: list = []
        mpd_l: list = []
        matched_detection_types: list = []
        matched_frame_id: Optional[str] = None
        matched_model_names: list = []
        matched_frame_img: Optional[str] = None
        manual_locking: bool = False
        saved_event: Optional[str] = None
        self.stream_sequence = options

        frame_strategy: Optional[str] = self.stream_sequence.get(
            "frame_strategy", "most_models"
        )
        g.log.dbg(f"frame_strategy: {frame_strategy}")
        t: Timer = Timer()
        self.media = MediaStream(stream=stream, options=self.stream_sequence)
        self.zones: list = self.stream_sequence.get("polygons", [])
        # if polygons:
        #     polygons = list(polygons)
        # todo: mpd as part of ml_overrides?
        mpd: Union[str, bool] = str2bool(
            self.global_filters.get("match_past_detections", False)
        )
        # Loops across all frames
        # match past detections is here, so we don't try and load/dump while still detecting

        if mpd:
            saved_bs, saved_ls, saved_cs, saved_event = mpd_pkl("load")
            self.pkl_data = {
                "saved_bs": saved_bs,
                "saved_ls": saved_ls,
                "saved_cs": saved_cs,
                "saved_event": saved_event,
                "mpd": mpd,
                "mpd_b": mpd_b,
                "mpd_c": mpd_c,
                "mpd_l": mpd_l,
            }
            g.log.debug(
                f"mpd: last_event={saved_event} -- saved labels={saved_ls} -- saved_bbox={saved_bs} -- "
                f"saved conf={saved_cs}"
            )

        while self.media.more():
            self.current_frame: Optional[np.ndarray] = self.media.read()
            if self.current_frame is None:
                g.log.debug(
                    f"{lp} There are no more frames to process, breaking out of frame grabbing loop"
                )
                break
            dimensions = self.media.image_dimensions()
            old_h = dimensions["original"][0]
            old_w = dimensions["original"][1]
            new_h = dimensions["resized"][0]
            new_w = dimensions["resized"][1]
            frame_perf_timer = Timer()
            _labels_in_frame: list = []
            _boxes_in_frame: list = []
            _error_boxes_in_frame: list = []
            _confs_in_frame: list = []
            _detection_types_in_frame: list = []
            _model_names_in_frame: list = []
            # remember this needs to occur after a frame
            # is read, otherwise we don't have dimensions
            if not self.zones:
                self.zones = [
                    {
                        "name": "full_image",
                        "value": [(0, 0), (old_w, 0), (old_w, old_h), (0, old_h)],
                        # 'value': [(0, 0), (5, 0), (5, 5), (0, 5)],
                        "pattern": None,
                    }
                ]

                g.log.debug(
                    2,
                    f"{lp} no polygons/zones specified, adding 'full_image' as polygon @ {self.zones[0]['value']}",
                )

            if (not self.has_rescaled) and self.stream_sequence.get(
                "resize", "no"
            ) != "no":
                if (old_h != new_h) or (old_w != new_w):
                    self.has_rescaled = True
                    xfact: float = old_w / new_w or 1.0
                    yfact: float = old_h / new_h or 1.0
                    self.zones[:] = self.rescale_polygons(self.zones, xfact, yfact)
            # For each frame, loop across all models
            found = False
            if not isinstance(self.model_sequence, list):
                raise ValueError(f"No model sequences configured? FATAL!")

            for model_loop, self.model_name in enumerate(self.model_sequence):
                self.model_filters = self.ml_sequence.get(self.model_name, {}).get(
                    "model_filters", {}
                )
                if self.model_filters is None:
                    self.model_filters = {}
                pre_existing_labels = self.model_filters.get("pre_existing_labels")
                if _pre_existing(
                    pre_existing_labels,
                    _labels_in_frame,
                    f"{self.model_name}:model_filters",
                    self.model_name,
                ):
                    continue

                if not self.models.get(self.model_name):
                    g.log.dbg(
                        f"DEBUGGING: {self.model_name} model is not loaded! LOADING NOW by calling self._load_models()"
                    )
                    self._load_models([self.model_name])
                model_sequence_strategy = self.ml_sequence.get(self.model_name, {}).get(
                    "sequence_strategy", "most"
                )

                # start of same model iteration (sequences)
                _b_best_in_same_model: list = []
                _l_best_in_same_model: list = []
                _c_best_in_same_model: list = []
                _e_best_in_same_model: list = []
                _m_best_in_same_model: list = []
                _polygons_in_same_model: list = []
                _error_polygons_in_same_model: list = []
                filtered_out_bboxs: list = []
                # For each model, loop across different variations/sequences
                for sequence_loop, sequence in enumerate(self.models[self.model_name]):
                    filtered: bool = False
                    self.sequence_options = sequence.get_options()
                    self.sequence_filters = self.sequence_options.get(
                        "sequence_filters", {}
                    )
                    if self.sequence_filters is None:
                        self.sequence_filters = {}
                    seq_name = self.sequence_options.get("name", "")
                    mpd = self.sequence_filters.get("match_past_detections", mpd)
                    if str2bool(mpd):
                        if not saved_cs or not len(saved_cs):
                            saved_bs, saved_ls, saved_cs, saved_event = mpd_pkl("load")

                    show_len: int = self.media.get_max_frames()
                    if in_file and self.media.type == "file":
                        # --file image show_len = 1 ,video file and API both use frame_set
                        show_len = 1
                    g.log.debug(
                        2,
                        f"frame: {self.media.get_last_frame()} [strategy:'{frame_strategy}'] ("
                        f"{self.media.get_frames_processed()} of {show_len}) - "
                        f"model: '{self.model_name}' [strategy:'{frame_strategy}'] ({model_loop + 1} of "
                        f"{len(self.model_sequence)}) - sequence: '{seq_name}' "
                        f"[strategy:'{model_sequence_strategy}'] ({sequence_loop + 1} of "
                        f"{len(self.models[self.model_name])})",
                    )
                    pre_existing_labels = self.sequence_filters.get(
                        "pre_existing_labels"
                    )
                    if _pre_existing(
                        pre_existing_labels,
                        _labels_in_frame,
                        f"'ml_sequence':'{self.model_name}':'sequence':'{seq_name}'",
                        self.model_name,
                    ):
                        continue

                    try:
                        (
                            detected_bbox,
                            detected_labels,
                            detected_confs,
                            detected_model_names,
                        ) = sequence.detect(input_image=self.current_frame)
                    except Exception as e:
                        g.log.error(f"{lp} error running sequence '{seq_name}'")
                        g.log.error(f"{lp} ERR_MSG--> {e}")
                    else:
                        tot_labels = len(detected_labels)
                        g.log.debug(
                            2,
                            f"{lp} model: '{self.model_name}' seq: '{seq_name}' found {tot_labels} "
                            f"detection{'s' if tot_labels > 1 or tot_labels == 0 else ''}"
                            f" -> {', '.join(detected_labels)}",
                        )
                        filtered_tot_labels: int = 0
                        # ONLY FILTER IF THERE ARE DETECTIONS
                        if tot_labels:

                            detected_objs = DetectedObject(
                                detected_labels, detected_confs, detected_bbox
                            )
                            # test = DetectedObject(labels=["car"], confidences=[0.9], b_boxes=[(0, 0, 10, 10)])
                            filtered_data, filtered_extras = self._filter_detections(
                                detected_objs,
                                detected_model_names,
                            )
                            detected_bbox = filtered_data["_b"]
                            detected_labels = filtered_data["_l"]
                            detected_confs = filtered_data["_c"]
                            filtered_out_bboxs = filtered_data["_e"]
                            detected_model_names = filtered_data["_m"]
                            filtered_tot_labels = len(detected_labels)
                            _e_best_in_same_model.extend(filtered_out_bboxs)
                            if not filtered_tot_labels:
                                filtered = True

                        if filtered:
                            g.log.debug(f"ALL DETECTED OBJECTS WERE FILTERED OUT")
                            # reset to False, causes issues otherwise
                            filtered = False
                            continue
                        g.log.debug(
                            2,
                            f"{lp}strategy: '{filtered_tot_labels}' filtered label"
                            f"{'s' if filtered_tot_labels > 1 else ''}: {detected_labels} {detected_confs} "
                            f"{detected_model_names} {detected_bbox}",
                        )
                        if filtered_tot_labels == 0:
                            filtered_extras["confidence_upper_break"] = False
                        # Moved here to be evaluated if changed by the 'confidence_upper' filter
                        added_ = False
                        # 'person': <confidence level>

                        if (
                            (model_sequence_strategy == "first")
                            or (
                                (model_sequence_strategy == "most")
                                and (len(detected_labels) > len(_l_best_in_same_model))
                            )
                            or (
                                (model_sequence_strategy == "most_unique")
                                and (
                                    len(set(detected_labels))
                                    > len(set(_l_best_in_same_model))
                                )
                            )
                        ):
                            added_ = True

                            _b_best_in_same_model = detected_bbox
                            _l_best_in_same_model = detected_labels
                            _c_best_in_same_model = detected_confs
                            _e_best_in_same_model = filtered_out_bboxs
                            _m_best_in_same_model = detected_model_names

                        elif model_sequence_strategy == "union":
                            added_ = True
                            _b_best_in_same_model.extend(detected_bbox)
                            _l_best_in_same_model.extend(detected_labels)
                            _c_best_in_same_model.extend(detected_confs)
                            _e_best_in_same_model.extend(filtered_out_bboxs)
                            _m_best_in_same_model.extend(detected_model_names)

                        if (model_sequence_strategy == "first") and len(detected_bbox):
                            g.log.debug(
                                3,
                                f"{lp} breaking out of sequence loop as 'same_model_sequence_strategy' is first",
                            )
                            break
                        if (
                            model_sequence_strategy != "first"
                            and filtered_extras.get("confidence_upper_break") is True
                        ):
                            g.log.debug(
                                3,
                                f"{lp}confidence_upper: short-circuiting 'same_model_sequence_strategy'",
                            )
                            if not added_ and len(detected_bbox):
                                g.log.debug(
                                    f"DEBUG>>> this match was not added to best in same model yet, ADDING NOW"
                                )
                                _b_best_in_same_model = detected_bbox
                                _l_best_in_same_model = detected_labels
                                _c_best_in_same_model = detected_confs
                                _e_best_in_same_model = filtered_out_bboxs
                                _m_best_in_same_model = detected_model_names
                            break
                # end of same model sequence iteration
                # at this state x_best_in_same_model contains the best match across
                # same model variations

                # still inside model loop
                if len(_l_best_in_same_model):
                    found = True
                    _labels_in_frame.extend(_l_best_in_same_model)
                    _boxes_in_frame.extend(_b_best_in_same_model)
                    _confs_in_frame.extend(_c_best_in_same_model)
                    _error_boxes_in_frame.extend(_e_best_in_same_model)
                    _detection_types_in_frame.extend(
                        [self.model_name] * len(_l_best_in_same_model)
                    )
                    _model_names_in_frame.extend(_m_best_in_same_model)
                    if model_sequence_strategy == "first":
                        g.log.debug(
                            2,
                            f"{lp} breaking out of MODEL loop as 'same_model_sequence_strategy' is 'first'",
                        )
                        break
                else:
                    if not filtered and self.model_valid:
                        g.log.debug(
                            2,
                            f"{lp} no '{self.model_name}' matches at all in frame: {self.media.last_frame_id_read}",
                        )
                    elif filtered and self.model_valid:
                        g.log.debug(
                            2,
                            f"{lp} all '{self.model_name}' matches in frame {self.media.last_frame_id_read} "
                            f"were filtered out  -> {filtered = } --- {self.model_valid = }",
                        )
                    else:
                        g.log.debug(
                            f"only 1 detection and it wasn't filtered out? -- IDK MAN come and check it out"
                            f" {filtered = } {self.model_valid = }"
                        )

            # end of model loop

            # still in frame loop
            frame_timer_end = frame_perf_timer.stop_and_get_ms()
            g.log.debug(
                2, f"perf:frame: {self.media.last_frame_id_read} took {frame_timer_end}"
            )
            if found:
                all_matches.append(
                    {
                        "labels": _labels_in_frame,
                        "model_names": _model_names_in_frame,
                        "confidences": _confs_in_frame,
                        "detection_types": _detection_types_in_frame,
                        "frame_id": self.media.last_frame_id_read,
                        "boxes": _boxes_in_frame,
                        "error_boxes": _error_boxes_in_frame,
                        "image": self.current_frame.copy(),
                    }
                )
                all_frames.append(self.media.get_last_read_frame())
                if frame_strategy == "first":
                    g.log.debug(
                        2,
                        f"{lp} breaking out of frame loop as 'frame_strategy' is 'first'",
                    )
                    break
                elif filtered_extras.get("confidence_upper_break") is True:
                    g.log.debug(
                        3, f"{lp}confidence_upper: short-circuiting 'frame_strategy'"
                    )
                    break

        # end of while media loop
        # find best match in all_matches
        # matched_poly, matched_err_poly = [], []
        for idx, item in enumerate(all_matches):
            # g.logger.debug(1,f"dbg:strategy: most:[{len(item['labels']) = } > {len(matched_l) = }]
            # most_models:[{len(item['detection_types']) = } > {len(matched_detection_types) = }]"
            #                    f"most_unique:[{len(set(item['labels'])) = } > {len(set(matched_l)) = }]")
            # Credit to @hqhoang for this idea - https://github.com/ZoneMinder/pyzm/issues/36
            # Takes into consideration confidences instead of returning the first match.
            if (
                (frame_strategy == "first")
                or (
                    (frame_strategy == "most")
                    and (len(item["labels"]) > len(matched_l))
                )
                or (
                    (frame_strategy == "most")
                    and (len(item["labels"]) == len(matched_l))
                    and (sum(matched_c) < sum(item["confidences"]))
                )
                or (
                    (frame_strategy == "most_models")
                    and (len(item["detection_types"]) > len(matched_detection_types))
                )
                or (
                    (frame_strategy == "most_models")
                    and (len(item["detection_types"]) == len(matched_detection_types))
                    and (sum(matched_c) < sum(item["confidences"]))
                )
                or (
                    (frame_strategy == "most_unique")
                    and (len(set(item["labels"])) > len(set(matched_l)))
                )
                or (
                    (frame_strategy == "most_unique")
                    and (len(set(item["labels"])) == len(set(matched_l)))
                    and (sum(matched_c) < sum(item["confidences"]))
                )
            ):
                # matched_poly = item['bbox2poly']
                matched_l = item["labels"]
                matched_model_names = item["model_names"]
                matched_c = item["confidences"]
                matched_frame_id = item["frame_id"]
                matched_detection_types = item["detection_types"]
                matched_b = item["boxes"]
                matched_e = item["error_boxes"]
                # matched_err_poly = item['err2poly']
                matched_frame_img = item["image"]
                # g.logger.debug(1,f"dbg:strategy: {matched_l = }")

        if manual_locking:
            for model_name in self.model_sequence:
                for sequence in self.models[model_name]:
                    sequence.release_lock()

        diff_time = t.stop_and_get_ms()
        matched_data = {
            "labels": matched_l,
            "model_names": matched_model_names,
            "confidences": matched_c,
            "frame_id": matched_frame_id,
            "type": matched_detection_types,
            "boxes": matched_b,
            # "bbox2poly": matched_poly,
            "image_dimensions": self.media.image_dimensions(),
            # 'stored_factors': (old_x_factor, old_y_factor),
            "polygons": self.zones,
            "error_boxes": matched_e,
            # "err2poly": matched_err_poly,
            "image": matched_frame_img,
        }

        mon_name = f"'Monitor': {g.mon_name} ({g.mid})->'Event': "
        g.log.debug(
            f"perf:{lp}FINAL: {mon_name if isinstance(stream, int) else ''}"
            f"{stream} -> complete detection sequence took: {diff_time}"
        )
        if str2bool(mpd):
            # Write detections to the MPD buffer to be evaluated next time

            if filtered_extras:
                if matched_b and matched_b not in mpd_b:
                    mpd_b.extend(matched_b)
                    mpd_c.extend(matched_c)
                    mpd_l.extend(matched_l)
                    g.log.debug(
                        f"mpd: adding the current detection to the match_past_detection buffer!"
                    )
            mpd_pkl("write", mpd_b, mpd_l, mpd_c, g.eid)
        self.media.stop()
        # if invoked again, we need to resize polys
        self.has_rescaled = False
        return matched_data, all_matches, all_frames
