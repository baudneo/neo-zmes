from enum import Enum
from os import getuid
from pathlib import Path
from typing import Optional

import cv2
import numpy as np

from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.pyzm_utils import Timer, str2bool
from pyzm.ml.mlobject import MLObject

from pydantic import BaseModel, Field, validator

"""
        - name: 'DarkNet::v4 Pre-Trained'
#          enabled: 'no'
          # sequence options
          # Override the name shown if show_models is enabled
          show_name: 'Yolo4'
          # .weights need a .cfg file, .onnx does not
          object_config: '{{yolo4_object_config}}'
          object_weights: '{{yolo4_object_weights}}'
          # The labels file, coco.names usually
          object_labels: '{{yolo4_object_labels}}'
          object_framework: '{{yolo4_object_framework}}'
          object_processor: '{{yolo4_object_processor}}'
          gpu_max_processes: '{{gpu_max_processes}}'
          gpu_max_lock_wait: '{{gpu_max_lock_wait}}'
          cpu_max_processes: '{{cpu_max_processes}}'
          cpu_max_lock_wait: '{{cpu_max_lock_wait}}'
          # Only applies to opencv framework and GPU , default is FP32 TURN OFF IF YOU SEE 'NaN' errors in YOLOv4
          fp16_target: '{{fp16_target}}'
          # currently this is a global setting turned on by just setting it to : yes
          show_models: '{{show_models}}'
          model_height: 512  # For YOLO, default is 416, even though .cfg was trained using 608
          model_width: 512  # For YOLO, default is 416, even though .cfg was trained using 608
          sequence_filters:
            object_min_confidence: '{{object_min_confidence}}'
            # filters set here will be applied to this sequence only, this overrides global_filters and model_filters
            match_past_detections: '{{match_past_detections}}'
            past_det_max_diff_area: '{{past_det_max_diff_area}}'
            confidence_upper: '{{confidence_upper}}'
            person_past_det_max_diff_area: '{{person_past_det_max_diff_area}}'
            car_past_det_max_diff_area: '{{car_past_det_max_diff_area}}'
"""

class MLFrameWork(Enum):
    OPENCV = "opencv"
    CORAL = "coral"

class MLProcessor(Enum):
    CPU = "cpu"
    GPU = "gpu"
    TPU = "tpu"

class SequenceFilters(BaseModel):
    label: str = Field(None, description="The label to filter on")
    match_past_detections: bool = False
    past_det_max_diff_area: int = 0
    confidence_upper: float = 0.0
    min_confidence: float = 0.0

    __overrides__: dict = {}


class SequenceOptions(BaseModel):
    name: str
    model_file = Path
    labels_file = Path
    framework: MLFrameWork = Field(MLFrameWork.OPENCV, alias="framework")
    processor: MLProcessor = Field(MLProcessor.CPU, alias="processor")
    max_processes: int = Field(1, alias="max_processes")
    max_lock_wait: int = Field(120, alias="max_lock_wait")
    fp16_target: bool = Field(False, alias="fp16_target")
    height: int = Field(416, alias="height")
    width: int = Field(416, alias="width")
    enabled: bool = True
    show_name: str = None
    config_file: Path = None
    filters: SequenceFilters = None

# Constants
LP: str = "yolo:"
g: GlobalConfig

YOLO_RESOLUTIONS: dict = {
    # Default resolutions for model based on version
    4: (416, 416),  # <= 4
    6: (640, 640),  # > 4 <= 6
}


class YoloException(Exception):
    def __init__(self, message="My default message", *args):
        # g.logger.error(message)
        message = f"{LP} {message}"
        super().__init__(message, *args)


# cv2 version check for unconnected layers fix
def cv2_version() -> int:
    # Sick and tired of OpenCV playing games....
    maj, min, patch = "", "", ""
    x = cv2.__version__.split(".")
    x_len = len(x)
    if x_len <= 2:
        maj, min = x
        patch = "0"
    elif x_len == 3:
        maj, min, patch = x
        patch = patch.replace("-dev", "") or "0"
    else:
        g.log.error(f"come and fix me again, cv2.__version__.split(\".\")={x}")
    return int(maj + min + patch)


class Yolo(MLObject):
    def __init__(self, *args, **kwargs):
        global g, LP
        # Log Prefix and the global config
        self.lp = LP = "yolo:"
        g = GlobalConfig()
        # Model init params
        self.options: dict = kwargs.get("options", {})
        if not self.options:
            raise YoloException(f"no options passed!")
        # Call parent constructor for its class methods
        super().__init__(*args, **kwargs)
        self.original_image: Optional[np.ndarray] = None
        self.version: int = 3
        self.model_name = "Yolo"
        self.net: Optional[cv2.dnn] = None
        self.model: Optional[cv2.dnn_DetectionModel] = None
        self.classes = None
        self.is_locked: bool = False
        self.sequence_name: str = ""
        g.log.debug(4, f"{LP} initialization params: {self.options}")

        self.processor: str = self.options.get("object_processor", "cpu")
        self.lock_maximum: int = int(
            self.options.get(f"{self.processor}_max_processes", 1)
        )
        self.lock_timeout: int = int(
            self.options.get(f"{self.processor}_max_lock_wait", 120)
        )
        self.lock_name = f"pyzm_uid{getuid()}_{self.processor.upper()}_lock"
        self.disable_locks = self.options.get("disable_locks", "no")

        self.input_height = self.options.get("model_height", 416)
        self.input_width = self.options.get("model_width", 416)

    def get_sequence_name(self) -> str:
        return self.sequence_name

    def get_options(self, key=None):
        if not key:
            return self.options
        else:
            return self.options.get(key)

    def get_classes(self):
        return self.classes

    def populate_class_labels(self):
        if self.options.get("object_labels"):
            labels_file = Path(self.options.get("object_labels"))
            if labels_file.is_file():
                with labels_file.open("r") as f:
                    self.classes = [line.strip() for line in f.readlines()]
            else:
                g.log.error(
                    f"{LP} the specified labels file '{labels_file}' does not exist/is not a file! "
                    f"Can not populate labels"
                )
                raise YoloException(
                    f"The specified labels file does not exist or is not a file! "
                    f"({self.options.get('object_labels')})"
                )

    def load_model(self):
        self.sequence_name = self.options.get("name")
        g.log.debug(f"{LP} loading model data from sequence '{self.sequence_name}'")
        t = Timer()
        model_input_file = self.options.get("object_weights")
        model_config_file = self.options.get("object_config")
        try:
            if not Path(model_input_file).is_file():
                raise YoloException(
                    f"The model input file does not exist or is not a file!"
                )
            elif Path(model_input_file).suffix == ".onnx":
                g.log.debug(
                    f"DBG => model input is ONNX format file, no .cfg file is needed!"
                )
                self.net = cv2.dnn.readNetFromONNX(model_input_file)
            elif Path(model_input_file).suffix == ".weights":
                g.log.dbg(
                    f"DBG => model file is .weights format, .cfg file is required!"
                )
                if not model_config_file or not Path(model_config_file).is_file():
                    raise YoloException(
                        f"The config file does not exist or is not a file!"
                    )
                self.net = cv2.dnn.readNet(model_input_file, model_config_file)
        except Exception as model_load_exc:
            g.log.error(
                f"{LP} Error while loading model file and/or config! (May need to re-download the model/cfg file) => {model_load_exc}"
            )
            raise YoloException(repr(model_load_exc))
        self.model = cv2.dnn_DetectionModel(self.net)
        self.model.setInputParams(
            scale=1 / 255, size=(self.input_width, self.input_height), swapRB=True
        )
        g.log.debug(
            f"perf:{LP} '{self.sequence_name}' initialization -> loading "
            f"'{Path(model_input_file)}' took: {t.stop_and_get_ms()}"
        )
        self.version = Path(model_input_file).name.strip().lower().split("yolov")[1][0]
        self.version = int(self.version)
        if self.get_options("show_name"):
            self.model_name = self.get_options("show_name")
        else:
            self.model_name = (
                "TinyYolo"
                if Path(model_input_file).name.lower().find("tiny") > 0
                else self.model_name
            )
            self.model_name = f"{self.model_name}V{self.version}"
        g.log.dbg(
            f"{LP} model name: {self.model_name} version detected: '{self.version}'"
        )

        if self.processor == "gpu":
            cv_ver = cv2_version()
            # 4.5.4 and above (@pliablepixels tracked down the exact version change
            # see https://github.com/ZoneMinder/mlapi/issues/44)
            if cv_ver < 454 and self.version >= 5:
                g.log.warning(
                    f"{LP} OpenCV (4.5.4+) required for YOLO v5 + detections!"
                )
            #     self.new_cv_scalar_fix = True
            if cv_ver < 420:
                g.log.error(
                    f"You are using OpenCV version {cv2.__version__} which does not support CUDA for DNNs. A minimum"
                    f" of 4.2 is required. See https://medium.com/@baudneo/install-zoneminder-1-36-x-6dfab7d7afe7"
                    f" on how to compile and install openCV 4.5.4 with CUDA"
                )
                self.processor = "cpu"
            else:  # Passed opencv version check, using GPU
                self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
                if str2bool(self.options.get("fp16_target")):
                    self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)
                    g.log.debug(
                        f"{LP} half precision floating point (FP16) cuDNN target enabled (turn this off if it"
                        f" makes yolo slower, you will notice if it does!)"
                    )
                else:
                    self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

        g.log.debug(
            f"{LP} using {self.processor.upper()} for detection"
            f"{', set CUDA/cuDNN backend and target' if self.processor.lower() == 'gpu' else ''}"
        )
        self.populate_class_labels()

    def parse_detection_outputs(
        self, input_image, output_data, conf_threshold, nms_threshold
    ):
        class_ids, confidences, boxes = [], [], []
        rows = output_data.shape[0]
        image_width, image_height = input_image.shape[:2]
        x_factor = image_width / self.input_width
        y_factor = image_height / self.input_height
        g.log.dbg(
            f"DBG -=> INSIDE wrap_detection()! {range(rows) = } - {x_factor = } - {y_factor = }"
        )
        for r in range(rows):
            row = output_data[r]
            confidence = row[4]
            if confidence >= conf_threshold:
                classes_scores = row[5:]
                _, _, _, max_indx = cv2.minMaxLoc(classes_scores)
                class_id = max_indx[1]
                if classes_scores[class_id] > conf_threshold:
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
                    x, y, w, h = (
                        row[0].item(),
                        row[1].item(),
                        row[2].item(),
                        row[3].item(),
                    )
                    left = int((x - 0.5 * w) * x_factor)
                    top = int((y - 0.5 * h) * y_factor)
                    width = int(w * x_factor)
                    height = int(h * y_factor)
                    box = [left, top, width, height]
                    boxes.append(box)
        indexes = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
        result_class_ids, result_confidences, result_boxes = [], [], []
        for i in indexes:
            result_confidences.append(confidences[i])
            result_class_ids.append(class_ids[i])
            result_boxes.append(boxes[i])

        return result_class_ids, result_confidences, result_boxes

    @staticmethod
    def square_image(frame):
        """Zero pad the array to make the image squared"""
        g.log.dbg(
            f"DBG -=> INSIDE square_image(), frame shape before padding: {frame.shape}"
        )
        row, col, _ = frame.shape
        _max = max(col, row)
        result = np.zeros((_max, _max, 3), np.uint8)
        result[0:row, 0:col] = frame
        g.log.dbg(
            f"DBG -=> INSIDE square_image(), frame shape after padding: {result.shape}"
        )
        return result

    def detect(self, input_image: Optional[np.ndarray] = None, retry: bool = False):
        if input_image is None:
            g.log.error(f"{LP} no image passed!?!")
            raise YoloException("NO_IMAGE")
        self.create_lock()  # create lock if not already created
        blob, outs = None, None
        class_ids, confidences, boxes = [], [], []
        bbox, label, conf = [], [], []
        squared = str2bool(self.options.get("square_image"))
        if squared:
            input_image = self.square_image(input_image)
        h, w = input_image.shape[:2]
        nms_threshold, conf_threshold = 0.4, 0.2
        filters = self.options.get("sequence_filters", {})
        g.log.dbg(
            f"DBG -=> INSIDE detect(), MIN CONF: {filters.get('object_min_confidence')} <-> "
            f"NMS: {filters.get('object_nms_threshold')}"
        )
        min_conf = filters.get("object_min_confidence", 0.0)
        nms_conf = filters.get("object_nms_threshold", 0.0)
        configured_conf_thresh = float(min_conf)
        configured_nms_thresh = float(nms_conf)
        if configured_conf_thresh and (configured_conf_thresh < conf_threshold):
            g.log.warn(
                f"{LP} confidence threshold too low (less than 20%), setting to {conf_threshold}"
            )
        else:
            conf_threshold = configured_conf_thresh
        if configured_nms_thresh and isinstance(configured_nms_thresh, float):
            nms_threshold = configured_nms_thresh

        g.log.dbg(f"{LP} confidence threshold: {conf_threshold} - NMS threshold: {nms_threshold}")
        try:
            self.acquire_lock()
            if not self.net or (self.net and retry):
                # model has not been loaded or this is a retry detection, so we want to rebuild
                # the model with changed options.
                g.log.dbg(
                    f"DEBUGGING - self.net? {'yes' if self.net else 'no'} -- {retry = }"
                )
                self.load_model()
            g.log.debug(
                f"{LP} '{self.sequence_name}' ({self.processor.upper()}) - input image {w}*{h}"
                f"{' [squared]' if squared else ''} - model input set as: "
                f"{self.input_width}*{self.input_height}"
            )
            t = Timer()
            if self.options.get("square_images", False):
                g.log.dbg(
                    f"DBG => creating a squared zero-padded image for YOLO {self.version} input,"
                    f" iterating forwarded layers ourself as well"
                )
                input_image = self.square_image(input_image)
                blob = cv2.dnn.blobFromImage(
                    input_image,
                    scalefactor=1 / 255,
                    size=(self.input_width, self.input_height),
                    swapRB=True,
                )
                self.net.setInput(blob)
                outs = self.net.forward()
                g.log.dbg(f"DEBUG>>> time to forward pass: {t.get_ms()}")
                class_ids, confidences, boxes = self.parse_detection_outputs(
                    input_image, outs[0], conf_threshold, nms_threshold
                )
                ##################################
                # image_width, image_height = input_image.shape[:2]
                # x_factor = image_width / self.input_width
                # y_factor = image_height / self.input_height
                #
                # x, y, w, h = (
                #     row[0].item(),
                #     row[1].item(),
                #     row[2].item(),
                #     row[3].item(),
                # )
                # left = int((x - 0.5 * w) * x_factor)
                # top = int((y - 0.5 * h) * y_factor)
                # width = int(w * x_factor)
                # height = int(h * y_factor)
                # box = [left, top, width, height]
                # boxes.append(box)
                ##################################

                for (classid, confidence, box) in zip(class_ids, confidences, boxes):
                    # label, bbox, conf
                    x = box[0]
                    y = box[1]
                    w_ = box[2]
                    h_ = box[3]
                    bbox.append(
                        [
                            x,
                            y,
                            x + w_,
                            y + h_,
                        ]
                    )
                    label.append(self.classes[classid])
                    conf.append(confidence)
                g.log.dbg(f"DEBUG>>> time to parse detection outputs: {t.get_ms()}")

            else:

                class_ids, confidences, boxes = self.model.detect(
                    input_image, conf_threshold, nms_threshold
                )
                g.log.dbg(f"DEBUG>>> time to built in detect: {t.get_ms()}")
                for (class_id, confidence, box) in zip(class_ids, confidences, boxes):
                    confidence = float(confidence)
                    if confidence >= conf_threshold:
                        x, y, _w, _h = (
                            int(round(box[0])),
                            int(round(box[1])),
                            int(round(box[2])),
                            int(round(box[3])),
                        )
                        bbox.append(
                            [
                                x,
                                y,
                                x + _w,
                                y + _h,
                            ]
                        )
                        label.append(self.classes[class_id])
                        conf.append(confidence)
                g.log.dbg(f"DEBUG>>> time to parse detection outputs: {t.get_ms()}")
        except Exception as all_ex:
            err_msg = repr(all_ex)
            # cv2.error: OpenCV(4.2.0) /home/<Someone>/opencv/modules/dnn/src/cuda/execution.hpp:52: error: (-217:Gpu
            # API call) invalid device function in function 'make_policy'
            g.log.error(f"{LP} exception during detection -> {all_ex}")
            if (
                err_msg.find("-217:Gpu") > 0
                and err_msg.find("'make_policy'") > 0
                and self.options["object_processor"] == "gpu"
            ):
                g.log.error(
                    f"{LP} (-217:Gpu # API call) invalid device function in function 'make_policy' - "
                    f"This happens when OpenCV is compiled with the incorrect Compute Capability "
                    f"(CUDA_ARCH_BIN). There is a high probability that you need to recompile OpenCV with "
                    f"the correct CUDA_ARCH_BIN before GPU detections will work properly!"
                )
                # set arch to cpu and retry?
                self.options["object_processor"] = "cpu"
                g.log.info(
                    f"{LP} GPU detection failed due to probable incorrect CUDA_ARCH_BIN (Compute Capability)."
                    f" Switching to CPU and retrying detection!"
                )
                self.detect(input_image, retry=True)
            raise YoloException(f"during detection")
        finally:
            self.release_lock()
        diff_time = t.stop_and_get_ms()
        g.log.debug(
            2,
            f"perf:{LP}{self.processor.upper()}: '{self.sequence_name}' detection took: {diff_time}",
        )
        _model_name = [f"{self.model_name}[{self.processor.upper()}]"] * len(label)
        if label:
            g.log.debug(f"{LP} {label} -- {bbox} -- {conf}")
            g.log.debug(f"DBG => {_model_name = }")

        else:
            g.log.debug(f"{LP} no detections to return!")

        return bbox, label, conf, _model_name
        # from collections import namedtuple
        # detection = namedtuple("detection", "label bbox conf model_name")
        # detections = []
        # for i in range(len(label)):
        #     detections.append(
        #         detection(
        #             label=label[i],
        #             bbox=bbox[i],
        #             conf=conf[i],
        #             model_name=_model_name[i],
        #         )
        #     )
        # return detections
