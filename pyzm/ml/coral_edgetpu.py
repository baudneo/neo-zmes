from os import getuid
from pathlib import Path

import cv2
import numpy as np
from PIL import Image

from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.pyzm_utils import Timer
from pyzm.ml.mlobject import MLObject

g: GlobalConfig
lp: str

# global placeholders for TPU lib imports
common = None
detect = None
make_interpreter = None


class TPUException(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        super().__init__(self.message)

    def __str__(self):
        if self.message:
            return self.message
        else:
            return "TPUException has been raised"


class Tpu(MLObject):
    def __init__(self, *args, **kwargs):
        global g, lp
        g = GlobalConfig()
        self.lp = lp = "coral:"
        self.options = None
        try:
            global common, detect, make_interpreter
            from pycoral.adapters import common as common, detect as detect
            from pycoral.utils.edgetpu import make_interpreter as make_interpreter, list_edge_tpus
        except ImportError:
            from platform import python_version
            allowed_ = ("3.6", "3.7", "3.8", "3.9", "3.10")
            if not python_version().startswith(allowed_):
                g.log.error(
                    f"{lp} The pycoral Coral Libs are not compatible with Python >3.6 or <3.10 as of August 20 2022, "
                    f"use 3.6 - 3.10. The tensorflow wheels are not available for the pycoral team to support "
                    f"{python_version()}. See https://github.com/google-coral/pycoral/issues/58 for more details."
                )
            else:
                g.log.warning(
                    f"{lp} pycoral libs not installed, this is ok if you do not plan to use "
                    f"TPU as detection processor. If you intend to use a TPU please install the TPU libs "
                    f"and pycoral!"
                )
        else:
            g.log.debug(f"{lp} the pycoral library has been successfully imported!")
            options = kwargs["options"]
            self.options = options

            super().__init__(*args, **kwargs)

            if options is None:
                g.log.error(
                    f"{lp} cannot initialize TPU object detection model -> no 'sequence' sent with weights, conf, "
                    f"labels, etc."
                )
                raise TPUException(f"{lp} -> NO OPTIONS")
            g.log.debug(f"{lp} initializing edge TPU with params: {options}")
            self.sequence_name: str = ""

            self.classes = {}
            self.processor = "tpu"
            self.lock_maximum = int(options.get(f"{self.processor}_max_processes", 1))
            self.lock_name = f"pyzm_uid{getuid()}_{self.processor.upper()}_lock"
            self.lock_timeout = int(options.get(f"{self.processor}_max_lock_wait", 120))
            self.disable_locks = options.get("disable_locks", "no")
            self.create_lock()
            self.is_locked = False
            self.model = None
            self.model_height = self.options.get("model_height", 312)
            self.model_width = self.options.get("model_width", 312)
            self.populate_class_labels()

    @staticmethod
    def get_model_name() -> str:
        return "coral"

    def get_options(self, key=None):
        if not key:
            return self.options
        else:
            return self.options.get(key)

    def get_sequence_name(self) -> str:
        return self.sequence_name

    def populate_class_labels(self):
        label_file = self.options.get("object_labels")
        if label_file and Path(label_file).is_file():
            fp = None
            try:
                fp = open(label_file)
            except Exception as exc:
                g.log.error(f"{lp} error while trying to open the 'object labels' file '{label_file}' -> \n{exc}")
            else:
                for row in fp:
                    # unpack the row and update the labels dictionary
                    (classID, label) = row.strip().split(" ", maxsplit=1)
                    self.classes[int(classID)] = label.strip()
            finally:
                if fp:
                    fp.close()
        elif not Path(label_file).is_file():
            g.log.error(f"{lp} '{Path(label_file).name}' does not exist or is not an actual file")
            raise TPUException(
                f"Provided label file does not exist or is not a file! Check the config for any spelling mistakes in the entire Path"
            )
        else:
            g.log.debug(f"{lp} No label file provided for this model")
            raise TPUException(
                f"Provided label file does not exist or is not a file! Check the config for any spelling mistakes in the entire Path"
            )

    def get_classes(self):
        return self.classes

    def load_model(self):
        # print(f"{self.options = }")
        self.sequence_name = self.options.get("name") or self.get_model_name()
        g.log.debug(f"{lp} loading model data from sequence '{self.sequence_name}'")
        # self.model = DetectionEngine(self.options.get('object_weights'))
        # Initialize the TF interpreter
        t = Timer()
        weights = Path(self.options.get("object_weights"))
        if weights and weights.is_file():
            g.log.dbg(f"{lp} loading model weights from '{weights.absolute() = }'")
            try:
                self.model = make_interpreter(weights.as_posix())
            except Exception as ex:
                ex = repr(ex)
                tokens = ex.split(" ")
                for tok in tokens:
                    if tok.startswith("libedgetpu"):
                        g.log.error(
                            f"{lp} TPU error detected (replace cable with a short high quality one, dont allow "
                            f"TPU/cable to move around). Reset the USB port or reboot!"
                        )
                        raise TPUException("TPU NO COMM")
            else:
                self.model.allocate_tensors()
                diff_time = t.stop_and_get_ms()
                g.log.debug(
                    f"perf:{lp} initialization -> loading '{Path(self.options.get('object_weights')).name}' "
                    f"took: {diff_time}"
                )
        else:
            g.log.error(
                f"{lp} The supplied model file does not exist or is not an actual file. Can't run detection!"
            )

    def detect(self, input_image=None):
        from pyzm.helpers.pyzm_utils import str_2_percentile
        orig_h, orig_w = h, w = input_image.shape[:2]
        x_factor = None
        y_factor = None
        model_resize = False
        if self.model_height and self.model_width:
            model_resize = True
        elif self.model_height and not self.model_width:
            self.model_width = orig_w
            model_resize = True
        elif not self.model_height and self.model_width:
            self.model_height = orig_h
            model_resize = True
        filters = self.options.get("sequence_filters", {})
        min_conf = str_2_percentile(filters.get("object_min_confidence", 0.2))
        bbox, labels, conf = [], [], []

        if model_resize:
            g.log.debug(2, f"{lp} model dimensions requested -> " f"{self.model_width}*{self.model_height}")
            input_image = cv2.resize(
                input_image, (int(self.model_width), int(self.model_height)), interpolation=cv2.INTER_AREA
            )
            # get scaling so we can make correct bounding boxes
            x_factor = w / self.model_width
            y_factor = h / self.model_height

        input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
        input_image = Image.fromarray(input_image)
        if self.options.get("auto_lock", True):
            self.acquire_lock()
        if not self.model:
            self.load_model()

        g.log.debug(
            f"{lp} '{self.sequence_name}' input image (w*h): {orig_w}*{orig_h} resized by model_width/height "
            f"to {self.model_width}*{self.model_height}"
        )
        t = Timer()
        try:
            _, scale = common.set_resized_input(
                self.model, input_image.size, lambda size: input_image.resize(size, Image.ANTIALIAS)
            )
            self.model.invoke()

        except Exception as ex:
            g.log.error(f"EXCEPTION THROWN-> {ex}")
            raise ex
        else:
            objs = detect.get_objects(self.model, min_conf, scale)
            diff_time = t.stop_and_get_ms()
            g.log.debug(f"perf:{lp} '{self.sequence_name}' detection took: {diff_time}")
            g.log.debug(f"DBG>>> {type(objs)} {objs = }")
            for obj in objs:
                g.log.debug(f"DBG>>> {type(obj)} {obj = }")
                # box = obj.bbox.flatten().astype("int")
                bbox.append(
                    [
                        int(round(obj.bbox.xmin)),
                        int(round(obj.bbox.ymin)),
                        int(round(obj.bbox.xmax)),
                        int(round(obj.bbox.ymax)),
                    ]
                )

                labels.append(self.classes.get(obj.id))
                conf.append(float(obj.score))

        finally:
            if self.options.get("auto_lock", True):
                self.release_lock()



        if labels:
            if model_resize:
                g.log.debug(
                    2,
                    f"{lp} The image was resized before processing by the 'model width/height', scaling "
                    f"bounding boxes in image back by factors of -> x={x_factor:.4} "
                    f"y={y_factor:.4}",
                )
                bbox = self.scale(bbox, x_factor, y_factor)
            g.log.debug(f"{lp} returning {labels} -- {bbox} -- {conf}")
        else:
            g.log.debug(f"{lp} no detections to return!")
        return bbox, labels, conf, ["coral"] * len(labels)  # , ret_val
