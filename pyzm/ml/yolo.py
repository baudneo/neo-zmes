from copy import deepcopy
from os import getuid
from pathlib import Path
from typing import Optional

import cv2
import numpy as np

from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.pyzm_utils import Timer, str2bool
from pyzm.ml.mlobject import MLObject

# Constants
LP: str = "yolo:"
g: GlobalConfig


class YoloException(Exception):
    def __init__(self, message="My default message", *args):
        # g.logger.error(message)
        super().__init__(message, *args)


class Yolo(MLObject):
    # the actual cnn object detection code
    # opencv DNN code credit: https://github.com/arunponnusamy/cvlib
    def __init__(self, *args, **kwargs):
        global g, LP
        self.lp = LP = "yolo:"
        g = GlobalConfig()
        self.options: dict = kwargs.get("options", {})
        if not self.options:
            raise YoloException(f"YOLO no options passed!")
        super().__init__(*args, **kwargs)

        self.original_image: Optional[np.ndarray] = None

        # UnConnectedLayers fix
        self.new_cv_scalar_fix = False
        self.net = None
        self.version: int = 3

        self.model_name = "Yolo"
        self.classes = None
        self.is_locked: bool = False
        self.sequence_name: str = ""
        g.log.debug(4, f"{LP} initialization params: {self.options}")

        self.processor: str = self.options.get("object_processor", "cpu")
        self.lock_maximum: int = int(
            self.options.get(f"{self.processor}_max_processes", 1)
        )
        self.lock_timeout: int = int(
            self.options.get(f"{self.processor}_max_lock_wait", 120)
        )
        self.lock_name = f"pyzm_uid{getuid()}_{self.processor.upper()}_lock"
        self.disable_locks = self.options.get("disable_locks", "no")
        # method from superclass to reduce duplicate code
        self.create_lock()
        # yolo needs to scale the bounding boxes based on the w/h of the image
        self.model_height = self.options.get("model_height", 416)
        self.model_width = self.options.get("model_width", 416)

    def get_model_name(self) -> str:
        return f"yolo-{self.processor}"

    @staticmethod
    def view_image(v_img):
        cv2.imwait("image.jpg", v_img)
        cv2.waitKey(0)

    def get_sequence_name(self) -> str:
        return self.sequence_name

    def get_options(self, key=None):
        if not key:
            return self.options
        else:
            return self.options.get(key)

    def get_classes(self):
        return self.classes

    def populate_class_labels(self):
        if self.options.get("object_labels"):
            labels_file = Path(self.options.get("object_labels"))
            if labels_file.is_file():
                with labels_file.open("r") as f:
                    self.classes = [line.strip() for line in f.readlines()]
            else:
                g.log.error(
                    f"{LP} the specified labels file '{labels_file.name}' does not exist/is not a file! "
                    f"Can not populate labels"
                )
                raise YoloException(
                    f"The specified labels file does not exist or is not a file! "
                    f"({self.options.get('object_labels')})"
                )

    def load_model(self):
        self.sequence_name = self.options.get("name")
        g.log.debug(f"{LP} loading model data from sequence '{self.sequence_name}'")
        t = Timer()
        model_input_file = self.options.get("object_weights")
        model_config_file = self.options.get("object_config")
        if not Path(model_input_file).is_file():
            raise YoloException(
                f"The model input file does not exist or is not a file!"
            )
        elif Path(model_input_file).suffix == ".onnx":
            g.log.debug(f"DBG => This is an ONNX format file, no .cfg file is needed!")
            self.net = cv2.dnn.readNetFromONNX(model_input_file)
        elif Path(model_input_file).suffix == ".weights":
            g.log.dbg(
                f"DBG => model file is in .weights format! -> {model_config_file = } -- {model_input_file = }"
            )
            if not model_config_file or not Path(model_config_file).is_file():
                raise YoloException(f"The config file does not exist or is not a file!")
            self.net = cv2.dnn.readNet(model_input_file, model_config_file)
        # self.net = cv2.dnn.readNetFromDarknet(config_file_abs_path, weights_file_abs_path)
        g.log.debug(
            f"perf:{LP} '{self.sequence_name}' initialization -> loading "
            f"'{Path(model_input_file)}' took: {t.stop_and_get_ms()}"
        )
        self.version = Path(model_input_file).name.strip().lower().split("yolov")[1][0]
        self.model_name = (
            "TinyYolo" if Path(model_input_file).name.find("tiny") > 0 else self.model_name
        )
        g.log.dbg(
            f"DBG => Model detected as '{self.model_name = }' Version detected as '{self.version}'"
            f" ----- {Path(model_input_file).name.strip().lower().split('yolov') = }"
        )
        if self.processor == "gpu":
            (maj, minor, patch) = cv2.__version__.split(".")
            min_ver = int(maj + minor)
            patch = patch if patch.isnumeric() else 0
            patch_ver = int(maj + minor + patch)
            # 4.5.4 and above (@pliablepixels tracked down the exact version change
            # see https://github.com/ZoneMinder/mlapi/issues/44)
            # @baudneo linked issue -> https://github.com/baudneo/pyzm/issues/3
            if patch_ver >= 454:
                g.log.info(
                    f"{LP} OpenCV (4.5.4+) fix for getUnconnectedOutLayers() API (Non nested structure - "
                    f"Thanks @pliablepixels !)"
                )
                self.new_cv_scalar_fix = True

            if min_ver < 42:
                g.log.error(
                    f"You are using OpenCV version {cv2.__version__} which does not support CUDA for DNNs. A minimum"
                    f" of 4.2 is required. See https://medium.com/@baudneo/install-zoneminder-1-36-x-6dfab7d7afe7"
                    f" on how to compile and install openCV 4.5.4 with CUDA"
                )
                self.processor = "cpu"
            else:  # Passed opencv version check, using GPU
                self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
                if str2bool(self.options.get("fp16_target")):
                    self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)
                    g.log.debug(
                        f"{LP} half precision floating point (FP16) cuDNN target enabled (turn this off if it"
                        f" makes yolo slower, you will notice if it does!)"
                    )
                else:
                    self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

        g.log.debug(
            f"{LP} using {self.processor.upper()} for detection"
            f"{', set CUDA/cuDNN backend and target' if self.processor.lower() == 'gpu' else ''}"
        )
        self.populate_class_labels()

    def wrap_detection(self, input_imagev5, output_data, conf_threshold):
        class_ids = []
        confidences = []
        boxes = []

        rows = output_data.shape[0]

        image_width, image_height, _ = input_imagev5.shape

        x_factor = image_width / self.model_width
        y_factor = image_height / self.model_height
        g.log.dbg(f"DBG -=> INSIDE wrap_detection()! {range(rows) = }")
        for r in range(rows):
            row = output_data[r]
            confidence = row[4]
            if confidence >= conf_threshold:
                classes_scores = row[5:]
                _, _, _, max_indx = cv2.minMaxLoc(classes_scores)
                class_id = max_indx[1]
                if classes_scores[class_id] > 0.25:
                    confidences.append(confidence)

                    class_ids.append(class_id)

                    x, y, w, h = (
                        row[0].item(),
                        row[1].item(),
                        row[2].item(),
                        row[3].item(),
                    )
                    left = int((x - 0.5 * w) * x_factor)
                    top = int((y - 0.5 * h) * y_factor)
                    width = int(w * x_factor)
                    height = int(h * y_factor)
                    box = np.array([left, top, width, height])
                    boxes.append(box)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.45)

        result_class_ids = []
        result_confidences = []
        result_boxes = []

        for i in indexes:
            result_confidences.append(confidences[i])
            result_class_ids.append(class_ids[i])
            result_boxes.append(boxes[i])

        return result_class_ids, result_confidences, result_boxes

    def format_yolov5(self, frame):
        row, col, _ = frame.shape
        _max = max(col, row)
        result = np.zeros((_max, _max, 3), np.uint8)
        result[0:row, 0:col] = frame
        return result

    def detect(self, input_image: Optional[np.ndarray] = None, retry: bool = False):
        if input_image is None:
            g.log.error(f"{LP} no image passed!?!")
            raise YoloException("NO_IMAGE")
        if not retry:
            self.original_image = deepcopy(input_image)
            # self.original_image = np.ndarray.astype(self.original_image, np.float32)

        indices = None
        ln = None
        blob = None
        outs = None
        downscaled = False
        bbox, label, conf = [], [], []
        upsize_x_factor = None
        upsize_y_factor = None

        h, w = input_image.shape[:2]
        max_size = self.options.get("max_size")
        max_size = w if not max_size else max_size  # Account for 0 or None or whatever
        if not isinstance(max_size, str) or (
            isinstance(max_size, str) and max_size.isnumeric()
        ):
            try:
                max_size = int(max_size)
            except TypeError or ValueError:
                g.log.error(f"{LP} 'max_size' can only be an integer -> 123 not 123.xx")
                max_size = w
            else:
                if w > max_size:
                    downscaled = True
                    from pyzm.helpers.pyzm_utils import resize_image

                    g.log.debug(
                        2, f"{LP} scaling image down to max (width) size: {max_size}"
                    )
                    input_image = resize_image(input_image, max_size)
                    new_height, new_width = input_image.shape[:2]
                    upsize_x_factor = w / new_width
                    upsize_y_factor = h / new_height

        h, w = input_image.shape[:2]
        try:
            if self.options.get("auto_lock", True):
                self.acquire_lock()
            if not self.net or (self.net and retry):
                # model has not been loaded or this is a retry detection, so we want to rebuild
                # the model with changed options.
                self.load_model()
            g.log.debug(
                f"{LP} '{self.sequence_name}' ({self.processor.upper()}) - input image {w}*{h} - resized by "
                f"model_width/height to: {self.model_width}*{self.model_height}"
            )
            scale = 0.00392  # 1/255, really. Normalize inputs.
            t = Timer()
            ln = self.net.getLayerNames()
            if not self.new_cv_scalar_fix:
                try:
                    ln = [ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
                except Exception as all_ex:
                    g.log.error(
                        f"{LP} OpenCV 4.5.4+ 'getUnconnectedOutLayers()' API fix did not work! Please do some testing "
                        f"to see if you have installed the opencv-python pip/pip3 packages by mistake if you compiled "
                        f"OpenCV for GPU/CUDA! see https://forums.zoneminder.com/viewtopic.php?f=33&t=31293 \n{all_ex}"
                    )
                    raise all_ex
            else:
                ln = [ln[i - 1] for i in self.net.getUnconnectedOutLayers()]
            if self.version == "5" or self.version == 5:
                g.log.dbg(f"DBG => Padding yolov5 image to make it a square!")
                input_image = self.format_yolov5(input_image)

            blob = cv2.dnn.blobFromImage(
                input_image,
                scale,
                (self.model_width, self.model_height),
                (0, 0, 0),
                True,
                crop=False,
            )

            self.net.setInput(blob)
            if self.version == "5" or self.version == 5:
                outs = self.net.forward()
            else:
                outs = self.net.forward(ln)
        except Exception as all_ex:
            err_msg = repr(all_ex)
            if (
                err_msg.find("-217:Gpu") > 0
                and err_msg.find("'make_policy'") > 0
                and self.options["object_processor"] == "gpu"
            ):
                g.log.error(
                    f"{LP} (-217:Gpu # API call) invalid device function in function 'make_policy' - "
                    f"This happens when OpenCV is compiled with the incorrect Compute Capability "
                    f"(CUDA_ARCH_BIN). There is a high probability that you need to recompile OpenCV with "
                    f"the correct CUDA_ARCH_BIN before GPU detections will work properly!"
                )
                # set arch to cpu and retry?
                self.options["object_processor"] = "cpu"
                g.log.info(
                    f"{LP} GPU detection failed due to probable incorrect CUDA_ARCH_BIN (Compute Capability)."
                    f" Switching to CPU and retrying detection!"
                )
                self.detect(self.original_image, retry=True)
            g.log.error(f"{LP} exception during blobFromImage -> {all_ex}")

            # cv2.error: OpenCV(4.2.0) /home/<Someone>/opencv/modules/dnn/src/cuda/execution.hpp:52: error: (-217:Gpu
            # API call) invalid device function in function 'make_policy'
            raise YoloException(f"blobFromImage")

        finally:
            if self.options.get("auto_lock", True):
                self.release_lock()

        class_ids, confidences, boxes = [], [], []
        nms_threshold, conf_threshold = 0.4, 0.2
        # Non Max Suppressive
        if float(self.options.get("object_min_confidence")) < conf_threshold:
            conf_threshold = float(self.options.get("object_min_confidence"))
        try:
            # [yolo: EXCEPTION while parsing layer output -->
            #   index 264690 is out of bounds for axis 0 with size 25195]
            # yolov5 is passed image and outs[0] in opencv 4.5.4+
            if self.version == "5" or self.version == 5:
                g.log.dbg(
                    f"DBG => YOLO Version 5 detected, using a different layer scheme!"
                )
                class_ids, confidences, boxes = self.wrap_detection(
                    input_image, outs[0], conf_threshold
                )
                label, bbox, conf = [], [], []
                for (classid, confidence, box) in zip(class_ids, confidences, boxes):
                    # label, bbox, conf
                    x = box[0]
                    y = box[1]
                    w_ = box[2]
                    h_ = box[3]
                    bbox.append(
                        [
                            int(round(x)),
                            int(round(y)),
                            int(round(x + w_)),
                            int(round(y + h_)),
                        ]
                    )
                    label.append(self.classes[classid])
                    conf.append(float(confidence))
            else:
                for out in outs:
                    for detection in out:
                        # FP32 and FP16 have different matrices
                        # g.logger.debug(f"dbg:{lp} {detection[:5]=}")
                        scores = detection[5:]
                        class_id = np.argmax(scores)
                        confidence = scores[class_id]
                        center_x = int(detection[0] * w)
                        center_y = int(detection[1] * h)
                        wid = int(detection[2] * w)
                        hei = int(detection[3] * h)
                        x = center_x - wid / 2
                        y = center_y - hei / 2
                        class_ids.append(class_id)
                        confidences.append(float(confidence))
                        boxes.append([x, y, wid, hei])
                indices = cv2.dnn.NMSBoxes(
                    boxes, confidences, conf_threshold, nms_threshold
                )
                label, bbox, conf, box = self.indice_process(
                    boxes,
                    indices,
                    confidences,
                    self.classes,
                    class_ids,
                    self.new_cv_scalar_fix,
                )

        except OverflowError as ex:
            if self.processor == "gpu":
                g.log.debug(
                    f"GPU needs to be reset? try re downloading YOLO models first, if that doesn't work "
                    f"try -> sudo nvidia-smi -r <- to reset the gpu and restart. "
                    f"Google 'how to kill xorg server' if Xorg is holding you up ("
                    f"hint: ctrl+alt+F1 and once done ctrl+alt+F7)"
                )
                g.log.error(f"{LP} OverflowError: {ex}")
                raise YoloException(f"GPU RESET")
            raise ex
        except YoloException as v_exc:
            if repr(v_exc) == "cannot convert float NaN to integer":
                if str2bool(self.options.get("fp16_target")):
                    g.log.error(
                        f"{LP} '{repr(v_exc)}' FP_16 CUDA TARGET is configured. Setting CUDA TARGET to FP_32 and "
                        f"re running detection!"
                    )
                    self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
                    self.detect(self.original_image, retry=True)
                else:
                    g.log.error(
                        f"{LP} 'NaN' (Not a Number) error but the CUDA TARGET is FP32?!?! FATAL!"
                    )
                    raise v_exc
        except Exception as e:
            g.log.error(f"{LP} EXCEPTION while parsing layer output -->\n{e}")
            raise e

        diff_time = t.stop_and_get_ms()
        g.log.debug(
            2,
            f"perf:{LP}{self.processor.upper()}: '{self.sequence_name}' detection took: {diff_time}",
        )

        if downscaled:
            g.log.debug(
                2,
                f"{LP} scaling bounding boxes back up using  x={upsize_x_factor} y={upsize_y_factor}",
            )
            bbox = self.scale(bbox, upsize_x_factor, upsize_y_factor)
        if label:
            g.log.debug(f"{LP} {label} -- {bbox} -- {conf}")
        else:
            g.log.debug(f"{LP} no detections to return!")
        self.original_image = None
        _model_name = [
            f"{self.model_name}v{self.version}[{self.processor.upper()}]"
        ] * len(label)
        g.log.debug(f"DBG => {_model_name = }")
        return bbox, label, conf, _model_name
