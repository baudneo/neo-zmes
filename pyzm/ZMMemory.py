"""
ZMMemory
=====================
Wrapper to access SHM for Monitor status
"""

import mmap
import os
import struct
from _ctypes import Structure
from ctypes import c_long
from collections import namedtuple
from typing import Optional, IO, Union
from sys import getsizeof as raw_size

from pyzm.helpers.GlobalConfig import GlobalConfig

g: GlobalConfig
IMAGE_BUFFER_COUNT: int = 5


class TimeVal(Structure):
    _fields_ = [("tv_sec", c_long), ("tv_usec", c_long)]


# This will compensate for 32/64 bit
TIMEVAL_SIZE: int = int(raw_size(TimeVal()) / 8)

"""
OLD STRUCTURE
shared_data => { type=>'SharedData', seq=>$mem_seq++, contents=> {
    size             => { type=>'uint32', seq=>$mem_seq++ }, I
    last_write_index => { type=>'uint32', seq=>$mem_seq++ }, I
    last_read_index  => { type=>'uint32', seq=>$mem_seq++ }, I
    state            => { type=>'uint32', seq=>$mem_seq++ }, I
    last_event       => { type=>'uint64', seq=>$mem_seq++ }, Q
    action           => { type=>'uint32', seq=>$mem_seq++ }, I
    brightness       => { type=>'int32', seq=>$mem_seq++ },  i
    hue              => { type=>'int32', seq=>$mem_seq++ },  i
    colour           => { type=>'int32', seq=>$mem_seq++ },  i
    contrast         => { type=>'int32', seq=>$mem_seq++ },  i
    alarm_x          => { type=>'int32', seq=>$mem_seq++ },  i
    alarm_y          => { type=>'int32', seq=>$mem_seq++ },  i
    valid            => { type=>'uint8', seq=>$mem_seq++ },  ?
    active           => { type=>'uint8', seq=>$mem_seq++ },  ?
    signal           => { type=>'uint8', seq=>$mem_seq++ },  ?
    format           => { type=>'uint8', seq=>$mem_seq++ },  ?
    imagesize        => { type=>'uint32', seq=>$mem_seq++ }, I
    epadding1        => { type=>'uint32', seq=>$mem_seq++ },  I
    startup_time     => { type=>'time_t64', seq=>$mem_seq++ },  Q
    last_write_time  => { type=>'time_t64', seq=>$mem_seq++ },  Q
    last_read_time   => { type=>'time_t64', seq=>$mem_seq++ },  Q 
    control_state    => { type=>'uint8[256]', seq=>$mem_seq++ }, s256
    alarm_cause      => { type=>'int8[256]', seq=>$mem_seq++ }, s256
"""

"""
size last_write_index last_read_index state capture_fps analysis_fps last_event_id action brightness
 hue colour contrast alarm_x alarm_y valid capturing analysing recording signal format imagesize last_frame_score
  audio_frequency audio_channels startup_time zmc_heartbeat_time last_write_time last_read_time last_viewed_time 
  control_state alarm_cause video_fifo_path audio_fifo_path
  
NEW STRUCTURE
shared_data => { type=>'SharedData', seq=>$mem_seq++, contents=> {
    size             => { type=>'uint32', seq=>$mem_seq++ }, I /* start+0 offset(32bit/4byte)    */
    last_write_index => { type=>'int32', seq=>$mem_seq++ }, i /* start+4 offset    */
    last_read_index  => { type=>'int32', seq=>$mem_seq++ }, i /* +8    */
    state            => { type=>'uint32', seq=>$mem_seq++ }, I /* +12   */
    capture_fps      => { type=>'double', seq=>$mem_seq++ }, d /* +20   */
    analysis_fps     => { type=>'double', seq=>$mem_seq++ }, d /* +28   */
    last_event_id    => { type=>'uint64', seq=>$mem_seq++ }, Q /* +36   */
    action           => { type=>'uint32', seq=>$mem_seq++ }, I /* +40   */
    brightness       => { type=>'int32', seq=>$mem_seq++ },  i /* +44   */
    hue              => { type=>'int32', seq=>$mem_seq++ },  i /* +48   */
    colour           => { type=>'int32', seq=>$mem_seq++ },  i /* +52   */
    contrast         => { type=>'int32', seq=>$mem_seq++ },  i /* +56   */
    alarm_x          => { type=>'int32', seq=>$mem_seq++ },  i /* +60   */
    alarm_y          => { type=>'int32', seq=>$mem_seq++ },  i /* +64   */
    valid            => { type=>'uint8', seq=>$mem_seq++ },  ? /* +65   */
    capturing        => { type=>'uint8', seq=>$mem_seq++ },  ? /* +66   */
    analysing        => { type=>'uint8', seq=>$mem_seq++ },  ? /* +67   */  # IN ZM - not treated as a boolean
    recording        => { type=>'uint8', seq=>$mem_seq++ },  ? /* +68   */
    signal           => { type=>'uint8', seq=>$mem_seq++ },  ? /* +69   */
    format           => { type=>'uint8', seq=>$mem_seq++ },  ? /* +70   +1 */
    imagesize        => { type=>'uint32', seq=>$mem_seq++ }, I /* +71  +4 */
    last_frame_score => { type=>'uint32', seq=>$mem_seq++ }, I /* +75  +4 */
    audio_frequency  => { type=>'uint32', seq=>$mem_seq++ }, I /* +79  +4 */
    audio_channels   => { type=>'uint32', seq=>$mem_seq++ }, I /* +83  +4 */
    startup_time     => { type=>'time_t64', seq=>$mem_seq++ },  q /* +87  +8 */ union(int64_t + time_t)
    zmc_heartbeat_time => { type=>'time_t64', seq=>$mem_seq++ },  q /* +95  +8 */ union(int64_t + time_t)
    last_write_time  => { type=>'time_t64', seq=>$mem_seq++ },  q /* +103 +8 */ union(int64_t + time_t)
    last_read_time   => { type=>'time_t64', seq=>$mem_seq++ },  q /* +111 +8 */ union(int64_t + time_t)
    last_viewed_time => { type=>'time_t64', seq=>$mem_seq++ },  q /* +119 +8 */ union(int64_t + time_t)
    control_state    => { type=>'int8[256]', seq=>$mem_seq++ }, s256 /* +127 +256 */ char[]
    alarm_cause      => { type=>'int8[256]', seq=>$mem_seq++ }, s256 /* +383 +256 */ char[]
    video_fifo_path  => { type=>'int8[64]', seq=>$mem_seq++ }, s64 /* +639 +64 */ char[]
    audio_fifo_path  => { type=>'int8[64]', seq=>$mem_seq++ }, s64 /* +703 +64 */ char[]
"""


"""
OLD STRUCTURE IS STILL CURRENT AS OF JULY 10th 2022
    trigger_data => { type=>'TriggerData', seq=>$mem_seq++, 'contents'=> {
    size             => { type=>'uint32', seq=>$mem_seq++ }, I
    trigger_state    => { type=>'uint32', seq=>$mem_seq++ }, I
    trigger_score    => { type=>'uint32', seq=>$mem_seq++ }, I
    padding          => { type=>'uint32', seq=>$mem_seq++ }, I
    trigger_cause    => { type=>'int8[32]', seq=>$mem_seq++ }, s32
    trigger_text     => { type=>'int8[256]', seq=>$mem_seq++ }, s256
    trigger_showtext => { type=>'int8[256]', seq=>$mem_seq++ }, s256
  }
"""
"""
NEW STRUCTURE
    trigger_data => { type=>'VideoStoreData', seq=>$mem_seq++, 'contents'=> {
    size             => { type=>'uint32', seq=>$mem_seq++ }, I /* +0 +4 */
    current_event    => { type=>'uint64', seq=>$mem_seq++ }, Q /* +4 +8 */
    event_file       => { type=>'int8[4096]', seq=>$mem_seq++ }, s4096 /* +12 +4096 */ char[]
    recording        => { type=>'uint8', seq=>$mem_seq++ },  ? /* +4108 +1 */ // used as both bool and a pointer to the timestamp when recording should begin
    }
    4109 bytes total by my calculation, comments in source say 4104 on 32/64 bit systems
"""


class ZMMemory:
    def __init__(self, api=None, path: Optional[str] = None, mid: Optional[Union[int, str]] = None):
        if not mid:
            raise ValueError("No monitor specified")
        global g
        g = GlobalConfig()
        if api is None:
            self.api = g.api
        else:
            self.api = api
        if path is None:
            path = f"/dev/shm"

        self.alarm_state_stages = {
            "STATE_IDLE": 0,
            "STATE_PREALARM": 1,
            "STATE_ALARM": 2,
            "STATE_ALERT": 3,
            "STATE_TAPE": 4,
            "ACTION_GET": 5,
            "ACTION_SET": 6,
            "ACTION_RELOAD": 7,
            "ACTION_SUSPEND": 8,
            "ACTION_RESUME": 9,
            "TRIGGER_CANCEL": 10,
            "TRIGGER_ON": 11,
            "TRIGGER_OFF": 12,
        }
        self.fhandle: Optional[IO] = None
        self.mhandle: Optional[mmap.mmap] = None

        self.fname = f"{path}/zm.mmap.{mid}"
        self.reload()

    def reload(self):
        """Reloads monitor information. Call after you get
        an invalid memory report

        Raises:
            ValueError: if no monitor is provided
        """
        # close file handler
        self.close()
        # open file handler in read binary mode
        self.fhandle = open(self.fname, "r+b")
        # geta rough sdize of the memory consumed by objkect (doesnt follow links or weak ref)
        sz = os.path.getsize(self.fname)
        if not sz:
            raise ValueError(f"Invalid size: {sz} of {self.fname}")

        self.mhandle = mmap.mmap(self.fhandle.fileno(), 0, access=mmap.ACCESS_READ)
        self.sd = None
        self.td = None
        self._read()

    def is_valid(self):
        """True if the memory handle is valid

        Returns:
            bool: True if memory handle is valid
        """
        try:
            d = self._read()
            return not d["shared_data"]["size"] == 0
        except Exception as e:
            g.log.error(f"Memory: {e}")
            return False

    def is_alarmed(self):
        """True if monitor is currently alarmed

        Returns:
            bool: True if monitor is currently alarmed
        """

        d = self._read()
        return int(d["shared_data"]["state"]) == self.alarm_state_stages["STATE_ALARM"]

    def alarm_state(self):
        """Returns alarm state

        Returns:
            dict: as below::

                {
                    'id': int # state id
                    'state': string # name of state
                }

        """

        d = self._read()
        return {
            "id": d["shared_data"]["state"],
            "state": list(self.alarm_state_stages.keys())[
                list(self.alarm_state_stages.values()).index(
                    int(d["shared_data"]["state"])
                )
            ],
        }

    def last_event(self):
        """Returns last event ID

        Returns:
            int: last event id
        """
        d = self._read()
        return d["shared_data"]["last_event"]

    def cause(self):
        """Returns alarm and trigger cause as applicable

        Returns:
            dict: as below::

                {
                    'alarm_cause': string
                    'trigger_cause': string
                }

        """
        d = self._read()
        return {
            "alarm_cause": d["shared_data"].get("alarm_cause"),  # May not be there
            "trigger_cause": d["shared_data"].get("trigger_cause"),
        }

    def trigger(self):
        """Returns trigger information

        Returns:
            dict: as below::

                {
                    'trigger_text': string,
                    'trigger_showtext': string,
                    'trigger_cause': string,
                    'trigger:state' {
                        'id': int,
                        'state': string
                    }
                }
        """

        d = self._read()
        return {
            "trigger_text": d["trigger_data"].get("trigger_text"),
            "trigger_showtext": d["trigger_data"].get("trigger_showtext"),
            "trigger_cause": d["trigger_data"].get("trigger_cause"),
            "trigger_state": {
                "id": d["trigger_data"].get("trigger_state"),
                "state": d["trigger_data"]["trigger_state"],
            },
        }

    def _read(self):
        self.mhandle.seek(0)  # goto beginning of file
        SharedData = namedtuple(
            "SharedData",
            "size last_write_index last_read_index state capture_fps analysis_fps last_event_id action brightness "
            "hue colour contrast alarm_x alarm_y valid capturing analysing recording signal format imagesize "
            "last_frame_score audio_frequency audio_channels startup_time zmc_heartbeat_time last_write_time "
            "last_read_time last_viewed_time control_state alarm_cause video_fifo_path audio_fifo_path",
        )

        # old_shared_data = r"IIIIQIiiiiii????IIQQQ256s256s"
        struct_shared_data = r"IiiIddQIiiiiiiccccccIIIIqqqqq256s256s64s64s"  # July 2022
        shared_data_bytes = 760  # bytes in SharedData - July 2022 *************'
        shared_data_bytes = 776

        TriggerData = namedtuple(
            "TriggerData",
            "size trigger_state trigger_score padding trigger_cause trigger_text trigger_showtext",
        )
        struct_trigger_data = r"IIII32s256s256s"
        trigger_data_bytes = 560  # bytes in TriggerData

        VideoStoreData = namedtuple(
            "VideoStoreData",
            "size padding current_event event_file recording",
        )
        struct_video_store = r"IIQ4096sq"
        video_store_bytes = 4128  # bytes in VideoStoreData - July 2022
        video_store_bytes = 4120  # struct exception

        TimeStampData = namedtuple(
            "TimeStampData",
            "time_t",
        )
        struct_time_stamp = r"q"
        time_stamp_bytes = IMAGE_BUFFER_COUNT * TIMEVAL_SIZE  # bytes in TimeStampData

        # We must know the IMAGE_BUFFER_COUNT for the monitor to calculate offsets, @ic0n recommends 3-5


        # image_size is from shared data struct (should be (width*height)*4)
        # --  width and height are from monitor settings

        # image_index is the last_write_index from shared data struct (check if last_write is last_read)
        # --        let last_write_index = state.shared_data.last_write_index as u32;
        #             if last_write_index != self.last_read_index
        #                 && last_write_index != self.image_buffer_count
        #             {
        #                 self.last_read_index = last_write_index;
        #                 let image = self.read_image(last_write_index)?;

        # image_offset will be offset of shared_images + image_size * image_index

        s = SharedData._make(
            struct.unpack(struct_shared_data, self.mhandle.read(shared_data_bytes))
        )
        t = TriggerData._make(
            struct.unpack(struct_trigger_data, self.mhandle.read(trigger_data_bytes))
        )
        v = VideoStoreData._make(
            struct.unpack(struct_video_store, self.mhandle.read(video_store_bytes))
        )
        # Timestamp data is timeval.size * IMAGE_BUFFER_COUNT
        ts = TimeStampData._make(
            struct.unpack(struct_time_stamp, self.mhandle.read(time_stamp_bytes))
        )
        # self.mhandle is now at sharedimages offset position, read the image data based upon index or grab all of the bytes and parse images?
        self.images_offset = self.mhandle.tell()
        image_byte_length = s.imagesize * IMAGE_BUFFER_COUNT
        # self.raw_images: bytes = self.mhandle.read(
        #     s.imagesize * IMAGE_BUFFER_COUNT
        # )  # grabs all images in SHM ring buffer
        struct_image = f"s{s.imagesize}" * IMAGE_BUFFER_COUNT
        image_nt_str = f""
        for i in range(IMAGE_BUFFER_COUNT):
            image_nt_str += f"Image_{i} "
        # By now the named tuple should be Image_0, Image_1 etc. up to IMAGE_BUFFER_COUNT
        SharedImagesData = namedtuple(
            "SharedImagesData",
            image_nt_str,
        )
        sh_img_data = SharedImagesData._make(struct.unpack(struct_image, self.mhandle.read(image_byte_length)))

        self.sd = s._asdict()
        self.td = t._asdict()
        self.vsd = v._asdict()
        self.tsd = ts._asdict()
        self.sh_img_data = sh_img_data._asdict()

        self.sd["alarm_cause"] = self.sd["alarm_cause"].split(b"\0", 1)[0].decode()
        self.sd["control_state"] = self.sd["control_state"].split(b"\0", 1)[0].decode()
        self.td["trigger_cause"] = self.td["trigger_cause"].split(b"\0", 1)[0].decode()
        self.td["trigger_text"] = self.td["trigger_text"].split(b"\0", 1)[0].decode()
        self.td["trigger_showtext"] = (
            self.td["trigger_showtext"].split(b"\0", 1)[0].decode()
        )
        return {
            "shared_data": self.sd,
            "trigger_data": self.td,
            "video_store_data": self.vsd,
            "time_stamp_data": self.tsd,
            "shared_images": self.sh_img_data,
        }

    def get(self):
        """returns raw shared and trigger data as a dict

        Returns:
            dict: raw shared and trigger data ::

                {
                    'shared_data': dict, # of shared data,
                    'trigger_data': dict # trigger data
                }
        """
        return self._read()

    def get_shared_data(self):
        """Returns just the shared data

        Returns:
            dict: shared data
        """
        return self._read()["shared_data"]

    def get_trigger_data(self):
        """Returns just the trigger data


        Returns:
            dict: trigger data
        """
        return self._read()["trigger_data"]

    def close(self):
        """Closes the handle"""
        try:
            if self.mhandle:
                self.mhandle.close()
            if self.fhandle:
                self.fhandle.close()
        except Exception as e:
            pass
