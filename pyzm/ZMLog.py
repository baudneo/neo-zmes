"""
ZMLog
=======
A python implementation of ZoneMinder's logging system
You can use this to connect it to the APIs if you want
"""
import glob
import grp
import os
import pwd
import signal
import syslog
import time
from configparser import ConfigParser
from datetime import datetime
from enum import Enum
from functools import partial
from getpass import getuser
from inspect import getframeinfo, stack, Traceback
from io import TextIOWrapper
from pathlib import Path
from shutil import which
from typing import Optional, Union, Any, NoReturn

# from rich import print
import psutil
from dotenv import load_dotenv
from sqlalchemy import MetaData, create_engine, or_, select, engine  # ,Table,inspect
from sqlalchemy.engine import Connection
from sqlalchemy.exc import SQLAlchemyError

lp = "ZM Log:"
ZM_INSTALLED = which("zmdc.pl")
DEFAULT_CONFIG = {
    "dbuser": "zmuser",
    "dbpassword": "zmpass",
    "webuser": "www-data",
    "webgroup": "www-data",
    "logpath": "/var/log/zm",
    "log_level_syslog": 0,
    "log_level_file": -5,
    "log_level_db": -5,
    "log_debug": 0,
    "log_level_debug": 0,
    "log_debug_target": "",
    "log_debug_file": 0,
    "server_id": 0,
    "dump_console": False,
}
# GlobalConfig causes a circular import
g = None


# This function to be called with functools.partial -> partial(sig_log_rot, g)
def sig_log_rot(g, sig, frame):
    lp = "signal handler:"
    name = g.log.logger_name
    overrides = g.log.logger_overrides
    # close handlers
    g.log.log_close()
    # re-init logger
    g.log = ZMLog(name=name, override=overrides, no_signal=True)
    g.log.info(
        f"{lp} ready after log re-initialization due to receiving HUP signal: {sig=}"
    )


# This function to be called with functools.partial -> partial(sig_intr, g)
def sig_intr(g, sig, frame):
    lp = "signal handler:"
    if sig == 2:
        g.log.error(f"{lp} KeyBoard Interrupt -> 2:SIGINT received!")
    elif sig == 6:
        g.log.error(f"{lp} Abort Interrupt -> 6:SIGABRT")
        g.log.log_close()
        os.abort()
    else:
        g.log.info(
            f"{lp} received interrupt signal ({sig}), safely closing logging handlers and exiting"
        )
    if g.log:
        # close the handlers, but don't remove them
        g.log.log_close()
    exit(0)


def str2bool(v: Optional[Union[str, bool]]) -> Optional[Union[str, bool]]:
    if v is None:
        return False
    if isinstance(v, bool):
        return v
    v = str(v).lower()
    true_ret = ("yes", "true", "t", "y", "1", "on", "ok", "okay", 1)
    false_ret = ("no", "false", "f", "n", "0", "off", 0)
    if v in true_ret:
        return True
    elif v in false_ret:
        return False
    else:
        return None


class ZMLogLevels(Enum):
    DBG = DEBUG = 1
    INF = INFO = 0
    WAR = WARNING = -1
    ERR = ERROR = -2
    FAT = FATAL = -3
    PNC = PANIC = -4
    OFF = -5


levels = {
    "DBG": 1,
    "INF": 0,
    "WAR": -1,
    "ERR": -2,
    "FAT": -3,
    "PNC": -4,
    "OFF": -5,
}
priorities = {
    "DBG": syslog.LOG_DEBUG,
    "INF": syslog.LOG_INFO,
    "WAR": syslog.LOG_WARNING,
    "ERR": syslog.LOG_ERR,
    "FAT": syslog.LOG_ERR,
    "PNC": syslog.LOG_ERR,
}


class ZMLog:
    def __init__(
        self,
        name: str,
        override: Optional[dict] = None,
        caller: Optional[Traceback] = None,
        globs: Optional[Any] = None,
        no_signal: bool = False,
        log_buffer: Optional = None,
    ):
        from pyzm.helpers.pyzm_utils import LogBuffer

        if not override:
            override = {}
        self.logging_overrides = override

        self.no_signal: bool = no_signal
        self.globs: Optional = None
        self.set_g(globs)

        self.dump_console: bool = False
        self.buffer: Optional[LogBuffer] = log_buffer
        self.is_active: bool = False
        self.config: dict = {}
        self.engine: Optional[engine.Engine] = None
        self.conn: Optional[Connection] = None
        self.sql_connected: bool = False
        self.config_table = None
        self.log_table = None
        self.meta: Optional[MetaData] = None
        # File handler
        self.log_filename: str = ""
        self.log_file_handler: Optional[TextIOWrapper] = None
        self.connection_str: str = ""
        load_dotenv()
        self.logger_name: str = ""
        self.logger_overrides: dict = {}
        self.pid: Optional[int] = None
        self.process_name: str = ""
        self.syslog: Optional[syslog] = None
        self.exit_times_called: int = 0
        self.__post_init__(name=name, override=override, caller=caller)

    def __post_init__(self, name: str, override: Optional[dict] = None, caller=None):
        self.syslog: syslog = syslog
        self.syslog.openlog(logoption=syslog.LOG_PID)
        self.pid: int = os.getpid()
        self.process_name: str = name or psutil.Process(self.pid).name()
        self.logger_name: str = name
        self.logger_overrides: dict = override
        db_data: dict = g.extras.get("db_data", {})

        defaults = DEFAULT_CONFIG
        if not db_data:
            self.config = {
                "conf_path": os.getenv(
                    "PYZM_CONFPATH", "/etc/zm"
                ),  # we need this to get started
                "dbuser": os.getenv("PYZM_DBUSER"),
                "dbpassword": os.getenv("PYZM_DBPASSWORD"),
                "dbhost": os.getenv("PYZM_DBHOST"),
                "webuser": os.getenv("PYZM_WEBUSER"),
                "webgroup": os.getenv("PYZM_WEBGROUP"),
                "dbname": os.getenv("PYZM_DBNAME"),
                "logpath": os.getenv("PYZM_LOGPATH"),
                "log_level_syslog": os.getenv("PYZM_SYSLOGLEVEL", 0),
                "log_level_file": os.getenv("PYZM_FILELOGLEVEL"),
                "log_level_db": os.getenv("PYZM_DBLOGLEVEL"),
                "log_debug": os.getenv("PYZM_LOGDEBUG"),
                "log_level_debug": os.getenv("PYZM_LOGDEBUGLEVEL"),
                "log_debug_target": os.getenv("PYZM_LOGDEBUGTARGET"),
                "log_debug_file": os.getenv("PYZM_LOGDEBUGFILE"),
                "server_id": os.getenv("PYZM_SERVERID"),
                "dump_console": os.getenv("PYZM_DUMPCONSOLE"),
                "driver": os.getenv("PYZM_DBDRIVER", "mysql+mysqlconnector"),
            }
        else:
            self.config = db_data["db_config"]

        if ZM_INSTALLED:
            if not db_data:
                if conf_path := self.config.get("conf_path"):
                    # read all config files in order
                    files = []
                    for f in glob.glob(f"{conf_path}/conf.d/*.conf"):
                        files.append(f)
                    files.sort()
                    files.insert(0, f"{conf_path}/zm.conf")
                    config_file = ConfigParser(
                        interpolation=None, inline_comment_prefixes="#"
                    )
                    f: Optional[str] = None
                    try:
                        for f in files:
                            with open(f, "r") as file_contents:
                                # This adds [zm_root] section to the head of each zm
                                # conf.d/ config file, not physically only in memory
                                config_file.read_string(
                                    f"[zm_root]\n{file_contents.read()}"
                                )
                    except Exception as exc:
                        tmp_msg: str = f"Error opening {f if f else files} -> {exc}"
                        self.syslog.syslog(
                            syslog.LOG_ERR,
                            self._format_string(tmp_msg),
                        )
                        self.buffer.error(tmp_msg)
                    else:
                        conf_data = config_file["zm_root"]
            else:
                conf_data = db_data.get("conf_data")

            if not db_data:
                if not self.config.get("dbuser"):
                    self.config["dbuser"] = conf_data.get("ZM_DB_USER")
                if not self.config.get("dbpassword"):
                    self.config["dbpassword"] = conf_data.get("ZM_DB_PASS")
                if not self.config.get("webuser"):
                    self.config["webuser"] = conf_data.get("ZM_WEB_USER")
                if not self.config.get("webgroup"):
                    self.config["webgroup"] = conf_data.get("ZM_WEB_GROUP")
                if not self.config.get("dbhost"):
                    self.config["dbhost"] = conf_data.get("ZM_DB_HOST")
                if not self.config.get("dbname"):
                    self.config["dbname"] = conf_data.get("ZM_DB_NAME")
                if not self.config.get("logpath"):
                    self.config["logpath"] = config_file["zm_root"].get("ZM_PATH_LOGS")

                self.connection_str = (
                    f"{self.config['driver']}://{self.config['dbuser']}"
                    f":{self.config['dbpassword']}@{self.config['dbhost']}"
                    f"/{self.config['dbname']}"
                )
            else:
                self.connection_str = db_data.get("connection_str")

            try:
                if db_data.get("engine"):
                    self.engine = db_data["engine"]
                elif not db_data or (db_data and not db_data.get("engine")):
                    self.engine = create_engine(self.connection_str, pool_recycle=3600)
                self.conn = self.engine.connect()
                self.sql_connected = True
            except SQLAlchemyError as e:
                self.sql_connected = False
                self.conn = None
                self.engine = None
                tmp_msg: str = f"{lp} Turning DB logging off. Could not connect to DB, message was: {e}"
                self.syslog.syslog(syslog.LOG_ERR, self._format_string(tmp_msg))
                self.buffer.error(tmp_msg)
                self.config["log_level_db"] = levels["OFF"]

            else:
                self.meta = MetaData(self.engine)
                self.meta.reflect(only=["Config", "Logs"])
                self.config_table = self.meta.tables["Config"]
                self.log_table = self.meta.tables["Logs"]

                select_st = select(
                    [self.config_table.c.Name, self.config_table.c.Value]
                ).where(
                    or_(
                        self.config_table.c.Name == "ZM_LOG_LEVEL_SYSLOG",
                        self.config_table.c.Name == "ZM_LOG_LEVEL_FILE",
                        self.config_table.c.Name == "ZM_LOG_LEVEL_DATABASE",
                        self.config_table.c.Name == "ZM_LOG_DEBUG",
                        self.config_table.c.Name == "ZM_LOG_DEBUG_LEVEL",
                        self.config_table.c.Name == "ZM_LOG_DEBUG_FILE",
                        self.config_table.c.Name == "ZM_LOG_DEBUG_TARGET",
                        self.config_table.c.Name == "ZM_SERVER_ID",
                    )
                )
                result_proxy = self.conn.execute(select_st)
                db_vals = {row[0]: row[1] for row in result_proxy}
                self.syslog.syslog(
                    syslog.LOG_WARNING,
                    self._format_string(f"{db_vals = }", level="WAR"),
                )
                #  db_vals = {'ZM_LOG_DEBUG': '0', 'ZM_LOG_DEBUG_FILE': '', 'ZM_LOG_DEBUG_LEVEL': '5',
                #  'ZM_LOG_DEBUG_TARGET': 'zmc_m1|zmc_m2', 'ZM_LOG_LEVEL_DATABASE': '-3',
                #  'ZM_LOG_LEVEL_FILE': '1', 'ZM_LOG_LEVEL_SYSLOG': '-5'}
                self.config["log_level_syslog"] = int(db_vals["ZM_LOG_LEVEL_SYSLOG"])
                self.config["log_level_file"] = int(db_vals["ZM_LOG_LEVEL_FILE"])
                self.config["log_level_db"] = int(db_vals["ZM_LOG_LEVEL_DATABASE"])
                self.config["log_debug"] = int(db_vals["ZM_LOG_DEBUG"])
                self.config["log_level_debug"] = int(db_vals["ZM_LOG_DEBUG_LEVEL"])
                self.config["log_debug_file"] = db_vals["ZM_LOG_DEBUG_FILE"]
                self.config["log_debug_target"] = db_vals["ZM_LOG_DEBUG_TARGET"]
                self.config["server_id"] = db_vals.get("ZM_SERVER_ID", 0)
                self.syslog.syslog(
                    syslog.LOG_WARNING,
                    self._format_string(
                        f"AFTER READING DB --> {self.config = }", level="WAR"
                    ),
                )
        else:
            self.no_signal = True
            tmp_msg = f"{lp} ZoneMinder installation not detected, configuring logs accordingly..."
            self.syslog.syslog(
                syslog.LOG_ERR,
                self._format_string(tmp_msg),
            )
            self.buffer.error(tmp_msg)

        for k, v in self.config.items():
            if v is None and k in defaults:
                self.syslog.syslog(
                    syslog.LOG_WARNING,
                    self._format_string(
                        f"The config KEY: '{k}' is set to None but is in defaults, overwriting!",
                        level="WAR",
                    ),
                )
                self.config[k] = defaults[k]

        self.syslog.syslog(
            syslog.LOG_WARNING,
            self._format_string(
                f"AFTER PULLING FROM DEFAULTS -> {self.config = }", level="WAR"
            ),
        )
        self.set_overrides(self.logger_overrides)

        if not self.no_signal:
            try:
                self.buffer.info(
                    f"{lp} setting up signal handlers for log 'rotation' and 'interrupt'"
                )
                signal.signal(signal.SIGHUP, partial(sig_log_rot, g))
                signal.signal(signal.SIGINT, partial(sig_intr, g))
            except Exception as e:
                tmp_msg = f"Error setting up log 'rotate' and 'interrupt' signal handlers -> {e}"
                self.buffer.error(tmp_msg)
        self.is_active = True
        show_log_name = f"'{self.log_filename}'"
        if ZM_INSTALLED:
            tmp_msg = (
                f"{lp} Connected to ZoneMinder Logging system with user '{getuser()}'"
                f"{f' -> {show_log_name}' if self.log_file_handler else ''}"
            )
            self.buffer.info(tmp_msg)
        else:
            syslog_str = self.config["log_level_syslog"] > levels["OFF"]
            tmp_msg = (
                f"Logging to {'console ' if str2bool(self.dump_console) else ''}"
                f"{'and ' if self.syslog and syslog_str else ''}"
                f"{'syslog ' if self.syslog and syslog_str else ''}"
                f"{'and ' if self.syslog and self.log_file_handler else ''}"
                f"{'file ' if self.log_file_handler else ''}with user '{getuser()}'"
                f"{f' -> {show_log_name}' if self.log_file_handler else ''}"
            )
            self.buffer.info(tmp_msg)
        if (
            self.buffer
            and self.buffer is not None
            and self.buffer.buffer
            and len(self.buffer.buffer)
        ):
            while self.buffer:
                self.buffer.buffer = sorted(
                    self.buffer.buffer, key=lambda x: x["timestamp"], reverse=True
                )
                # pop it just in case it somehow gets iterated again
                line = self.buffer.pop()
                if line:
                    if line.get("lineno"):
                        line["lineno"] = f"{line['lineno']}(dmp)-"
                    if line["display_level"].startswith("INF"):
                        self.info(line)
                    elif line["display_level"].startswith("ERR"):
                        self.error(line)
                    elif line["display_level"].startswith("WAR"):
                        self.warning(line)
                    elif line["display_level"].startswith("DBG"):
                        self.debug(line)
                    elif line["display_level"].startswith("PNC"):
                        self.panic(line)
                    elif line["display_level"].startswith("FAT"):
                        self.fatal(line)
                    else:
                        self.error(
                            f"BUFFER DUMP->) Unknown log level '{line['level']}'"
                        )

    def set_overrides(self, overrides: dict) -> NoReturn:
        """
        Set the overrides for the log.
        """
        # print(f"ZMLog - set_overrides() called with -> {overrides}")
        self.logger_overrides = overrides
        for k, v in self.config.items():
            if v is None and k in DEFAULT_CONFIG:
                self.config[k] = DEFAULT_CONFIG[k]
            if db_conf_data := g.extras.get("db_data", {}).get("conf_data"):
                if v is None and k in db_conf_data:
                    self.config[k] = db_conf_data[k]
            if k in overrides:
                self.config[k] = overrides[k]
        self.dump_console = self.config["dump_console"]
        self.syslog.syslog(
            syslog.LOG_ERR,
            self._format_string(
                f"set_overrides called: {overrides = } ---=-=-=-=-=-=- {self.config = }",
                level="ERR",
            ),
        )

        # print(f"ZMLog END OF set_overrides() -> {self.config = }")
        self._init_file_logger()

    def _init_file_logger(self) -> NoReturn:
        if self.log_file_handler:
            self.log_file_handler.flush()
            self.log_file_handler.close()
        uid = pwd.getpwnam(self.config["webuser"]).pw_uid
        gid = grp.getgrnam(self.config["webgroup"]).gr_gid
        if self.config["log_level_file"] > levels["OFF"]:
            self.log_filename = f"{self.config['logpath']}/{self.process_name}.log"
            try:
                log_file = Path(self.log_filename)
                log_file.touch(exist_ok=True)
                # If in a docker container, do not chown the log file
                if not g.config.get("DOCKER"):
                    os.chown(self.log_filename, uid, gid)  # proper permissions
                # Don't forget to close the file handler
                self.log_file_handler = log_file.open("a")
            except Exception as e:
                _change_perms = " and changing permissions of"
                tmp_msg = (
                    f"{lp} Error for user: '{getuser()}' creating{_change_perms if not g.config.get('DOCKER') else ''}"
                    f" file: '{self.log_filename}'"
                )
                self.syslog.syslog(
                    syslog.LOG_ERR,
                    self._format_string(tmp_msg),
                )
                print(tmp_msg)

    def set_g(self, global_obj):
        global g
        self.globs = g = global_obj

    def is_active(self):
        return self.is_active

    def set_level(self, level):
        pass

    def get_buff(self):
        return NotImplemented()

    def flush_buffer(self):
        return NotImplemented()

    def get_config(self):
        return self.config

    def _db_reconnect(self):
        g = self.globs

        try:
            self.conn.close()
        except Exception:
            pass
        if ZM_INSTALLED:
            try:
                self.engine = create_engine(self.connection_str, pool_recycle=3600)
                self.conn = self.engine.connect()
                # inspector = inspect(engine)
                # self.buffer.debug(inspector.get_columns('Config'))
                self.meta = MetaData(self.engine)
                self.meta.reflect(only=["Config", "Logs"])
                self.config_table = self.meta.tables["Config"]
                self.log_table = self.meta.tables["Logs"]
                message = "reconnecting to Database..."
                log_string = "{level} [{pname}] [{msg}]".format(
                    level="INF", pname=self.process_name, msg=message
                )
                self.syslog.syslog(syslog.LOG_INFO, log_string)
            except SQLAlchemyError as e:
                self.sql_connected = False
                self.syslog.syslog(
                    syslog.LOG_ERR,
                    self._format_string(
                        "Turning off DB logging due to error received:" + str(e)
                    ),
                )
                return False
            else:
                self.sql_connected = True
                return self.sql_connected

    def log_close(self, *args, **kwargs):
        g = self.globs

        idx = min(len(stack()), 1)  # in case someone calls this directly
        caller = getframeinfo(stack()[idx][0])

        self.exit_times_called += 1
        self.is_active = False
        if self.exit_times_called <= 1:
            if self.conn:
                # self.Debug (4, "Closing DB connection")
                self.conn.close()
            if self.engine:
                # self.Debug (4, "Closing DB engine")
                self.engine.dispose()
                # Put this here so it logs to syslog and file before closing those handlers
            self.debug(f"{lp} Closing all log handlers NOW", caller=caller)
            if not g.log:
                print(f"{lp} Closing all log handlers NOW")

            if self.log_file_handler:
                # self.Debug (4, "Closing log file handle")
                self.log_file_handler.close()
                self.log_file_handler = None
            if self.syslog:
                # self.Debug (4, "Closing syslog connection")
                self.syslog.closelog()

    def _format_string(self, message="", level="ERR"):
        g = self.globs
        log_string = "{level} [{pname}] [{message}]".format(
            level=level, pname=self.process_name, message=message
        )
        return log_string

    def time_format(self, dt_form):
        g = self.globs
        if len(str(float(f"{dt_form.microsecond / 1e6:06}")).split(".")) > 1:
            micro_sec = str(float(f"{dt_form.microsecond / 1e6:06}")).split(".")[1]
        else:
            micro_sec = str(float(f"{dt_form.microsecond / 1e6:06}")).split(".")[0]
        return f"{dt_form.strftime('%m/%d/%y %H:%M:%S')}.{micro_sec}"

    def _log(self, **kwargs):
        self.syslog.syslog(
            syslog.LOG_ERR,
            f"{self.process_name = } _log function called with kwargs: {kwargs = }",
        )
        # print(f"\n_log called! - {self.config = }\n")
        blank, caller, nl, level, tight, debug_level, message = (
            None,
            None,
            None,
            "DBG",
            False,
            1,
            None,
        )
        dt = None
        display_level = None
        fnfl = None
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
            elif k == "level":
                level = v
            elif k == "message":
                message = v
            elif k == "debug_level":
                debug_level = v
            elif k == "blank":
                blank = v
            elif k == "timestamp":
                dt = v
            elif k == "display_level":
                display_level = v
            elif k == "filename" and v is not None:
                fnfl = f"{v}:{kwargs['lineno']}"
        file_log_string = ""
        if not dt:
            dt = self.time_format(datetime.now())
        # first stack element will be the wrapper log function
        # second stack element will be the actual calling function or maybe class wrapper?
        # third will be actual func?
        # print (len(stack()))
        if not caller or (
            caller and fnfl
        ):  # if caller was not passed we create it here, meaning in logs this modules name and line no will show up
            idx = min(len(stack()), 2)  # in the case of someone calls this directly
            caller = getframeinfo(stack()[idx][0])
        # print ('CALLER INFO --> FILE: {} LINE: {}'.format(caller.filename, caller.lineno))

        component = self.process_name
        server_id = self.config.get("server_id")
        pid = self.pid
        level_ = levels[level]
        code = level
        line = caller.lineno

        if not display_level:
            display_level = level
        syslog_string = f"{display_level} [{self.process_name}] [{message}]"

        # If DBG show which DBG level
        if level == "DBG":
            display_level = f"{level}{debug_level}"

        # write to syslog
        self.syslog.syslog(
            syslog.LOG_ERR,
            self._format_string(
                f"{level_ = } - {self.config['log_level_syslog'] = } --==-- "
                f"{self.config['log_level_db'] = } --==-- {self.config['log_level_file'] = } --==-- "
                f"{message = } --==-- {caller.filename = } --==-- {caller.lineno = } --==-- ",
                level="ERR",
            ),
        )
        if level_ <= self.config["log_level_syslog"]:
            self.syslog.syslog(priorities[level], syslog_string)
        # write to db only if ZM installed
        #  level_ = 1 - code = 'DBG' - self.config['log_level_db'] = 0 ]
        if level_ <= self.config["log_level_db"] and ZM_INSTALLED:
            send_to_db = True
            if not self.sql_connected:
                self.syslog.syslog(
                    syslog.LOG_INFO,
                    self._format_string(f"{lp} Connecting to ZoneMinder SQL DB"),
                )
                if not self._db_reconnect():
                    self.syslog.syslog(
                        syslog.LOG_ERR,
                        self._format_string(
                            f"{lp} Reconnecting failed, not writing to ZM DB"
                        ),
                    )
                    send_to_db = False
            if send_to_db:
                try:
                    cmd = self.log_table.insert().values(
                        TimeKey=time.time(),
                        Component=component,
                        ServerId=server_id,
                        Pid=pid,
                        Level=level_,
                        Code=code,
                        Message=message,
                        File=os.path.split(caller.filename)[1],
                        Line=line,
                    )
                    self.conn.execute(cmd)
                except SQLAlchemyError as e:
                    self.sql_connected = False
                    self.syslog.syslog(
                        syslog.LOG_ERR, self._format_string(f"Error writing to DB: {e}")
                    )
        if not fnfl:
            fnfl = f"{Path(caller.filename).name.split('.')[0]}:{caller.lineno}"
        print_log_string: str = (
            f"{dt} {self.process_name}[{pid}] {display_level} {fnfl}->[{message}]"
        )
        if level_ <= self.config["log_level_file"]:
            file_log_string = (
                f"{dt} {self.process_name}[{pid}] {display_level} {fnfl}->[{message}]\n"
            )
            if self.log_file_handler:
                try:
                    self.log_file_handler.write(file_log_string)  # write to file
                    self.log_file_handler.flush()
                except Exception as e:
                    tmp_msg = f"'{file_log_string}' is not available to be written to, trying to create a new log file"
                    if self.buffer and self.buffer is not None:
                        self.buffer.error(f"(buffer)->{tmp_msg}")
                    else:
                        self.error(tmp_msg)
                    # make log file
                    uid = pwd.getpwnam(self.config["webuser"]).pw_uid
                    gid = grp.getgrnam(self.config["webgroup"]).gr_gid
                    if self.config["log_level_file"] > levels["OFF"]:
                        self.log_filename = (
                            f"{self.config['logpath']}/{self.process_name}.log"
                        )
                        try:
                            log_file = Path(self.log_filename)
                            log_file.touch(exist_ok=True)
                            os.chown(self.log_filename, uid, gid)  # proper permissions
                            self.log_file_handler = log_file.open("a")
                        except Exception as e:
                            tmp_msg = (
                                f"Error for user: {getuser()} while creating and changing permissions of "
                                f"log file -> {str(e)}"
                            )
                            print(tmp_msg)
                            self.syslog.syslog(
                                syslog.LOG_ERR,
                                self._format_string(tmp_msg),
                            )
                            self.log_file_handler = None
                        else:
                            # write to file after re-creating log file
                            self.log_file_handler.write(file_log_string)
                            self.log_file_handler.flush()

        if self.dump_console:
            print(f"\n{print_log_string}")

    def info(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level = None, None, None
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]
        else:
            message = args[0]
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        # self._log(level='INF', message=message, caller=caller, tight=tight, nl=nl)
        self._log(
            level="INF",
            message=message,
            caller=caller,
            debug_level=level,
            tight=tight,
            nl=nl,
            timestamp=ts,
            display_level=display_level,
            filename=filename,
            lineno=lineno,
        )

    def debug(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level, message = None, None, None, None
        level = 1
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]

        elif len(args) == 1:
            message = args[0]
        elif len(args) == 2:
            level = args[0]
            message = args[1]

        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        if not caller:
            idx = min(len(stack()), 1)  # in the case of someone calls this directly
            caller = getframeinfo(stack()[idx][0])


        # self.config = {'conf_path': '/etc/zm', 'dbuser': 'zmuser', 'dbpassword': 'zmpass', 'dbhost': 'localhost',
        # 'webuser': 'www-data', 'webgroup': 'www-data', 'dbname': 'zm', 'logpath': '/var/log/zm',
        # 'log_level_syslog': 1, 'log_level_file': 1, 'log_level_db': -3, 'log_debug': True, 'log_level_debug': 5,
        # 'log_debug_target': 'zmc_m1|zmc_m2', 'log_debug_file': 1, 'server_id': 0, 'dump_console': False,
        # 'driver': 'mysql+mysqlconnector'
        target = self.config["log_debug_target"]
        self.syslog.syslog(
            syslog.LOG_ERR,
            f"IN DEBUG LOG FUNC {self.process_name = } --- {target = }",
        )
        if target and not self.dump_console:
            targets = [x.strip().lstrip("_") for x in target.split("|")]
            # if current name does not fall into debug targets don't log
            self.syslog.syslog(
                syslog.LOG_ERR,
                f"IN DEBUG LOG FUNC {targets = }",
            )
            if not any(map(self.process_name.startswith, targets)):
                self.syslog.syslog(
                    syslog.LOG_ERR,
                    f"SKIPPING DEBUG RECORD AS process name is not in targets!",
                )
                return
        if self.config["log_debug"] and level <= self.config["log_level_debug"]:
            self._log(
                level="DBG",
                message=message,
                caller=caller,
                debug_level=level,
                tight=tight,
                nl=nl,
                timestamp=ts,
                display_level=display_level,
                filename=filename,
                lineno=lineno,
            )

    def warning(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level = None, None, None
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]
        else:
            message = args[0]
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        self._log(
            level="WAR",
            message=message,
            caller=caller,
            debug_level=level,
            tight=tight,
            nl=nl,
            timestamp=ts,
            display_level=display_level,
            filename=filename,
            lineno=lineno,
        )

    def error(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level = None, None, None
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]
        else:
            message = args[0]
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        self._log(
            level="ERR",
            message=message,
            caller=caller,
            debug_level=level,
            tight=tight,
            nl=nl,
            timestamp=ts,
            display_level=display_level,
            filename=filename,
            lineno=lineno,
        )

    def fatal(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level = None, None, None
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]
        else:
            message = args[0]
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        self._log(
            level="FAT",
            message=message,
            caller=caller,
            debug_level=level,
            tight=tight,
            nl=nl,
            timestamp=ts,
            display_level=display_level,
            filename=filename,
            lineno=lineno,
        )
        self.log_close()
        exit(-1)

    def panic(self, *args, **kwargs):
        g = self.globs
        tight = False
        caller, nl, level = None, None, None
        ts = None
        message = None
        lineno = None
        display_level = None
        filename = None
        if isinstance(args[0], dict):
            a = args[0]
            ts = a["timestamp"]
            lineno = a["lineno"]
            display_level = a["display_level"]
            filename = a["filename"]
            message = a["message"]
        else:
            message = args[0]
        for k, v in kwargs.items():
            if k == "caller":
                caller = v
            elif k == "tight":
                tight = v
            elif k == "nl":
                nl = v
        self._log(
            level="PNC",
            message=message,
            caller=caller,
            debug_level=level,
            tight=tight,
            nl=nl,
            timestamp=ts,
            display_level=display_level,
            filename=filename,
            lineno=lineno,
        )
        self.log_close()
        exit(-1)

    inf = info
    dbg = debug
    wrn = warn = warning
    err = error
    fat = ftl = fatal
    pnc = panic
