from enum import Enum
from pathlib import Path
from time import sleep
from typing import Any, AnyStr, List, Optional, Tuple, Union, Set
from decimal import Decimal

import cv2
import numpy as np
import requests
from imutils.video import FileVideoStream as FVStream

from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.pyzm_utils import resize_image, str2bool


# log prefix to add onto
LP: str = "media:"
g: GlobalConfig






class MediaStream:
    def __init__(
        self,
        stream: Union[str, int],
        stream_type: Optional[str] = None,
        options: Optional[dict] = None,
    ):
        self.event_tot_frames: int = 0
        self.event_ended: str = ""

        def _set_api():
            # assume it is an event id, in which case we need to access it via ZM API
            self.current_frame = self.next_frame_id_to_read = self.start_frame
            self.stream = (
                f"{g.api.get_portalbase()}/index.php?view=image&eid={self.stream}"
            )
            self.type = "api"

        #   

        #  INIT START 
        lp = f"{LP}init:"
        if not options:
            raise ValueError(f"{lp} no stream options provided!")
        if stream_type is None:
            stream_type = "video"
        global g
        g = GlobalConfig()
        self.options: Optional[dict] = options
        self.stream: str = stream
        _allowed_stream_types = ("api", "video", "file")
        if stream_type not in _allowed_stream_types:
            raise TypeError(
                f"{stream_type} is INVALID! Allowed types: {_allowed_stream_types}"
            )
        self.type: str = stream_type
        g.log.dbg(f"{lp} options: {self.options}")

        #   
        #   
        #   
        #   
        #  FRAME IDS 
        self.attempted_fids: Set[int] = set()  # All tried
        self.successful_fids: Set[int] = set()  # All successful
        self.skipped_fids: Set[int] = set()  # All skipped
        self.frames_processed: int = 0  # number of frames processed

        #  FRAME BUFFER 
        self.start_frame: int = int(self.options.get("start_frame", 1))
        self.fps_fid_skip: int = int(self.options.get("frame_skip", 1))
        self.min_frames: int = 1
        self.max_frames: int = int(self.options.get("max_frames", 0))
        self.current_frame: int = 0
        self.current_snapshot: int = 0
        self.next_frame_id_to_read: int = 2
        self.last_frame_id_read: int = 0
        self.last_snapshot_id: int = 0
        self.fps: int = 0
        self.buffer_pre_count: int = 0
        self.buffer_post_count: int = 0

        if _fps := self.options.get("fps"):
            self.fps = g.mon_fps = int(Decimal(_fps).quantize(Decimal("1")))
        if _pre := self.options.get("precount"):
            self.buffer_pre_count = g.mon_pre = int(_pre)
        if _post := self.options.get("postcount"):
            self.buffer_post_count = g.mon_post = int(_post)

        #  VIDEO STREAM LIB 
        # fixme OpenCV can process video frames instead of FVStream
        self.fvs: Optional[FVStream] = None

        #  FRAME_SET 
        self.frame_set: Optional[List[AnyStr]] = None
        self.frame_set_index: int = 0

        #  RESIZE 
        self.resize: Union[str, int] = self.options.get("resize", "no")
        self.orig_h_w: Optional[Tuple[str, str]] = None
        self.resized_h_w: Optional[Tuple[str, str]] = None

        self.max_attempts = self.convert_val(
            int, {"max_attempts": self.options.get("max_attempts", 3)}
        )
        self.max_attempts_sleep_time = self.convert_val(
            float,
            {"delay_between_attempts": self.options.get("delay_between_attempts", 3.0)},
        )

        #  Check if a delay is configured 
        if not g.past_event and (delay := self.options.get("delay", 0.0)):
            try:
                delay = float(delay)
            except ValueError or TypeError as delay_exc:
                g.log.error(
                    f"{lp} the configured delay is malformed! can only be a number (x or x.xx)"
                )
                g.log.debug(delay_exc)
            else:
                if delay:
                    g.log.debug(
                        f"{LP} a delay is configured, this only applies one time - waiting for {delay} "
                        f"second(s) before starting"
                    )
                    sleep(delay)

        #  Parse stream into a type 
        if isinstance(self.stream, str):
            if self.stream.isnumeric():
                _set_api()
            #  Check if the input stream is an image file 
            elif Path(self.stream).suffix.lower() in (".jpg", ".png", ".jpeg"):
                g.log.debug(
                    f"{LP} The supplied stream '{self.stream}' is assumed to be an image file"
                )
                self.type = "file"
                return
        elif isinstance(self.stream, int):
            _set_api()
        else:
            g.log.error(
                f"{LP} The supplied stream '{self.stream}' is malformed or an incorrect type!"
            )

        #  advanced mode must be set for frame_set to override smart frame buffer 
        if self.options.get("frame_set") and str2bool(self.options.get("advanced")):
            if isinstance(self.options.get("frame_set"), str):
                self.frame_set = self.options.get("frame_set").split(",")
            elif isinstance(self.options.get("frame_set"), list):
                self.frame_set = [str(i).strip() for i in self.options.get("frame_set")]
            else:
                g.log.warning(
                    f"{LP} error in frame_set format (not a string or list), setting to 'snapshot,alarm,snapshot'",
                )
                self.frame_set = ["snapshot", "alarm", "snapshot"]
            g.log.debug(f"DEBUG >>>> {self.frame_set = }")
            self.max_frames = self.frame_set_length()
            if self.type == "video" and (
                "alarm" in self.frame_set or "snapshot" in self.frame_set
            ):
                # remove snapshot and alarm then see if any left before error
                self.frame_set = [
                    str(i)
                    for i in self.options.get("frame_set")
                    if i != "snapshot" or i != "alarm"
                ]
                # If frame_set is now an empty list, then error
                if not self.frame_set:
                    g.log.error(
                        f"{LP} you are using 'snapshot' or 'alarm' frame ids inside of frame_set with a VIDEO FILE"
                        f", 'snapshot' or 'alarm' are a special feature of grabbing frames from the ZM API. You"
                        f"must specify actual frame ID' when detecting on a VIDEO FILE"
                    )
                    raise ValueError("WRONG FRAME_SET TYPE")

            g.log.debug(
                2,
                f"{LP} processing a maximum of {self.frame_set_length()} frame(s) -> {self.frame_set}",
            )
            self.frame_set_index = 0
            self.start_frame = self.frame_set[self.frame_set_index]
        else:
            # advanced mode is disabled or frame_set not configured, use smart mode
            self.frame_set = []
            self.frame_set_index = 0
            # Alarm frame is always the first frame, pre count buffer length+1 for alarm frame
            self.current_frame = (
                self.start_frame
            ) = self.next_frame_id_to_read = self.buffer_pre_count
            # The pre- / post-buffers will give the absolute minimum number of frames to grab, assuming no event issues
            g.log.dbg(f"ZeroDivision DEBUG >>> {self.buffer_pre_count = } -- {self.buffer_post_count = } -- {self.fps = }")
            self.min_frames = int(
                (self.buffer_post_count + self.buffer_pre_count) / self.fps
            )
            # We don't know how long an event will be so set an upper limit of at least
            # pre- + post-buffers calculated as seconds because we are pulling 1 FPS
            self.max_frames = max(10, self.min_frames)
            self.fps_fid_skip = self.fps
            g.log.debug(f"{lp} using smart mode")

        # fixme: video loading, append fids etc
        if self.type == "video":
            self.current_frame = self.start_frame
            g.log.debug(
                f"{LP}video: loading video stream {stream} - skipping to frame: {self.current_frame}"
            )
            self.debug_filename = repr(Path(self.stream).name)
            self.fvs = FVStream(self.stream)
            self.fvs.start()
            sleep(0.5)  # let it settle?
            if self.frame_set:
                while (
                    self.fvs.more() and self.frames_processed <= self.frame_set_length()
                ):
                    self.fvs.read()
                    self.successful_fids.add(self.current_frame)
            else:
                while self.fvs.more() and self.next_frame_id_to_read < int(
                    self.start_frame
                ):
                    self.fvs.read()
                    self.next_frame_id_to_read += 1

        else:  # Use API
            g.log.debug(2, f"{LP} using API calls for stream -> {stream}")

    def read(self):
        def _grab_event_data(msg: Optional[str] = None):
            """Calls global API make_request method to get event data"""
            if msg:
                g.log.debug(f"{LP}read>event_data: {msg}")
            try:
                g.Event, g.Monitor, g.Frame, g.Monitor_new = g.api.get_all_event_data()
            except Exception as e:
                g.log.error(f"{lp} error grabbing event data from API -> {e}")
                raise e
            else:
                g.log.debug(f"{lp} grabbed event data from ZM API for event '{g.eid}'")
                self.event_tot_frames = int(g.Event.get("Frames", 0))
                self.event_ended = g.Event.get("EndDateTime")
                if self.event_ended:
                    g.log.dbg(f"DBG => THIS EVENT HAS AN EndDateTime ({self.event_ended}) checking max_frames "
                              f"and modifying if needed")
                    new_max = int(self.event_tot_frames / self.fps)
                    g.log.dbg(
                        f"DEBUG >>>> current max: {self.max_frames} new_max: {new_max} (event_total_frames"
                        f"[{self.event_tot_frames}] / fps[{self.fps}])"
                    )
                    if new_max > self.max_frames:
                        g.log.debug(
                            f"{lp} max_frames ({self.max_frames}) is lower than current calculations, "
                            f"setting to {new_max}"
                        )
                        self.max_frames = new_max

        response: Optional[requests.Response] = None
        frame: Optional[np.ndarray] = None
        if self.type == "api":
            lp = f"{LP}read>api:"
            self.current_frame: int = self.next_frame_id_to_read
            if self.frames_skipped() > 0 or self.frames_processed > 0:
                g.log.debug(
                    f"DBG <=> [{self.frames_processed}/{self.max_frames} frames processed: {self.successful_fids}] "
                    f"- [{self.frames_skipped()}/{self.max_frames} frames skipped: {self.skipped_fids} -- "
                    f"ATTEMPTED: {self.attempted_fids}]"
                )
            else:
                g.log.debug(f"{lp} processing first frame!")
            if str2bool(self.options.get("check_snapshots")):
                g.log.debug(f"{lp} checking snapshots is enabled!")
                # Check if event data available or get data for snapshot fid comparison
                self.current_snapshot = None
                if (not g.past_event and self.current_frame) and (
                    (
                        (
                            self.get_frames_processed() > 0 and self.last_snapshot_id > 0
                        )  # If frames have been processed there must be a snapshot id that was processed
                        or (
                            self.get_frames_processed() == 0 and self.last_snapshot_id == 0
                        )  # If no frames processed and no snapshot processed, run it
                    )
                    and self.frames_processed % 3 == 0  # Only run every 3 frames (seconds)
                ):
                    _grab_event_data(msg=f"grabbing data for snapshot comparisons...")
                    if curr_snapshot := int(g.Event.get("MaxScoreFrameId", 0)):
                        _add_txt = (
                            f" old snapshot frame id was {self.last_snapshot_id}"
                            if self.last_snapshot_id
                            else ""
                        )
                        g.log.debug(
                            f"{lp} current snapshot frame id is {curr_snapshot}{_add_txt}"
                        )
                        if (self.last_snapshot_id and curr_snapshot) and (
                            curr_snapshot > self.last_snapshot_id
                        ):
                            g.log.dbg(
                                f"DEBUG => {lp} current snapshot frame id is not the same as the last snapshot id "
                                f"CURR:{curr_snapshot} - PREV:{self.last_snapshot_id}, grabbing snapshot"
                            )
                            self.current_snapshot = curr_snapshot
                        self.last_snapshot_id = curr_snapshot
                    else:
                        g.log.warning(
                            2,
                            f"{lp} Event: {g.eid} - No Snapshot Frame ID found in ZM API? -> {g.Event = }",
                        )
            if not g.api_event_response or not g.Event:
                _grab_event_data(msg="NO EVENT DATA!!! grabbing from API...")

            #  Check if we have already processed this frame ID 
            g.log.dbg(
                f"DBG => checking processed fids: {self.successful_fids}"
            ) if self.successful_fids else None
            if self.current_frame in self.successful_fids:
                g.log.debug(
                    f"{lp} skipping Frame ID: '{self.current_frame}' as it has already been"
                    f" processed for event {g.eid}"
                )
                return self._processed_frame(append_fid=self.current_frame, skip=True)
            #  SET URL TO GRAB IMAGE FROM 
            fid_url = f"{self.stream}&fid={self.current_frame}"
            if self.current_snapshot:
                g.log.dbg(f"DBG => using snapshot frame id: {self.current_snapshot}")
                fid_url = f"{self.stream}&fid={self.current_snapshot}"

            if g.past_event:
                g.log.warning(
                    f"{lp} this is a past event, max image grab attempts set to 1"
                )
                self.max_attempts = 1
            for image_grab_attempt in range(self.max_attempts):
                image_grab_attempt += 1
                show_url = fid_url
                if g.api.sanitize:
                    show_url = fid_url.replace(
                        g.api.portal_url, f"{g.api.sanitize_str}"
                    )
                g.log.debug(
                    f"{lp} attempt #{image_grab_attempt}/{self.max_attempts} to grab image from URL: {show_url}"
                )
                try:
                    response = g.api.make_request(fid_url)
                except Exception as e:
                    g.log.error(
                        f"{lp} could not make request to URL: {show_url} ERR_MSG={e}"
                    )
                if (
                    response
                    and isinstance(response, requests.Response)
                    and response.status_code == 200
                ):
                    try:
                        img = cv2.imdecode(
                            np.asarray(bytearray(response.content), np.uint8),
                            cv2.IMREAD_COLOR,
                        )
                        self.resized_h_w = self.orig_h_w = img.shape[:2]
                        g.log.debug(
                            f"{lp} image returned from ZM API dimensions: {self.orig_h_w}"
                        )
                        img = self._resize_image(img)

                    except Exception as e:
                        g.log.error(
                            f"{lp} could not build IMAGE from response (cv2.imdecode, cv2.resize) "
                            f"URL={fid_url} ERR_MSG={e}"
                        )
                    # received a reply with an image and constructed the image
                    else:
                        return self._processed_frame(
                            append_fid=self.current_frame, image=img
                        )

                # response code not 200 or no response
                else:
                    resp_msg = ""
                    if response:
                        resp_msg = f" response code={response.status_code} - response={response}"
                    else:
                        resp_msg = f" no response received!"
                    g.log.warn(f"{lp} image was not retrieved!{resp_msg}")

                    _grab_event_data(msg="checking if event has ended...")
                    g.log.dbg(
                        f"{lp} event total Frames: {self.event_tot_frames} -- EndDateTime: {self.event_ended}"
                    )
                    if self.event_ended:  # Assuming event has ended
                        g.log.dbg(f"{lp} event has ended, checking OOB status...")
                        # check current frame to if we are OOB
                        if self.current_frame > self.event_tot_frames:
                            # We are OOB, so we are done
                            g.log.debug(
                                f"{lp} we are OOB (current requested fid: {self.current_frame} - "
                                f"total frames in event: {self.event_tot_frames}) - event has ended, RETURNING None"
                            )
                            return self._processed_frame(
                                append_fid=self.current_frame, end=True
                            )
                    else:
                        g.log.debug(
                            f"{lp} event has not ended yet! Total Frames: {self.event_tot_frames}"
                        )
                    if not g.past_event and (image_grab_attempt < self.max_attempts):
                        g.log.debug(f"{lp} image attempt failed! sleeping for 1 second")
                        sleep(1)

            return self._processed_frame(append_fid=self.current_frame, skip=True)

        # image from file
        # 'delay_between_frames' - probably not
        elif self.type == "file":
            lp: str = "media:read:file:"
            frame = cv2.imread(self.stream)
            self.last_frame_id_read = 1
            self.orig_h_w = frame.shape[:2]
            frame = self._resize_image(frame)
            return frame

        # video from file ???
        # 'delay_between_frames' - possibly? use cases?
        elif self.type == "video":
            attempts = 0
            frames_read = 0
            while True:
                lp = f"{LP}read:video:"
                frame = self.fvs.read()
                frames_read += 1
                self.attempted_fids.add(frames_read)

                if frame is None:
                    attempts += 1
                    if attempts >= self.max_attempts:
                        g.log.error(f"{lp} Error reading frame #-{frames_read}")
                        return
                    else:
                        g.log.debug(
                            f"{lp} error reading frame #-{frames_read} -> {attempts} of "
                            f" a max of {self.max_attempts}"
                        )
                        continue

                self.orig_h_w = frame.shape[:2]
                self.last_frame_id_read = self.next_frame_id_to_read
                self.next_frame_id_to_read += 1
                if (self.last_frame_id_read - 1) % self.fps_fid_skip:
                    g.log.debug(5, f"{lp} skipping frame {self.last_frame_id_read}")
                    self.skipped_fids.add(self.last_frame_id_read)
                    continue

                g.log.debug(
                    2,
                    f"{lp} processing frame: {self.last_frame_id_read}",
                )
                self.successful_fids.add(self.last_frame_id_read)
                break
            # END OF WHILE LOOP

            g.log.debug(f"{lp} ***** RESIZE={self.options.get('resize')}")
            frame = self._resize_image(frame)
            if str2bool(self.options.get("save_frames")) and self.debug_filename:
                save_frames_dir = self.options.get("save_frames_dir", "/tmp")
                if Path(save_frames_dir).exists() and Path(save_frames_dir).is_dir():
                    f_name = f"{save_frames_dir}/{self.debug_filename}-output_video-{self.last_frame_id_read}.jpg"
                    g.log.debug(
                        2,
                        f"{lp}video: stream_sequence is configured to save grabbed frames! saving image to '{f_name}'",
                    )
                    cv2.imwrite(f_name, frame)

            return frame

    def more(self):
        if self.type == "file":
            return False

        elif self.type == "api":
            if self.frame_set:
                # returns true if index is less than allowable max processed frames?
                return self.frame_set_index < self.max_frames
            else:
                g.log.dbg(
                    f"DBG => API TYPE => more() called {self.get_frames_processed() = } -- {self.max_frames = } -- "
                    f"{self.get_frames_processed() < self.max_frames = }"
                )
                return self.get_frames_processed() < self.max_frames

        elif self.type == "video":
            return self.fvs.more()

    def stop(self):
        if self.type == "video":
            self.fvs.stop()

    def get_last_frame(self) -> int:
        return self.last_frame_id_read or 0

    def convert_val(self, cls, info_) -> Any:
        """return *v* converted to class type *cls* object, msg is the ValueError message to display
        if v cannot be converted to cls"""
        ret_val = None
        cls_name = type(cls()).__name__
        for k, v in info_.items():
            if v is None or isinstance(v, cls):
                return v
            try:
                ret_val = cls(v)
            except Exception as exc:
                msg = f"media: error trying to convert '{k}' to a {cls_name} -> {exc}"
                g.log.error(msg)
                # raise ValueError(msg)
            finally:
                return ret_val

    def _processed_frame(self, image=None, append_fid=None, skip=False, end=False):
        """Increment frame set index and frames processed. Both frame set and not frame set compatible"""
        lp = f"{LP}processed_frame:"
        self.frames_processed += 1
        self.last_frame_id_read = append_fid
        self.attempted_fids.add(append_fid)
        if skip:
            self.skipped_fids.add(append_fid)
        else:
            self.successful_fids.add(append_fid)
        if not end:
            if self.frame_set:
                g.log.dbg(f"{lp} frame_set is being used, incrementing frame_set index")
                self.frame_set_index += 1
            else:
                self.next_frame_id_to_read = append_fid + self.fps_fid_skip
                g.log.dbg(
                    f"{lp} incrementing next frame ID to read by {self.fps_fid_skip} = {self.next_frame_id_to_read}"
                )
        elif end or self.frames_processed > self.max_frames:
            _msg = (
                "end has been called, no more images to process!"
                if end
                else f"max_frames ({self.max_frames}) has been reached, stopping!"
            )
            g.log.error(f"{lp} {_msg}")
            return None
        try:
            _image = image.any()
        except Exception:
            return self.read()
        else:
            return image


    def frame_set_length(self):
        return len(self.frame_set) or 0

    def get_current_frame(self):
        return self.current_frame

    def frames_skipped(self):
        return len(self.skipped_fids) or 0

    def frames_tried(self):
        return len(self.attempted_fids) or 0

    def get_debug_filename(self):
        return self.debug_filename

    def image_dimensions(self):
        return {"original": self.orig_h_w, "resized": self.resized_h_w}

    def get_last_read_frame(self):
        return self.last_frame_id_read or 0

    def get_max_frames(self):
        return self.max_frames or 0

    def get_frames_processed(self):
        return self.frames_processed

    def _resize_image(self, frame=None):
        """Resize image to fit in the specified dimensions"""
        if self.resize != "no":
            try:
                self.resize = int(self.resize)
            except TypeError:
                g.log.error(
                    f"media:resize: 'resize' is malformed can only be a whole number (not XXX.YY only XXX) or "
                    f"'no', setting to 'no'..."
                )
                self.options["resize"] = self.resize = "no"
            else:
                frame = resize_image(frame, self.resize)
                self.resized_h_w = frame.shape[:2]
        return frame
