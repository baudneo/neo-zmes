from decimal import Decimal
from os import remove
from pathlib import Path
from time import sleep
from typing import Any, AnyStr, List, Optional, Tuple, Union

import cv2
import numpy as np
import requests
from imutils.video import FileVideoStream as FVStream

from pyzm.api import ZMApi
from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.pyzm_utils import grab_frame_id, resize_image, str2bool

# log prefix to add onto
lp: str = "media:"
g: GlobalConfig


class MediaStream:
    frames_processed: int

    def __init__(
        self,
        stream: Union[str, int],
        type_: Optional[str] = None,
        options: Optional[dict] = None,
    ):
        if type_ is None:
            type_ = "video"
        if not options:
            raise ValueError(f"{lp} no stream options provided!")
        global g
        g = GlobalConfig()
        self.options: Optional[dict] = options
        self.fps: Decimal = Decimal(0.0)
        self.precount: int = 0
        self.postcount: int = 0
        g.log.dbg(f"media:init: options: {options}")
        if self.options.get("fps"):
            self.fps = g.mon_fps = Decimal(self.options.get("fps"))
            g.log.dbg(
                f"{lp} FPS of the monitor has been supplied as -> {self.options['fps']} -- converted to {self.fps = } - {type(self.fps) = }"
            )
        if self.options.get("precount"):
            self.precount = g.mon_pre = int(self.options.get("precount"))
            g.log.dbg(
                f"{lp} precount of the monitor has been supplied as -> {self.options['precount']} -- converted to {self.precount = } - {type(self.precount) = }"
            )
        if self.options.get("postcount"):
            self.postcount = g.mon_post = int(self.options.get("postcount"))
            g.log.dbg(
                f"{lp} postcount of the monitor has been supplied as -> {self.options['postcount']} -- converted to {self.postcount = } - {type(self.postcount) = }"
            )
        self.handled_fids: list[str] = []
        self.fids_skipped: list[str] = []
        # <frame id>: int : cv2.im(de?)encoded image
        self.skip_all_count: int = 0
        self.stream: str = stream
        g.log.dbg(f"DEBUG => stream supplied as {stream}")
        self.type: str = type_
        self.fvs: Optional[FVStream] = None
        self.next_frame_id_to_read: int = 1
        self.last_frame_id_read: int = 0
        self.more_images_to_read: bool = True
        self.frames_processed: int = 0
        self.frames_skipped: int = 0
        # For 'video' type
        self.frames_read: int = 0
        self.api: ZMApi = g.api
        self.frame_set: List[AnyStr] = []
        self.frame_set_index: int = 0
        self.frame_set_len: int = 0
        self.orig_h_w: Optional[Tuple[str, str]] = None
        self.resized_h_w: Optional[Tuple[str, str]] = None
        self.is_deletable: bool = False
        self.allowed_attempts = None
        self.fids_processed: List[AnyStr] = []
        self.last_frame_id_read: Optional[Union[int, str]] = None

        if options.get("delay"):
            try:
                delay = float(options["delay"])
            except ValueError or TypeError as delay_exc:
                g.log.error(
                    f"media: the configured delay is malformed! can only be a number (x or x.xx)"
                )
                g.log.debug(delay_exc)
            else:
                if delay and not g.past_event:
                    g.log.debug(
                        f"{lp} a delay is configured, this only applies one time - waiting for {options['delay']} "
                        f"seconds"
                    )
                    sleep(float(options.get("delay")))

        if isinstance(self.stream, str):
            ext = Path(self.stream).suffix
            if ext.lower() in (".jpg", ".png", ".jpeg"):
                g.log.debug(
                    f"{lp} The supplied stream '{self.stream}' is an image file"
                )
                self.type = "file"
                return

        self.start_frame = int(options.get("start_frame", 1))
        self.frame_skip = int(options.get("frame_skip", 1))
        self.max_frames = int(options.get("max_frames", 0))
        self.batch_attempts = int(options.get("batch_max_attempts", 5))
        self.batch_delay: Union[float, int] = (
            float(options.get("delay_between_batches", 1.0)) or 1.0
        )
        self.frames_before_error = 0
        if (isinstance(self.stream, str) and self.stream.isnumeric()) or (
            isinstance(self.stream, int)
        ):
            # assume it is an event id, in which case we
            # need to access it via ZM API

            if self.options.get("pre_download"):
                # If pre_download and grabbing from API, download the images locally and then run detections
                g.log.debug(
                    f"{lp} Download event images locally before running detections for stream: {g.eid} "
                )
                api_events = self.api.events({"event_id": int(g.eid)})

                if not api_events:
                    g.log.error(f"{lp} no such event {g.eid} found with API")
                    return
                # should only be 1 event as we sent a filter for that event in the call
                for event in api_events:
                    self.stream = event.Event.download_video(
                        download_dir=self.options.get("pre_download_dir", "/tmp/")
                    )
                    self.is_deletable = True
                    self.type = "video"

            # use API
            else:
                self.next_frame_id_to_read = int(self.start_frame)
                self.stream = f"{self.api.get_portalbase()}/index.php?view=image&eid={self.stream}"
                g.log.debug(2, f"{lp} setting 'image' as type for event -> '{g.eid}'")
                self.type = "image"

        if self.options.get("frame_set"):  # process frame_set
            if isinstance(self.options.get("frame_set"), str):
                self.frame_set = self.options.get("frame_set").split(",")
            elif isinstance(self.options.get("frame_set"), list):
                self.frame_set = [str(i).strip() for i in self.options.get("frame_set")]
            else:
                g.log.warning(
                    f"{lp} error in frame_set format (not a string or list), setting to 'snapshot,alarm,snapshot'",
                )
                self.frame_set = ["snapshot", "alarm", "snapshot"]
            g.log.debug(f"DEBUG >>>> {self.frame_set = }")
            self.max_frames = self.frame_set_len = len(self.frame_set)
            if self.type == "video" and (
                "alarm" in self.frame_set or "snapshot" in self.frame_set
            ):
                # remove snapshot and alarm then see if any left before error
                self.frame_set = [
                    str(i)
                    for i in self.options.get("frame_set")
                    if i != "snapshot" or i != "alarm"
                ]
                if not self.frame_set:
                    g.log.error(
                        f"{lp} you are using 'snapshot' or 'alarm' frame ids inside of frame_set with a VIDEO FILE"
                        f", 'snapshot' or 'alarm' are a special feature of grabbing frames from the ZM API. You"
                        f"must specify actual frame ID' when detecting on a VIDEO FILE"
                    )
                    raise ValueError("WRONG FRAME_SET TYPE")

            g.log.debug(
                2,
                f"{lp} processing a maximum of {self.frame_set_len} frame"
                f"{'s' if not self.frame_set_len == 1 else ''}-> {self.frame_set}",
            )
            self.frame_set_index = 0
            self.start_frame = self.frame_set[self.frame_set_index]

        # todo check this logic still works
        if self.type == "video":
            g.log.debug(f"{lp}VID: starting video stream {stream}")
            f = Path(self.stream).name
            self.debug_filename = f"{f}"

            self.fvs = FVStream(self.stream).start()
            sleep(0.5)  # let it settle?
            g.log.debug(f"{lp}VID: first load - skipping to frame {self.start_frame}")
            if self.frame_set:
                while self.fvs.more() and self.frames_read <= self.frame_set_len:
                    self.fvs.read()
                    self.frames_read += 1
            else:
                while self.fvs.more() and self.next_frame_id_to_read < int(
                    self.start_frame
                ):
                    self.fvs.read()
                    self.next_frame_id_to_read += 1

        else:  # Use API
            g.log.debug(2, f"{lp} using API calls for stream -> {stream}")

    def get_debug_filename(self):
        return self.debug_filename

    def image_dimensions(self):
        return {"original": self.orig_h_w, "resized": self.resized_h_w}

    def get_last_read_frame(self):
        return self.last_frame_id_read

    def more(self):

        if self.type == "file":
            return self.more_images_to_read

        if self.frame_set:
            # returns true if index is less than allowable max processed frames?
            return self.frame_set_index < self.max_frames
        if self.frames_processed >= self.max_frames:
            g.log.debug(
                f"media: bailing as we have read {self.frames_processed} frames out of an "
                f"allowed max of {self.max_frames}"
            )
            return False
        else:
            if self.type == "video":
                return self.fvs.more()
            else:
                return self.more_images_to_read

    def stop(self):

        if self.type == "video":
            self.fvs.stop()
        if self.is_deletable:
            try:
                remove(self.stream)
            except Exception as e:
                g.log.error(
                    f"media:stop: could not delete downloaded images: {self.stream}"
                )
                g.log.debug(e)
            else:
                g.log.debug(2, f"media:stop: deleted '{self.stream}'")

    def get_last_frame(self) -> Optional[str]:
        last_frame = None
        if self.handled_fids:
            last_frame = self.handled_fids[-1]
        if self.frame_set_index < self.frame_set_len:
            if last_frame == self.frame_set[self.frame_set_index]:
                last_frame = ""
        return str(last_frame)

    def convert_val(self, cls, k, v, msg=None) -> Any:
        """return *v* converted to class type *cls* object, msg is the ValueError message to display
        if v cannot be converted to cls"""

        if v is None or not v:
            return v
        try:
            ret_val = cls(v)
        except Exception as exc:
            g.log.error(
                f"media: error trying to convert '{k}' to a {repr(cls)} -> {exc}"
            )
            if msg:
                raise ValueError(msg)
        else:
            return ret_val

    def _increment_read_frame(
        self, image=None, append_fid=None, l_frame_id=None, skip_fid: bool = False
    ):
        """Increment frame set index and frames processed. Both frame set and not frame set compatible"""
        if self.frame_set:
            self.last_frame_id_read = (
                l_frame_id if l_frame_id else self.frame_set[self.frame_set_index]
            )

            self.frame_set_index += 1
            if self.frame_set_index <= self.frame_set_len:
                self.frames_processed += 1
            else:
                self.more_images_to_read = False
                self.next_frame_id_to_read = 0
            if image is None:
                return self.read()
            else:
                self.handled_fids.append(append_fid)
                if skip_fid:
                    self.fids_skipped.append(l_frame_id)
                else:
                    self.fids_processed.append(append_fid)
        elif not self.frame_set:
            self.last_frame_id_read = self.next_frame_id_to_read
            self.next_frame_id_to_read += self.frame_skip
            if image is None:
                return self.read()
            else:
                if skip_fid:
                    self.fids_skipped.append(l_frame_id)
                self.fids_processed.append(append_fid)
                self.handled_fids.append(append_fid)
        if image.any():
            return image

    def skip_frame(self, current_frame):
        if self.frame_set:
            self.last_frame_id_read = current_frame
            self.frame_set_index += 1
            self.frames_skipped += 1
            self.fids_skipped.append(current_frame)
            self.handled_fids.append(current_frame)
            g.log.debug(
                f"{lp} skipping Frame ID: '{current_frame}' as it has already been"
                f" processed for event {g.eid}. Frame IDs -> Processed: {self.fids_processed} "
                f"- Skipped {self.fids_skipped}"
            )
            return self.read()

    def read(self):

        response: Optional[requests.Response] = None

        frame: Optional[np.ndarray] = None
        # image from file
        # 'delay_between_frames' - probably not
        if self.type == "file":
            lp: str = "media:read:file:"
            frame = cv2.imread(self.stream)
            self.last_frame_id_read = 1
            self.orig_h_w = frame.shape[:2]
            self.frames_processed += 1
            self.more_images_to_read = False
            if self.options.get("resize", "no") != "no":
                try:
                    self.options["resize"] = int(self.options.get("resize"))
                except TypeError:
                    g.log.error(
                        f"{lp} 'resize' is malformed can only be a whole number (not XXX.YY only XXX) or "
                        f"'no', setting to 'no'..."
                    )
                    self.options["resize"] = "no"
                else:
                    # Get the desired width then pass it to the resize function
                    vid_w: Optional[Union[str, int]] = self.options.get("resize")
                    frame = resize_image(frame, vid_w)
            self.resized_h_w = frame.shape[:2]
            return frame

        # video from file ???
        # 'delay_between_frames' - possibly? use cases?
        elif self.type == "video":
            while True:
                lp = "media:read:video:"
                frame = self.fvs.read()
                self.frames_read += 1

                if frame is None:
                    # this is contiguous errors (outer try) for type == 'image'
                    self.frames_before_error += 1
                    if self.frames_before_error >= self.batch_attempts:
                        g.log.error(f"{lp} Error reading frame #-{self.frames_read}")
                        return
                    else:
                        g.log.debug(
                            f"{lp} error reading frame #-{self.frames_read} -> {self.frames_before_error} of "
                            f" a max of {self.batch_attempts}"
                        )
                        continue

                self.frames_before_error = 0
                self.orig_h_w = frame.shape[:2]
                if self.frame_set and (
                    self.frames_read != int(self.frame_set[self.frame_set_index])
                ):
                    continue
                    # At this stage we are at the frame to read
                if self.frame_set:

                    self.frame_set_index += 1
                    if self.frame_set_index < len(self.frame_set):
                        g.log.debug(
                            4,
                            f"{lp} now moving to frame -> {self.frame_set[self.frame_set_index]}",
                        )
                else:
                    self.last_frame_id_read = self.next_frame_id_to_read
                    self.next_frame_id_to_read += 1
                    if (self.last_frame_id_read - 1) % self.frame_skip:
                        g.log.debug(5, f"{lp} skipping frame {self.last_frame_id_read}")
                        continue

                g.log.debug(
                    2,
                    f"{lp} processing frame:{self.last_frame_id_read}",
                )
                self.fids_processed.append(
                    self.last_frame_id_read if self.last_frame_id_read != 0 else None
                )
                self.frames_processed += 1
                break
            # END OF WHILE LOOP

            g.log.debug(f"{lp} ***** RESIZE={self.options.get('resize')}")
            if self.options.get("resize", "no") != "no":
                vid_w = self.options.get("resize")
                frame = resize_image(frame, vid_w)
            self.resized_h_w = frame.shape[:2]

            if str2bool(self.options.get("save_frames")) and self.debug_filename:
                save_frames_dir = self.options.get("save_frames_dir", "/tmp")
                if Path(save_frames_dir).exists() and Path(save_frames_dir).is_dir():
                    f_name = f"{save_frames_dir}/{self.debug_filename}-output_video-{self.last_frame_id_read}.jpg"
                    g.log.debug(
                        2,
                        f"{lp}video: stream_sequence is configured to save every frame! saving image to '{f_name}'",
                    )
                    cv2.imwrite(f_name, frame)

            return frame

        # grab images frame by frame from ZM API (can pre download frames and then process)
        elif self.type == "image":
            lp = "media:read:image:"
            delayed: bool = False
            comp_fid: Optional[str] = None
            delay_frames = self.convert_val(
                float,
                "delay_between_frames",
                self.options.get("delay_between_frames"),
                f"{lp} error converting 'delay_between_frames' to float",
            )
            if self.frame_set_index >= self.frame_set_len:
                return None

            current_frame: str = str(self.frame_set[self.frame_set_index]).strip()
            if self.frames_skipped > 0 or self.frames_processed > 0:
                g.log.debug(
                    f"{lp} [{self.frames_processed}/{self.max_frames} frames processed: {self.fids_processed}] "
                    f"- [{self.frames_skipped}/{self.max_frames} frames skipped: {self.fids_skipped}]"
                )
                g.log.debug(
                    f"{lp} [Current Requested FID: {current_frame}] - "
                    f"[Last Requested Frame ID: {self.get_last_frame()}]"
                )
                if not g.past_event and delay_frames:
                    # does not delay the first frame read or any frames if it is a past event.
                    # All frames are already written to disk
                    g.log.debug(
                        4,
                        f"{lp} 'delay_between_frames' sleeping {delay_frames} seconds before reading next frame",
                    )
                    delayed = True
                    sleep(delay_frames)
                elif g.past_event and delay_frames:
                    g.log.debug(
                        f"{lp} 'delay_between_frames' is configured but this is a past event, ignoring delay..."
                    )
                if (
                    current_frame.startswith("sn")
                    and self.get_last_frame().startswith("s")
                    and self.options.get("delay_between_snapshots")
                ) and not delayed:
                    g.log.debug(
                        2,
                        f"{lp} sleeping {self.options.get('delay_between_snapshots')} seconds before "
                        f"reading concurrent snapshot frame ('delay_between_snapshots'), last frame was "
                        f"a snapshot and so is this frame request",
                    )
                    snapshot_sleep = self.convert_val(
                        float,
                        "delay_between_snapshots",
                        self.options.get("delay_between_snapshots"),
                        f"{lp} error converting 'delay_between_snapshots' to a float",
                    )
                    delayed = True
                    sleep(snapshot_sleep)

            else:
                g.log.debug(f"{lp} about to process first frame!")

            if not g.api_event_response or not g.Event:
                try:
                    g.Event, g.Monitor, g.Frame = self.api.get_all_event_data()
                except Exception as e:
                    g.log.error(f"{lp} error grabbing event data from API -> {e}")
                    raise e
                else:
                    g.log.debug(f"{lp} grabbed event data from ZM API")

            if current_frame.startswith("a"):
                if g.Event.get("AlarmFrameId"):
                    current_frame = self.frame_set[
                        self.frame_set_index
                    ] = f"a-{g.Event.get('AlarmFrameId')}"
                    g.log.debug(
                        2,
                        f"{lp} Event: {g.eid} - converting 'alarm' to a frame ID -> {current_frame}",
                    )
                else:
                    g.log.debug(
                        2,
                        f"{lp} Event: {g.eid} - No Alarm Frame ID found in ZM API? -> {g.Event = }",
                    )
            elif current_frame.startswith("sn"):
                if not g.past_event:
                    try:
                        g.Event, g.Monitor, g.Frame = self.api.get_all_event_data()
                    except Exception as e:
                        g.log.error(f"{lp} error grabbing event data from API -> {e}")
                        raise e
                    else:
                        g.log.debug(
                            f"{lp} grabbed event data from ZM API in order to grab "
                            f"current snapshot frame ID"
                        )
                if g.Event.get("MaxScoreFrameId"):
                    current_frame = f"s-{g.Event.get('MaxScoreFrameId')}"
                    g.log.debug(
                        2,
                        f"{lp} Event: {g.eid} - converting 'snapshot' to a frame ID -> {current_frame}",
                    )
                else:
                    g.log.debug(
                        2,
                        f"{lp} Event: {g.eid} - No Snapshot Frame ID found in ZM API? -> {g.Event = }",
                    )

            # Check if we have already processed this frame ID before
            comp_fid = grab_frame_id(current_frame)
            for proc_fid in self.fids_processed:
                # convert a-xx or s-xx to xx if needed
                proc_fid = grab_frame_id(str(proc_fid))
                if proc_fid == comp_fid:
                    return self.skip_frame(current_frame)

            fid_url = f"{self.stream}&fid={comp_fid}"

            # outer loop is batch attempts
            max_attempts = self.convert_val(
                int,
                "max_attempts",
                self.options.get("max_attempts", 3),
                f"{lp} error while converting 'max_attempts' to an int",
            )
            sleep_time = self.convert_val(
                float,
                "delay_between_attempts",
                self.options.get("delay_between_attempts", 3.0),
                f"{lp} error while converting 'delay_between_attempts' to a float",
            )
            batch_sleep_time = self.convert_val(
                float,
                "delay_between_batches",
                self.options.get("delay_between_batches", 1.0),
                f"{lp} error while converting 'delay_between_batches' to a float",
            )
            if g.past_event:
                g.log.debug(f"{lp} past event, batch attempts is being set to 1")
                self.batch_attempts = 1
                max_attempts = 1
            for batch_attempt in range(self.batch_attempts):
                batch_attempt += 1
                # make sure correct types

                g.log.debug(
                    f"{lp} starting batch attempt #{batch_attempt} out of "
                    f"{self.batch_attempts}"
                )
                for image_grab_attempt in range(max_attempts):
                    image_grab_attempt += 1
                    try:
                        show_url = (
                            fid_url.replace(
                                self.api.portal_url, f"{self.api.sanitize_str}"
                            )
                            if self.api.sanitize
                            else fid_url
                        )

                        g.log.debug(
                            f"{lp} attempt #{image_grab_attempt}/{max_attempts} to grab image from URL: {show_url}"
                        )
                        response = self.api.make_request(fid_url)
                    except Exception as e:
                        err_msg = f"{e}"
                        if sleep_time and not g.past_event:
                            g.log.debug(
                                f"{lp} error ({err_msg}) while grabbing image, sleeping for {sleep_time} seconds"
                            )
                            sleep(sleep_time)
                    else:
                        g.log.dbg(f"DEBUG!!!>>>> response: {response}")
                        if (
                            response
                            and isinstance(response, requests.Response)
                            and response.status_code == 200
                        ):
                            try:
                                img = cv2.imdecode(
                                    np.asarray(bytearray(response.content), np.uint8),
                                    cv2.IMREAD_COLOR,
                                )
                                self.orig_h_w = img.shape[:2]

                                if str2bool(self.options.get("save_frames")):
                                    save_frames_dir = self.options.get(
                                        "save_frames_dir", "/tmp"
                                    )
                                    if Path(save_frames_dir).is_dir():
                                        f_name = f"{save_frames_dir}/saved-{g.eid}_{comp_fid}.jpg"
                                        g.log.debug(
                                            2,
                                            f"{lp} 'save_frames' is configured! -> saving image to '{f_name}'",
                                        )
                                        try:
                                            cv2.imwrite(f_name, img)
                                        except Exception as exc:
                                            g.log.error(
                                                f"{lp} ERROR writing image to disk -> {exc}"
                                            )
                                    else:
                                        g.log.error(
                                            f"{lp} there is a problem with the configured 'save_frames_dir' "
                                            f"({save_frames_dir}) check the path for spelling mistakes"
                                        )

                                if self.options.get("resize", "no") != "no":
                                    vid_w = int(self.options.get("resize"))
                                    img = resize_image(img, vid_w)
                                    self.resized_h_w = img.shape[:2]
                                else:
                                    g.log.debug(
                                        f"{lp} image returned from ZM API dimensions: {self.orig_h_w}"
                                    )
                            except Exception as e:
                                g.log.error(
                                    f"{lp} could not build IMAGE from response (cv2.imdecode, cv2.resize) "
                                    f"URL={fid_url} ERR_MSG={e}"
                                )
                            # received a reply with an image and constructed the image
                            else:
                                a_fid = (
                                    str(comp_fid)
                                    if self.frame_set
                                    else self.last_frame_id_read
                                )
                                return self._increment_read_frame(
                                    image=img,
                                    append_fid=a_fid,
                                    l_frame_id=current_frame,
                                )

                        # response code not 200 or no response
                        else:
                            g.log.debug(f"DEBUG!!>>> image was not retrieved!")
                            if response:
                                g.log.error(
                                    f"{lp} response code = {response.status_code} - response={response}"
                                )

                            if (
                                sleep_time
                                and not g.past_event
                                and (image_grab_attempt < max_attempts)
                            ):
                                g.log.debug(
                                    f"{lp} image attempt failed! sleeping for {sleep_time} seconds"
                                )
                                sleep(sleep_time)
                g.log.error(
                    f"{lp} batch of attempts FAILED ({batch_attempt}/{self.batch_attempts}) trying to grab "
                    f"frame ID '{comp_fid}'  retrying..."
                )
                if (
                    batch_sleep_time
                    and not g.past_event
                    and (batch_attempt < self.batch_attempts)
                ):
                    g.log.debug(
                        f"{lp} batch FAILED! sleeping for {batch_sleep_time} seconds"
                    )
                    sleep(batch_sleep_time)
            return self.skip_frame(current_frame)
