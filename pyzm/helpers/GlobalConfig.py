from dataclasses import dataclass, field
from decimal import Decimal
from pathlib import Path
from time import perf_counter
from typing import Union, Optional, List

# from pyzm.api import ZMApi
from pyzm.ZMLog import ZMLog


class Singleton(type):
    """Python implementation of the Singleton pattern, to be used as a metaclass."""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


@dataclass()
class GlobalConfig(metaclass=Singleton):
    """Customized dataclass to hold global objects that ZMES utilizes"""
    pushover = None  # Pushover object
    base_path: Optional[str] = field(default=None)

    mon_name: str = field(default_factory=str)
    event_cause: str = field(default_factory=str)
    event_path: str = field(default_factory=str)

    last_processed_eid: Union[str, int] = field(
        default_factory=int
    )  # For caching purposes
    cache_dir: Path = field(default_factory=Path)  # For caching purposes
    cache_data: dict = field(default_factory=dict)  # For caching purposes

    DEFAULT_CONFIG: dict = field(default_factory=dict)  # hardcoded config values
    past_event: bool = False  # Is this a past event?
    secrets_filename: str = field(default_factory=str)
    config_filename: str = field(default_factory=str)

    args: dict = field(default_factory=dict)  # command line arguments

    eid: Optional[
        Union[str, int]
    ] = None  # global Event ID or the name of the input file/video
    mid: Optional[int] = None  # global Monitor ID
    # logger and alias, starts with a buffer that is flushed once ZMLog is initialized
    logger: Optional[ZMLog] = None
    log: Optional[ZMLog] = None
    # ZM API Interface -> todo: fix circular imports
    api: Optional = None
    # object that will hold active config values
    config: dict = field(default_factory=dict)
    # how long it takes for an animation to be created
    animation_seconds: Optional[perf_counter] = None
    # return from requesting event data, contains Monitor, Frames and Event Table data
    api_event_response: Optional[dict] = None
    api_monitor_response: Optional[dict] = None
    # Total frame buffer length for current event / video /image
    event_tot_frames: Optional[int] = None
    # Containers to hold the event Table data
    Frame: Optional[list] = None
    Monitor: Optional[dict] = None
    Monitor_new: Optional[dict] = None
    Event: Optional[dict] = None

    mon_fps: int = field(default_factory=int)
    mon_pre: int = field(default_factory=int)
    mon_post: int = field(default_factory=int)

    # Holds all threads
    ThreadPool: dict = field(default_factory=dict)
    # Did the script run at the START or END of an event
    event_type: str = field(default_factory=str)
    # Extras
    extras: dict = field(default_factory=dict)

    # Method to set up the aliases
    def __setlog__(self, log: ZMLog):
        self.logger = self.log = log



