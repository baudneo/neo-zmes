"""Templates for structures"""
__all__ = ["POLYGON_TEMPLATE", "RESOLUTION_STRINGS", "PUSHOVER_SOUND_WEIGHTS"]

from typing import Tuple, Dict

POLYGON_TEMPLATE: Dict[str, None] = {
    "name": None,
    "value": None,
    "resolution": None,
    "pattern": None,
    "contains": None,
    "max_size": None,
    "min_conf": None,
    "conf_upper": None,
    "past_area_diff": None,
}

RESOLUTION_STRINGS: Dict[str, Tuple[int, int]] = {
    # pixel resolution string to tuple
    "4k uhd": (3840, 2160),
    "uhd": (3840, 2160),
    "4k": (4096, 2160),
    "6MP": (3072, 2048),
    "5MP": (2592, 1944),
    "4MP": (2688, 1520),
    "3MP": (2048, 1536),
    "2MP": (1600, 1200),
    "1MP": (1280, 1024),
    "1440p": (2560, 1440),
    "2k": (1920, 1080),
    "1080p": (1920, 1080),
    "960p": (1280, 960),
    "720p": (1280, 720),
    "full pal": (720, 576),
    "full ntsc": (720, 480),
    "pal": (704, 576),
    "ntsc": (704, 480),
    "4cif": (704, 480),
    "2cif": (704, 240),
    "cif": (352, 240),
    "qcif": (176, 120),
    "480p": (854, 480),
    "360p": (640, 360),
    "240p": (426, 240),
    "144p": (256, 144),
}

PUSHOVER_SOUND_WEIGHTS: Dict[str, int] = {
    "person": 0,
    "dog": 1,
    "cat": 2,
    "car": 3,
}
