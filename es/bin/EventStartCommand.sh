#!/usr/bin/env bash
trap 'cleanup' SIGINT SIGTERM
cleanup() {
  exit 1
}

CONFIG_FILE="/etc/zm/objectconfig.yml"
ZMES_DIR="/var/lib/zmeventnotification"
SECRETS_FILE="/etc/zm/secrets.yml"
# ENVIRONMENT VARIABLES USED BY NEO ZMES (WIP)
[ -n "$ZMES_CONFIG_FILE" ] && CONFIG_FILE="$ZMES_CONFIG_FILE"
[ -n "$ZMES_INSTALL_DIR" ] && ZMES_DIR="$ZMES_INSTALL_DIR"
[ -n "$ZMES_SECRETS_DIR" ] && SECRETS_FILE="$ZMES_SECRETS_DIR"

MID=$2
EID=$1
[[ -z $ZM_ML_FILE ]] && ZM_ML_FILE='/home/zmadmin/zm_ml/examples/eventstart.py'

ZMES=(
  python3.9
  "${ZMES_DIR}/bin/zm_detect.py"
  --event-id "${EID}"
  --monitor-id "${MID}"
  --config "${CONFIG_FILE}"
  --live
)
echo -e "\n\nRunning ZMES script: ${ZMES[*]}\n\n"
DET_OUTPUT=$("${ZMES[@]}")
echo -e "\n\nZMES output: ${DET_OUTPUT}\n\n"

echo 0
exit 0
