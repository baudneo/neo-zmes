#!/usr/bin/python3.9
"""Main script to kick off object detection, allows for local and remote detections. This script is also
responsible for notifications via push and MQTT.
"""
import copy
import glob
import hashlib
import json
import os
import signal
import subprocess
import tempfile
import time
import urllib.parse
from argparse import ArgumentParser
from ast import literal_eval
from configparser import ConfigParser
from datetime import datetime
from decimal import Decimal
from functools import partial
from io import BytesIO
from pathlib import Path
from pickle import dumps as pkl_dumps, loads as pkl_loads
from threading import Thread
from traceback import format_exc
from typing import Optional, Union, NoReturn, List, Tuple, Set

import cv2
import numpy as np
import requests
import urllib3
import yaml
from sqlalchemy import MetaData, create_engine, select
from sqlalchemy.engine import Engine, Connection, CursorResult
from sqlalchemy.exc import SQLAlchemyError

from pyzm import __version__ as pyzm_version
from pyzm.ZMLog import sig_intr, sig_log_rot, which
from pyzm.api import ZMApi, version_tuple
from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.helpers.Pushover import Pushover, do_po_emerg
from pyzm.helpers.pyzm_utils import (
    create_animation,
    LogBuffer,
    create_api,
    do_hass,
    do_mqtt,
    draw_bbox,
    grab_frame_id,
    pretty_print,
    resize_image,
    start_logs,
    str2bool,
    verify_items,
    write_text,
    import_zm_zones,
)
from pyzm.interface import ZMESConfig
from pyzm.interface import ZMES_DEFAULT_CONFIG as DEFAULT_CONFIG
from pyzm.ml.detect_sequence import DetectSequence

lp: str = "zmes:"
__app_version__: str = "0.0.3"
g: GlobalConfig
loaded_cache: bool = False


def _get_jwt_filename(ml_api_url: str) -> str:
    """Get filename for mlapi JWT"""
    _file_name = ml_api_url.lstrip("http://").lstrip("https://")
    # If there is a :port
    _file_name = _file_name.split(":")
    if len(_file_name) > 1:
        _file_name = _file_name[0]
    return f"{_file_name}_mlapi_tkn"


def remote_login(user: str, password: str, ml_api_url: str):
    access_token: Optional[str] = None
    lp = "zmes:mlapi:login:"
    ml_login_url: str = f"{ml_api_url}/login"
    _file_name: str = _get_jwt_filename(ml_api_url)

    jwt_file: Path = g.cache_dir / _file_name
    if jwt_file.exists() and jwt_file.is_file():
        try:
            data = json.loads(jwt_file.read_text())
        except ValueError as v_exc:
            g.log.error(
                f"{lp} JSON error -> loading access token from file, removing file... \n{v_exc}"
            )
            try:
                jwt_file.unlink(missing_ok=True)
            except Exception as exc:
                g.log.error(f"{lp} error while trying to delete '{jwt_file}'")
            access_token = None
        else:
            generated = data["time"]
            expires = data["expires"]
            access_token = data["token"]
            # epoch timestamp
            now = time.time()
            # let's make sure there is at least 10 secs left
            if int(now + 10 - generated) >= expires:
                g.log.debug(
                    f"{lp} found access token, but it has or is about to expire. Need to login to MLAPI host..."
                )
                access_token = None
            else:
                g.log.debug(
                    f"{lp} no need to login, access token is valid for {now - generated} sec",
                )
    # Get API access token
    if access_token is None:
        g.log.debug(f"{lp} requesting AUTH JWT from MLAPI")
        try:
            http_timeout: int = 30
            r = requests.post(
                url=ml_login_url,
                data=json.dumps(
                    {
                        "username": user,
                        "password": password,
                    }
                ),
                headers={"content-type": "application/json"},
                timeout=http_timeout,
            )
            r.raise_for_status()

        except Exception as ex:
            g.log.error(f"{lp} ERROR request post -> {ex}")
            raise ValueError("NO_GATEWAY")
        else:
            data = r.json()
            access_token = data.get("access_token")
            if not access_token:
                g.log.error(f"{lp} NO ACCESS TOKEN RETURNED! data={data}")
                raise ValueError("error getting remote API token")
            try:
                w_data = {
                    "token": access_token,
                    "expires": data.get("expires"),
                    "time": time.time(),
                }
                jwt_file.write_text(json.dumps(w_data))
            except Exception as ex:
                g.log.error(f"{lp} error while writing MLAPI AUTH JWT to temp cache!")
                g.log.debug(f"{lp} EXCEPTION>>> {ex}")
            else:
                g.log.debug(2, f"{lp} writing MLAPI AUTH JWT to temp cache")
    return access_token


def remote_detect(stream_options: Optional[dict] = None, route: Optional[dict] = None):
    """Sends an http request to mlapi host with data needed for inference"""
    # This uses mlapi (https://github.com/baudneo/mlapi) to run inference with the sent data and converts format to
    # what is required by the rest of the code.
    lp: str = "zmes:mlapi:"
    model: str = "object"  # default to object
    access_token: Optional[str] = None
    auth_header = Optional[dict]
    show_header: Optional[dict] = None
    files: dict = {}
    # ml_routes integration, we know ml_enabled is True
    ml_api_url: Optional[str] = route["gateway"]
    route_name: str = route["name"] or "route_name_not_set"
    auth_enabled: bool = False
    show_gateway = (
        ml_api_url
        if not str2bool(g.config.get("sanitize_logs"))
        else ml_api_url.replace(g.config.get("portal"), g.config.get("sanitize_str"))
    )
    g.log.info(
        f"|----------= Encrypted Route Name: '{route_name}' | Gateway URL: '{show_gateway}' | "
        f"Weight: {route['weight']} =----------|"
    )
    ml_object_url: str = f"{ml_api_url}/detect/object?type={model}"

    if not route.get("user") or not route.get("pass"):
        g.log.error(f"{lp} No MLAPI user/password configured")
        raise ValueError("NO CREDENTIALS")
    if not access_token:
        try:
            access_token = remote_login(route["user"], route["pass"], route["gateway"])
        except Exception as ex:
            g.log.warning(f"{lp} ERROR getting remote API token -> {ex}")
            raise ValueError(f"NO MLAPI TOKEN")
        else:
            if not access_token:  # add a re login loop?
                raise ValueError(f"NO MLAPI TOKEN")
            auth_header = {"Authorization": f"Bearer {access_token}"}
            if str2bool(g.config.get("sanitize_logs")):
                show_header = {
                    "Authorization": f"{auth_header.get('Authorization')[:57]}...{g.config.get('sanitize_str')}"
                }
            else:
                show_header = auth_header

    file_image: Optional[np.ndarray] = None
    if g.args.get("file"):  # File Input
        input_ = g.args["file"]
        g.log.debug(
            2,
            f"{lp} '--file' supplied -> reading image from '{input_}'",
        )
        file_image = cv2.imread(input_)  # read from file
        if (
            g.config.get("resize", "no") != "no"
        ):  # resize before converting to http serializable
            vid_w = g.config["resize"]
            image = resize_image(file_image, vid_w)
            _succ, jpeg = cv2.imencode(".jpg", image)
            if not _succ:
                g.log.error(f"{lp} ERROR: cv2.imencode('.jpg', <FILE IMAGE>)")
                raise ValueError("NO INPUT IMAGE ENCODE")
            files = {"image": ("image.jpg", jpeg.tobytes(), "application/octet")}
    # ml-overrides grabs the default value for these patterns because their actual values are held in g.config
    # we can send these now and enforce them? but if using mlapi should mlapiconfig.yml take precedence over
    # ml_overrides? model_sequence already overrides mlapi, so..... ???
    ml_overrides: dict = {
        # Only enable when we are running with --file
        "enable": True if g.args.get("file") else False,
        "model_sequence": g.config["ml_sequence"]
        .get("general", {})
        .get("model_sequence"),
        "object": {
            "pattern": g.config["ml_sequence"]
            .get("object", {})
            .get("general", {})
            .get("object_detection_pattern")
        },
        "face": {
            "pattern": g.config["ml_sequence"]
            .get("face", {})
            .get("general", {})
            .get("face_detection_pattern")
        },
        "alpr": {
            "pattern": g.config["ml_sequence"]
            .get("alpr", {})
            .get("general", {})
            .get("alpr_detection_pattern")
        },
    }
    # Get api credentials ready, sent from ZM
    encrypted_data: dict = {}
    try:
        from cryptography.fernet import Fernet

        # assign Fernet object to a variable
    except ImportError:
        g.log.error(
            f"{lp} the cryptography library does not seem to be installed! Please install using -> "
            f"(sudo) pip3 install cryptography"
        )
        Fernet = None
        # raise an exception to trigger the next route or local fallback
        raise ValueError(f"NO CRYPTOGRAPHY LIBRARY")
    else:
        if thread_api := g.ThreadPool.get("api_thread"):
            if thread_api and thread_api.is_alive():
                g.log.debug(
                    2,
                    f"{lp} API thread is alive -> waiting for thread to finish",
                )
                thread_api.join()
        try:
            # get the credential data needed to pass mlapi
            kickstart: dict = g.api.cred_dump()
            kickstart["api_url"] = g.api.api_url
            kickstart["portal_url"] = g.api.portal_url
            auth_enabled = kickstart.pop("enabled")
            if kickstart.get("access_token"):
                # encode str into bytes for encryption use
                key: str = route["enc_key"].encode("utf-8")
                # init the Fernet object with the encryption key
                f: Fernet = Fernet(key=key)
        except Exception as exc:
            g.log.error(
                f"{lp} it appears the encryption key provided is malformed! "
                f"check the encryption key in route '{route_name}'"
            )
            g.log.debug(
                f"{lp} {exc} -- KEY length: {len(route['enc_key'])} KEY: {route['enc_key']}"
            )
            g.log.debug(f"DEBUG <>>> Exception -> {exc}")

            # Invalid base64-encoded string: number of data characters (13)
            # cannot be 1 more than a multiple of 4
            _flush_cache(g.cache_dir)
            # raise an exception to trigger the next route or local fallback
            raise ValueError(f"encryption key malformed for {route_name}")
        else:
            # Auth type and creds based on which type
            if g.api.auth_type and g.api.auth_type == "token":
                encrypted_data = {
                    f.encrypt(str(k).encode("utf-8"))
                    .decode(): f.encrypt(str(v).encode("utf-8"))
                    .decode()
                    for k, v in kickstart.items()
                    if v is not None
                }
            else:
                g.log.error(f"{lp} BASIC_AUTH or NO_AUTH situation")
                encrypted_data = kickstart
            # Add the route name after encryption so that it is readable on the other end
            encrypted_data["name"] = route_name
            encrypted_data["enabled"] = auth_enabled

    # ------------------------------ Encryption END ---------------------------------
    if not stream_options.get("resize") and g.config.get("resize"):
        # make 'resize' activated by just having a value in the config
        stream_options["resize"] = g.config["resize"]
    mlapi_json: dict = {
        "version": __app_version__,
        "mid": g.mid,
        "reason": g.args.get("reason"),
        "stream": g.eid,
        "stream_options": stream_options,
        "ml_overrides": ml_overrides,
        "sub_options": None,
        "encrypted data": encrypted_data,
    }
    # files = {
    #     'document': (local_file_to_send, open(local_file_to_send, 'rb'), 'application/octet'),
    #     'datas' : ('datas', json.dumps(datas), 'application/json'),
    # }
    if files:
        # If we are sending a file we must add the JSON to the files dict (flask interprets this as multipart)
        files["json"] = json.dumps(mlapi_json)
    _files_str: str = f"****** Files: {files}\n"
    g.log.debug(
        2,
        f"MLAPI request data\n** Gateway URL: '"
        f"{show_gateway}' using auth_header={show_header}\n****** JSON: "
        f"stream: {mlapi_json['stream']} | mid: {mlapi_json['mid']} | reason: {mlapi_json['reason']}"
        f"\n****** JSON: stream options: {mlapi_json['stream_options']}\n{_files_str if files else ''}",
    )

    try:
        from requests_toolbelt.multipart import decoder

        http_timeout: int = 60 * 5
        r: requests.post = requests.post(
            url=ml_object_url,
            headers=auth_header,
            # json doesn't send when sending files to mlapi, so we send the file and the json together
            json=mlapi_json if not files else None,
            files=files,
            timeout=http_timeout,
        )
        r.raise_for_status()
    except Exception as all_ex:
        g.log.error(f"{lp} error during post to mlapi host-> {all_ex}")
        raise all_ex
    else:
        data: Optional[dict] = None
        multipart_data: decoder.MultipartDecoder = (
            decoder.MultipartDecoder.from_response(r)
        )
        part: decoder.MultipartDecoder.from_response
        img: Optional[np.ndarray] = None
        for part in multipart_data.parts:
            if (
                part.headers.get(b"Content-Type") == b"image/jpeg"
                or part.headers.get(b"Content-Type") == b"application/octet"
            ):
                g.log.debug(f"{lp} decoding jpeg from the multipart response")
                img = part.content
                img = np.frombuffer(img, dtype=np.uint8)
                img = cv2.imdecode(img, cv2.IMREAD_UNCHANGED)

            elif part.headers.get(b"Content-Type") == b"application/json":
                g.log.debug(
                    f"{lp} parsed JSON detection data from the multipart response"
                )
                data = json.loads(part.content.decode("utf-8"))

        if img is not None and len(img.shape) <= 3:
            # check if the image is already resized to the configured 'resize' value
            if (
                stream_options.get("resize", "no") != "no"
                and stream_options.get("resize") != img.shape[1]
            ):
                img = resize_image(img, stream_options["resize"])
            data["matched_data"]["image"] = img
        return data


def get_es_version() -> str:
    """Get the ZMES Perl script version"""
    # Get zmeventnotification.pl VERSION
    es_version: str = "(?)"
    try:
        from shutil import which

        es_version = subprocess.check_output(
            [which("zmeventnotification.pl"), "--version"]
        ).decode("ascii")
    except Exception as all_ex:
        g.log.error(
            f"{lp} ERROR while grabbing zmeventnotification.pl VERSION -> {all_ex}"
        )
        es_version = "Unknown"
    return es_version.rstrip()


def get_fps_zmu(
    mid: Union[int, str],
    user: Optional[str] = None,
    password: Optional[str] = None,
    token: Optional[str] = None,
) -> float:
    """Use zmu to get a monitors reported FPS"""
    fps: float = 0.0
    g.log.dbg(
        f"DEBUG<><>>> getting FPS for monitor {mid} - TOKEN?: {not not token} / USER?: {not not user} / PASSWORD?: {not not password}"
    )
    try:
        zmu_cmd = ["zmu", "-m", str(mid), "--fps"]
        if token and user and password:
            g.log.dbg(f"DEBUG>>> USER PASSWORD and TOKEN supplied, using User/Pass")
            zmu_cmd.extend(["-U", user, "-P", password])
        elif user and password:
            g.log.dbg(f"DEBUG>>> USER and PASSWORD supplied, using User/Pass")
            # use the user/password to get the FPS
            zmu_cmd.extend(["-U", user, "-P", password])
        elif user and not password:
            # log that the user needs a password
            g.log.error(f"{lp} zmu user {user} needs a password. USER but no PASSWORD")
        elif password and not user:
            # log that the password needs a user
            g.log.error(f"{lp} zmu password needs a user. PASSWORD but no USER")
        elif token:
            # use the token to get the FPS
            g.log.dbg(f"DEBUG>>> TOKEN supplied, using Token")
            zmu_cmd.extend(["-T", token])
        g.log.dbg(f"DEBUG>>> calling zmu using these commands -> {zmu_cmd}")
        output = subprocess.check_output(zmu_cmd).decode("ascii")
        g.log.dbg(f"DEBUG>>> zmu output before parsing -> {output}")
    except Exception as all_ex:
        g.log.error(f"{lp} ERROR while grabbing zmu FPS -> {all_ex}")
    else:
        if output:
            g.log.dbg(f"DBG>>> parsing zmu output into something usable!")
            fps = round(float(output.split(" ")[1].rstrip(",")))
    finally:
        g.log.dbg(f"DEBUG>>> zmu FPS -> {fps}")
        g.mon_fps = Decimal(fps)
        return fps


def parse_cli_args():
    """Parse CLI arguments into a dict"""
    ap = ArgumentParser()
    ap.add_argument(
        "--shm-poll",
        action="store_true",
        help="From the zmeventnotification.pl Perl daemon",
    )
    ap.add_argument("--flush-cache", action="store_true", help="Flush cache")
    ap.add_argument("--docker", action="store_true", help="verbose output")
    ap.add_argument("--secrets", help="path to secrets file")
    ap.add_argument(
        "-et", "--event-type", help="event type-> start or end", default="start"
    )
    ap.add_argument(
        "-c",
        "--config",
        help="config file with path Default: /etc/zm/objectconfig.yml",
        default="/etc/zm/objectconfig.yml",
    )
    ap.add_argument(
        "-e",
        "--event-id",
        "--eventid",
        dest="event_id",
        help="event ID to retrieve (Required)",
    )
    ap.add_argument(
        "-p",
        "--eventpath",
        "--event-path",
        dest="event_path",
        help="path to store objdetect image file, usually passed by perl script",
        default="",
    )
    ap.add_argument(
        "-m",
        "--monitor-id",
        help="monitor id - For use by the PERL script (Automatically found)",
    )
    ap.add_argument(
        "-v", "--version", help="print version and quit", action="store_true"
    )
    ap.add_argument(
        "--bareversion", help="print only app version and quit", action="store_true"
    )
    ap.add_argument(
        "-o",
        "--output-path",
        help="internal testing use only - path for debug images to be written",
    )
    ap.add_argument(
        "-f", "--file", help="internal testing use only - skips event download"
    )
    ap.add_argument(
        "-r",
        "--reason",
        help="reason for event (notes field in ZM i.e -> Motion:Front Yard)",
    )
    ap.add_argument(
        "-n",
        "--notes",
        help="updates notes field in ZM with detections",
        action="store_true",
    )
    ap.add_argument(
        "-d", "--debug", help="enables debug with console output", action="store_true"
    )
    ap.add_argument(
        "-bd",
        "--baredebug",
        help="enables debug without console output (if monitoring log files with 'tail -F', as an example)",
        action="store_true",
    )

    ap.add_argument(
        "-lv",
        "--live",
        help="this is a live event (used internally by perl script; affects logic)",
        action="store_true",
    )

    args, _ = ap.parse_known_args()
    args = vars(args)
    if not args:
        print(f"{lp} ERROR-FATAL -> no args!")
        exit(1)
    else:
        if args.get("version"):
            print(f"app:{__app_version__}, pyzm:{pyzm_version}")
            exit(0)
        elif args.get("bareversion"):
            print(f"{__app_version__}")
            exit(0)
        if not args.get("config"):
            print(f"{lp} ERROR-FATAL -> no config file supplied!")
            raise SyntaxError("--config REQUIRED")
        g.config_filename = args["config"]
        g.cache_dir = Path(tempfile.gettempdir()) / "zmes"
        g.event_type = args.get("event_type")

        if args.get("event_id"):
            g.eid = int(args["event_id"])
        elif args.get("file"):
            g.eid = args["file"]
        else:
            print(
                "--event-id <Event ID #> or --file </path/to/file.(jpg/png/mp4)> REQUIRED"
            )
            raise SyntaxError("--event-id or --file REQUIRED")

        if args.get("flush_cache"):
            print(f"{lp} flushing cache! -> '--flush-cache' was supplied")
            _flush_cache(g.cache_dir)

        thread_secrets_fn: Thread = Thread(
            name="get secrets filename",
            target=_get_secret_filename,
            args=[g.config_filename],
            daemon=True,
        )
        thread_secrets_fn.start()
        g.ThreadPool["secret_filename"] = thread_secrets_fn

    return args


def _db_create() -> Engine:
    """A private function to retrieve the Monitor ID from the ZoneMinder DataBase"""
    # From @pliablepixels SQLAlchemy work - all credit goes to them.
    lp: str = "ZM-DB:"
    db_data: dict = {}
    db_config = {
        "conf_path": os.getenv(
            "PYZM_CONFPATH", "/etc/zm"
        ),  # we need this to get started
        "dbuser": os.getenv("PYZM_DBUSER"),
        "dbpassword": os.getenv("PYZM_DBPASSWORD"),
        "dbhost": os.getenv("PYZM_DBHOST"),
        "webuser": os.getenv("PYZM_WEBUSER"),
        "webgroup": os.getenv("PYZM_WEBGROUP"),
        "dbname": os.getenv("PYZM_DBNAME"),
        "logpath": os.getenv("PYZM_LOGPATH"),
        "log_level_syslog": os.getenv("PYZM_SYSLOGLEVEL"),
        "log_level_file": os.getenv("PYZM_FILELOGLEVEL"),
        "log_level_db": os.getenv("PYZM_DBLOGLEVEL"),
        "log_debug": os.getenv("PYZM_LOGDEBUG"),
        "log_level_debug": os.getenv("PYZM_LOGDEBUGLEVEL"),
        "log_debug_target": os.getenv("PYZM_LOGDEBUGTARGET"),
        "log_debug_file": os.getenv("PYZM_LOGDEBUGFILE"),
        "server_id": os.getenv("PYZM_SERVERID"),
        "dump_console": os.getenv("PYZM_DUMPCONSOLE", False),
        "driver": os.getenv("PYZM_DBDRIVER", "mysql+mysqlconnector"),
    }
    files = []
    conf_path: Path = Path(db_config["conf_path"])
    if conf_path.exists():
        for fi in glob.glob(f"{conf_path}/conf.d/*.conf"):
            files.append(fi)
        files.sort()
        files.insert(0, f"{db_config['conf_path']}/zm.conf")
        config_file = ConfigParser(interpolation=None, inline_comment_prefixes="#")
        try:
            for f in files:
                with open(f, "r") as zm_conf_file:
                    # This adds [zm_root] section to the head of each zm .conf.d config file,
                    # not physically only in memory
                    config_file.read_string(f"[zm_root]\n{zm_conf_file.read()}")
        except Exception as exc:
            g.log.error(f"{lp} error opening ZoneMinder .conf files! -> {files}")
            g.log.debug(f"{format_exc()}")
            # Exit closes the Log thread
            g.log.log_close(exit=1)
            # This will exit this thread
            exit(1)
        else:
            conf_data = config_file["zm_root"]
            db_data["conf_data"] = conf_data
            if not db_config.get("dbuser"):
                db_config["dbuser"] = conf_data["ZM_DB_USER"]
            if not db_config.get("dbpassword"):
                db_config["dbpassword"] = conf_data["ZM_DB_PASS"]
            if not db_config.get("dbhost"):
                db_config["dbhost"] = conf_data["ZM_DB_HOST"]
            if not db_config.get("dbname"):
                db_config["dbname"] = conf_data["ZM_DB_NAME"]
            db_data["db_config"] = db_config

        db_data["connection_str"] = connection_str = (
            f"{db_config['driver']}://{db_config['dbuser']}"
            f":{db_config['dbpassword']}@{db_config['dbhost']}"
            f"/{db_config['dbname']}"
        )

        try:
            engine: Optional[Engine]
            db_data["engine"] = engine = create_engine(
                connection_str, pool_recycle=3600
            )
            conn: Optional[Connection] = engine.connect()
        except SQLAlchemyError as e:
            conn = None
            engine = None
            g.log.error(f"DB configs - {connection_str}")
            g.log.error(f"Could not connect to DB, message was: {e}")
        else:

            g.extras["db_data"] = db_data
            conn.close()
            return engine


def _db_mid(engine: Optional[Engine] = None):
    if engine is None:
        engine = g.extras.get("db_data", {}).get("engine")

    meta: MetaData = MetaData(engine)
    # New reflection method, only reflect the Events and Monitors tables
    meta.reflect(only=["Events", "Monitors", "Monitor_Status"])
    try:
        conn: Optional[Connection] = engine.connect()
    except SQLAlchemyError as e:
        conn = None
        engine = None
        g.log.error(f"Could not connect to DB, message was: {e}")
    else:
        e_select: select = select([meta.tables["Events"].c.MonitorId]).where(
            meta.tables["Events"].c.Id == g.eid
        )
        select_result: CursorResult = conn.execute(e_select)

        mid: Optional[str] = None
        for row in select_result:
            mid = row[0]
        select_result.close()
        del select_result
        if mid:
            g.mid = int(mid)
            g.log.debug(f"{lp} ZoneMinder DB returned Monitor ID: {g.mid}")
        else:
            g.log.warning(
                f"{lp} the database query did not return a monitor ID for this event?"
            )
        # Get Monitor 'Name'
        m_select: select = select([meta.tables["Monitors"].c.Name]).where(
            meta.tables["Monitors"].c.Id == g.mid
        )
        select_result: CursorResult = conn.execute(m_select)
        mon_name: Optional[str] = None
        for mon_row in select_result:
            mon_name = mon_row[0]
        select_result.close()
        if mon_name:
            g.mon_name = mon_name
            g.log.debug(f"{lp} ZoneMinder DB returned monitor name ('{mon_name}')")
        else:
            g.log.warning(
                f"{lp} the database query did not return a monitor name ('Name') for monitor ID {g.mid}"
            )

        # Get Monitor Pre/Post Event Count
        pre_event_select: select = select(
            [meta.tables["Monitors"].c.PreEventCount]
        ).where(meta.tables["Monitors"].c.Id == g.mid)
        select_result: CursorResult = conn.execute(pre_event_select)
        mon_pre: Optional[str] = None
        for mon_row in select_result:
            mon_pre = mon_row[0]
        select_result.close()
        if mon_pre:
            g.mon_pre = int(mon_pre)
            g.log.debug(
                f"{lp} ZoneMinder DB returned monitor PreEventCount ('{mon_pre}')"
            )
        else:
            g.log.warning(
                f"{lp} the database query did not return monitor pre-event count ('PreEventCount') for monitor ID {g.mid}"
            )
        # PostEventCount
        post_event_select: select = select(
            [meta.tables["Monitors"].c.PostEventCount]
        ).where(meta.tables["Monitors"].c.Id == g.mid)
        select_result: CursorResult = conn.execute(post_event_select)
        mon_post: Optional[str] = None
        for mon_row in select_result:
            mon_post = mon_row[0]
        select_result.close()
        if mon_post:
            g.mon_post = int(mon_post)
            g.log.debug(
                f"{lp} ZoneMinder DB returned monitor PostEventCount ('{mon_post}')"
            )
        else:
            g.log.warning(
                f"{lp} the database query did not return monitor post-event count ('PostEventCount') for monitor ID {g.mid}"
            )
        # Get Monitor capturing FPS
        ms_select: select = select([meta.tables["Monitor_Status"].c.CaptureFPS]).where(
            meta.tables["Monitor_Status"].c.MonitorId == g.mid
        )
        select_result: CursorResult = conn.execute(ms_select)
        mon_fps: Optional[float] = None
        for mons_row in select_result:
            g.log.debug(f"{lp} Monitor_Status row: {mons_row}")
            mon_fps = float(mons_row[0])
        select_result.close()
        if mon_fps is not None:
            g.mon_fps = Decimal(mon_fps)
            g.log.debug(f"{lp} ZoneMinder DB returned monitor FPS ('{mon_fps}')")
        else:
            g.log.warning(
                f"{lp} the database query did not return monitor FPS ('CaptureFPS') for monitor ID {g.mid}"
            )


def _db_reason(engine: Optional[Engine] = None):
    lp: str = "zmes:db:"
    reason: Optional[str] = None
    scheme: Optional[str] = None
    storage_id: Optional[int] = None
    start_datetime: Optional[datetime] = None
    storage_path: Optional[str] = None
    event_path: Optional[str] = None
    if engine is None:
        engine = g.extras.get("db_data", {}).get("engine")
    meta: Optional[MetaData] = MetaData(engine)
    try:
        meta.reflect(only=["Events", "Storage"])
        conn: Optional[Connection] = engine.connect()
    except SQLAlchemyError as e:
        conn = None
        engine = None
        meta = None
        g.log.error(f"Could not connect to DB, message was: {e}")
    else:
        g.log.debug(f"{lp} DB connection successful")
        reason_select: select = select([meta.tables["Events"].c.Notes]).where(
            meta.tables["Events"].c.Id == g.eid
        )
        scheme_select: select = select([meta.tables["Events"].c.Scheme]).where(
            meta.tables["Events"].c.Id == g.eid
        )
        storage_id_select: select = select([meta.tables["Events"].c.StorageId]).where(
            meta.tables["Events"].c.Id == g.eid
        )
        start_datetime_select: select = select(
            [meta.tables["Events"].c.StartDateTime]
        ).where(meta.tables["Events"].c.Id == g.eid)
        reason_result: CursorResult = conn.execute(reason_select)
        scheme_result: CursorResult = conn.execute(scheme_select)
        storage_id_result: CursorResult = conn.execute(storage_id_select)
        start_datetime_result: CursorResult = conn.execute(start_datetime_select)

        for row in reason_result:
            reason = row[0]
        reason_result.close()
        for row in scheme_result:
            scheme = row[0]
        scheme_result.close()
        for row in storage_id_result:
            storage_id = row[0]
        storage_id_result.close()
        for row in start_datetime_result:
            start_datetime = row[0]
        start_datetime_result.close()

        if storage_id:
            storage_path_select: select = select([meta.tables["Storage"].c.Path]).where(
                meta.tables["Storage"].c.Id == storage_id
            )
            storage_path_result: CursorResult = conn.execute(storage_path_select)
            for row in storage_path_result:
                storage_path = row[0]
            storage_path_result.close()
        else:
            g.log.debug(f"{lp} no storage ID for event {g.eid}")

        if start_datetime:
            if storage_path:
                event_path = f"{storage_path}/{_rel_path(scheme, start_datetime)}"
            else:
                if storage_id:
                    g.log.error(
                        f"{lp} no storage path for StorageId {storage_id}, the StorageId could "
                        f"of been removed/deleted/disabled"
                    )
                else:
                    g.log.error(f"{lp} no StorageId for event {g.eid}!")
        else:
            g.log.debug(f"{lp} no StartDateTime for event {g.eid}")

        if event_path:
            g.event_path = g.args["event_path"] = event_path
            g.log.debug(
                f"{lp} storage path for event ID: {g.eid} has been calculated as '{event_path}'"
            )
        else:
            g.log.warning(
                f"{lp} the database could not calculate the storage path for this event!"
            )

        if reason:
            g.event_cause = g.args["reason"] = reason
            g.log.debug(f"{lp} ZoneMinder DB returned event cause as '{reason}'")
        else:
            g.log.warning(
                f"{lp} the database query did not return a 'reason' ('Cause') for this event!"
            )


def _get_secret_filename(config_filename: str) -> Optional[str]:
    """
    Returns the filename of the secret file for the given YAML config file.
    If the config file does not exist, returns None.
    """
    conf_ = Path(config_filename)
    if conf_.exists() and conf_.is_file():
        rc: dict = yaml.safe_load(conf_.read_text())
        out: dict = {}
        lp: str = "secrets_filename:"
        # Flatten the sections to get all the keys to the 'base' level
        if (
            str2bool(rc.get("SECTIONS"))
            or str2bool(rc.get("MLAPI"))
            or str2bool(rc.get("ZMES"))
        ):
            sections = rc.keys()
            g.log.debug(f"{lp} config file has 'sections' enabled, flattening...")
            for section in sections:
                if section == "monitors":
                    out[section] = rc[section]
                elif isinstance(rc[section], dict):
                    for k, v in rc[section].items():
                        if k not in out:
                            out[k] = v
        else:
            out = rc
        g.extras["flat_config"] = out
        if out.get("secrets"):
            g.log.debug(
                f"{lp} found secrets filename in config file keys: {out['secrets']}"
            )
            g.secrets_filename = out["secrets"]
            g.extras["pyzm_overrides"] = out.get("pyzm_overrides", {})
            g.log.debug(f"{lp} found pyzm_overrides: {g.extras['pyzm_overrides']}")
            return g.secrets_filename
    g.log.debug(f"{lp} no secrets filename found in config file")
    return None


def _write_cache_data(data_: dict, cache_: Path) -> NoReturn:
    """Write the data to the cache file."""
    lp: str = "zmes:write cache:"
    g.log.debug(f"{lp} attempting to write cache data to {cache_}")

    try:
        cache_.write_bytes(pkl_dumps(data_))
    except Exception as e:
        g.log.error(f"{lp} could not write cache data: {e}")
    else:
        g.log.debug(f"{lp} cache data written successfully!")


def _read_cache_data(absolute_path: Path) -> dict:
    lp: str = "zmes:read cache:"
    ret_val: dict = {}
    if not Path(absolute_path).exists():
        g.log.warning(f"{lp} no cached data found in {absolute_path}")
    else:
        if absolute_path.exists():
            g.log.info(f"{lp} loading cached data from {absolute_path}")
            try:
                ret_val = pkl_loads(absolute_path.read_bytes())
            except Exception as e:
                g.log.error(f"{lp} could not load cache data: {e}")
            else:
                g.log.debug(f"{lp} cache data loaded!")
    return ret_val


def _flush_cache(cache_dir: Union[Path, str]) -> NoReturn:
    """
    Removes the cache_dir and files.

    :param cache_dir: The directory to purge, supplied as a Path object or str.
    """
    lp: str = "zmes:purge cache:"
    if not isinstance(cache_dir, Path) and isinstance(cache_dir, str):
        cache_dir = Path(cache_dir)
    elif not isinstance(cache_dir, str) and not isinstance(cache_dir, Path):
        g.log.warning(f"{lp} cache_dir is not a string or Path object")
        return

    for file in cache_dir.glob("*"):
        if file.exists():
            g.log.debug(f"{lp} removing cache file: {file.name}")
            file.unlink()
    if Path(cache_dir).exists():
        g.log.debug(f"{lp} removing cache directory: {cache_dir}")
        Path(cache_dir).rmdir()


def _api_event_data(api_thread: Thread) -> NoReturn:
    if api_thread and api_thread.is_alive():
        start_wait = time.perf_counter()
        g.log.warning(f"{lp} API thread is still alive, waiting for it to finish")
        api_thread.join(20)
        g.log.debug(
            f"perf:{lp} waiting for the API thread took {time.perf_counter() - start_wait} seconds"
        )
    if not g.api:
        g.log.error(f"{lp} g.api object = {g.api} , EXITING...")
        g.log.log_close()
        exit(1)
    # mon_fps = get_fps_zmu(g.mid, g.api.username, g.api.password, g.api.access_token)
    # g.log.dbg(f"DBG>>> in _api_event_data, mon_fps for Monitor ID: {g.mid} = {mon_fps}")
    g.Event, g.Monitor, g.Frame, g.Monitor_new = g.api.get_all_event_data()


def _rel_path(scheme: str, dt: Optional[datetime] = None) -> str:
    ret_val: str = ""
    lp: str = "zmes:rel path:"
    if not g.mid:
        g.log.debug(
            f"{lp} waiting for monitor ID to be set, joining DB monitor ID thread"
        )
        mid_thread: Optional[Thread] = g.ThreadPool.get("mid")
        if mid_thread and mid_thread.is_alive():
            mid_thread.join(10)
        if not g.mid:
            g.log.error(f"{lp} still waiting for monitor ID to be set, exiting")
            g.log.log_close()
            exit(1)

    if scheme == "Deep":
        if dt:
            ret_val = f"{g.mid}/{dt.strftime('%y/%m/%d/%H/%M/%S')}"
        else:
            g.log.error(f"{lp} no datetime for deep scheme path!")
    elif scheme == "Medium":
        ret_val = f"{g.mid}/{dt.strftime('%Y-%m-%d')}/{g.eid}"
    elif scheme == "Shallow":
        ret_val = f"{g.mid}/{g.eid}"
    else:
        g.log.error(f"{lp} unknown scheme {scheme}")
    return ret_val


def _bg_push_url_creds(uname, pword):
    lp: str = "'push_user' auth:"
    token_: Optional[str] = None
    if uname and pword:
        login_data = {
            "user": uname,
            "pass": pword,
        }
        while not g.api:
            g.log.dbg(f"DBG!!!>>> g.api is not set, sleeping for 0.5 seconds")
            time.sleep(0.5)
        g.log.dbg(f"DEBUG>>> g.api is CREATED!")
        while not g.api.api_url:
            g.log.dbg(f"DBG!!!>>> g.api.api_url is not set, sleeping for 0.2 seconds")
            time.sleep(0.2)
        g.log.dbg(f"DBG!!!>>> g.api.api_url is now set -> {g.api.api_url = }")
        url = f"{g.api.api_url}/host/login.json"
        try:
            login_response = requests.post(url, data=login_data)
            login_response.raise_for_status()
            login_response_json = login_response.json()
        except Exception as exc:
            g.log.error(
                f"{lp} Error trying to obtain user: '{uname}' token for push "
                f"notifications, token will not be provided"
            )
            g.log.debug(f"{lp} EXCEPTION>>> {exc}")
        else:
            if version_tuple(login_response_json.get("apiversion")) >= version_tuple(
                "2.0"
            ):
                g.log.debug(
                    2,
                    f"{lp} detected ZM API ver 2.0+, grabbing JWT for configured push_user '{uname}'",
                )
                token_ = f"token={login_response_json.get('access_token')}"
            else:
                g.log.debug(
                    2,
                    f"{lp}: Token auth is not setup on your ZM host, no access token "
                    f"will be provided to push notifications",
                )
            if token_:
                g.log.debug(f"{lp} token retrieved!")
        if not token_:
            # Uses the zm_user and zm_password that ZMES uses if push_user and push_pass not set
            g.log.warning(
                f"{lp} there does not seem to be a push_user and push_pass set for viewing "
                f"events/images/push notifications, using credentials from config: {g.config_filename}"
            )
            token_ = g.api.get_auth()

    g.extras["pushover_url_creds"] = token_
    return token_


def _check_past_force(
    force_options: Union[bool, List[bool], Tuple[bool], Set[bool]]
) -> bool:
    """Checks if this is a past event and evaluated the 'force_xxx' options passed to it
    True means its not a past event or it is a past event and the force_option overrode it.
    """
    lp = "check past force:"
    ret = False
    if not g.past_event:
        ret = True
    elif g.past_event:
        ret = False
        if force_options:
            if isinstance(force_options, bool):
                ret = True
            elif isinstance(force_options, (list, tuple, set)):
                for opt in force_options:
                    if opt:
                        ret = True
                        break
    return ret


def main() -> str:
    # perf counters
    perf_start: time.perf_counter = time.perf_counter()
    perf_total_time_start_to_detect: time.perf_counter
    perf_start_after_detection: time.perf_counter
    perf_start_of_remote_detection: Optional[time.perf_counter] = None
    perf_end_of_remote_detection: Optional[time.perf_counter] = None
    perf_start_of_local_detection: Optional[time.perf_counter] = None
    perf_end_of_local_detection: Optional[time.perf_counter] = None
    perf_start_of_local_fallback_detection: Optional[time.perf_counter] = None
    perf_end_of_local_fallback_detection: Optional[time.perf_counter] = None
    # Threads
    thread_animations: Optional[Thread] = None
    thread_pushover_jpg: Optional[Thread] = None
    thread_pushover_gif: Optional[Thread] = None
    thread_mqtt: Optional[Thread] = None
    thread_mid: Optional[Thread] = None
    thread_api: Optional[Thread] = None
    thread_push_creds: Optional[Thread] = None
    thread_cause: Optional[Thread] = None
    thread_push_url_creds: Optional[Thread] = None
    thread_event_data: Optional[Thread] = None
    # images
    objdetect_jpeg_image: Optional[np.ndarray] = None
    pushover_image: Optional[Union[np.ndarray, bytes]] = None
    # vars
    final_msg: str = ""
    old_notes: str = ""
    notes_zone: str = ""
    notes_cause: str = ""
    prefix: str = ""
    pred: str = ""
    detections: list = []
    seen: list = []
    remote_sanitized: dict = {}
    ml_options: dict = {}
    stream_options: dict = {}
    matched_data: dict = {}
    all_data: dict = {}
    obj_json: dict = {}
    m: DetectSequence
    # create pushover object for error notifications

    po: Pushover
    matched_data: dict
    push_clickable_user: str = ""
    push_clickable_pass: str = ""
    bio: BytesIO
    _frame_id: str = ""
    lp: str = "zmes:init:"
    # Set the hardcoded default config
    g.DEFAULT_CONFIG = DEFAULT_CONFIG
    # Process CLI arguments as g.eid needs to be set for the SQL ORM monitor ID grabber
    g.args = parse_cli_args()
    try:
        from platform import python_version
        g.log.info(
            f"------| NEO app->Hooks: {__app_version__} - pyzm: {pyzm_version} - "
            f"ES: {get_es_version()} - OpenCV: {cv2.__version__} - Python Interpreter: {python_version()}|------"
        )
        g.log.info(
            f"{lp} setting up signal handlers for log 'rotation' and 'interrupt'"
        )
        signal.signal(signal.SIGHUP, partial(sig_log_rot, g))
        signal.signal(signal.SIGINT, partial(sig_intr, g))
    except Exception as e:
        g.log.error(
            f"{lp} error while setting up log 'rotate' and 'interrupt' signal handlers -> {e}"
        )
        raise e
    thread_cause = Thread(name="Get 'Cause'", target=_db_reason, daemon=True)
    thread_cause.start()
    g.ThreadPool["cause"] = thread_cause
    thread_mid = Thread(
        name="Get Monitor ID",
        target=_db_mid,
        daemon=True,
    )
    thread_mid.start()
    g.ThreadPool["mid"] = thread_mid
    # get g.mid so we can start logging ASAP
    thread_mid.join(10)

    g.config["pyzm_overrides"] = g.extras.get("pyzm_overrides", {})
    thread_zm_log = Thread(
        name="ZMLog",
        target=start_logs,
        kwargs={"type_": "zmes", "no_signal": True},
    )
    thread_zm_log.start()
    g.ThreadPool["log"] = thread_zm_log
    g.args["secrets"] = g.secrets_filename

    if g.args.get("shm_poll") or g.args.get("live"):
        g.args["live"] = True
    called_from = "ZMES SHM Polling" if g.args.get("shm_poll") else "Event<X>Command"
    g.log.debug(
        f"{lp} current event ({g.args['event_id']}) assumed to be called from '{called_from}'"
    )

    perf_start_conf: time.perf_counter = time.perf_counter()
    if not g.args.get("file") and not g.args.get("live"):
        g.past_event = True
        g.log.debug(f"{lp} this is a 'PAST' (debugging?) event!")
    elif g.args.get("file"):
        g.log.debug(
            f"{lp} --file INPUT so LIVE / PAST event logic untouched -> "
            f"{g.past_event = }"
        )

    # Cache
    g.cache_dir.mkdir(exist_ok=True)
    parse_config: bool = True
    current_event_data: dict = {
        "eid": g.args.get("event_id"),
        "shm": False,
    }

    if g.args.get("shm_poll"):
        current_event_data["shm"] = True

    cached_data_filename: str = "zmes_cache"
    g.cache_data = _read_cache_data(g.cache_dir.joinpath(cached_data_filename))
    cached_event_data: dict = g.cache_data.get("event_data", {})
    if cached_event_data:
        if cached_event_data.get("eid") == g.args["event_id"] and not g.past_event:
            g.log.warning(
                f"{lp} the previously processed event id is the same as this event id"
            )

            if (cached_event_data.get("shm") and not current_event_data.get("shm")) or (
                not cached_event_data.get("shm") and current_event_data.get("shm")
            ):
                g.log.warning(
                    f"{lp} It seems you have EventStartCommand and zmeventnotification.pl "
                    f"running for the same event, skipping as this event is already being "
                    f"processed"
                )

    g.log.debug(f"{lp} temp_dir: {g.cache_dir}")

    cached_hash_data = g.cache_data.get("hash_data", {})
    curr_conf_hash: str = (
        hashlib.sha256(Path(g.config_filename).read_bytes()).hexdigest() or ""
    )
    curr_secrets_hash: str = ""
    if g.secrets_filename:
        curr_secrets_hash = hashlib.sha256(
            Path(g.secrets_filename).read_bytes()
        ).hexdigest()
    _sec_msg: str = f" and secrets ({g.secrets_filename})"
    g.log.debug(
        f"{lp} hashed config ({g.config_filename}){_sec_msg if curr_secrets_hash else ''} file"
    )

    if cached_hash_data:
        stored_conf_hash = cached_hash_data.get("conf_hash")
        stored_secrets_hash = cached_hash_data.get("secrets_hash")

        if stored_conf_hash:
            if stored_conf_hash == curr_conf_hash:
                parse_config = False
            else:
                g.log.debug(
                    f"{lp} hash mismatch for config file! CURRENT: {curr_conf_hash} - "
                    f"STORED: {stored_conf_hash}"
                )
                parse_config = True

            if stored_secrets_hash and curr_secrets_hash:
                if stored_secrets_hash != curr_secrets_hash:
                    g.log.debug(
                        f"{lp} hash mismatch for secrets file! CURRENT: {curr_secrets_hash} - "
                        f"STORED: {stored_secrets_hash}"
                    )
                    parse_config = True

    if parse_config:
        g.log.debug(f"{lp} parsing config, writing temp cache data to {g.cache_dir}")
        zmes_config = ZMESConfig(
            g.args["config"], DEFAULT_CONFIG, "zmes", no_auto_parse=True
        )
        zmes_config.parse(config_hash=curr_conf_hash, secrets_hash=curr_secrets_hash)
        cached_hash_data = {
            "conf_hash": zmes_config.config_hash,
            "secrets_hash": None,
        }
        if zmes_config.secrets_hash:
            cached_hash_data["secrets_hash"] = zmes_config.secrets_hash

    else:
        g.log.debug(f"{lp} loading config and api objects from the cache")
        zmes_config = g.cache_data.get("conf_obj")
        g.api: ZMApi = g.cache_data.get("api_obj")
        g.api.set_g()
        cached_hash_data = {
            "conf_hash": curr_conf_hash,
            "secrets_hash": None,
        }
        if curr_secrets_hash:
            cached_hash_data["secrets_hash"] = curr_secrets_hash

    if not g.mid and not g.args.get("file"):
        g.log.debug(
            f"{lp} SOMETHING is very WRONG! {g.mid = } - {g.args.get('file') = } - EXITING!"
        )
        raise SyntaxError("Global monitor ID is not populated or file not supplied!")

    # Checks if there is a config built using monitor ID in the 'monitors' section
    if g.mid in zmes_config.built_per_mon_configs:
        g.log.debug(
            f"{lp} there is an overridden config built for monitor {g.mid} from the 'monitors' section"
        )
        g.config = zmes_config.built_per_mon_configs[g.mid]
    else:
        g.log.debug(
            f"{lp} there is no overridden config built for monitor '{g.mid}' from the 'monitors' "
            f"section. Using the base config"
        )
        g.config = zmes_config.config
    g.log.debug(
        f"perf:{lp} obtaining configuration took {time.perf_counter() - perf_start_conf}"
    )

    g.base_path = g.config.get("base_data_path")
    do_resize = g.config.get("resize", "no")
    if do_resize != "no":
        # Convert to an int in a try block
        try:
            do_resize = int(do_resize)
        except Exception as e:
            g.log.warning(f"{lp} error while converting resize value to an int -> {e}")
            g.config["resize"] = "no"
        else:
            g.config["resize"] = do_resize

    objdet_force = str2bool(g.config.get("force_debug"))
    if g.args.get("file"):
        g.config["wait"] = 0

    if str2bool(g.config.get("force_live")):
        g.log.warning(f"{lp} forcing LIVE logic!")
        g.past_event = False

    # g.api object is cached if possible, if not, create it
    if not g.api:
        thread_api = Thread(
            name="ZM API",
            target=create_api,
        )
        thread_api.start()
        g.ThreadPool["api_thread"] = thread_api
    push_clickable_user: Optional[str] = g.config.get("push_user")
    push_clickable_pass: Optional[str] = g.config.get("push_pass")
    thread_push_url_creds = Thread(
        name="push_url_creds",
        target=_bg_push_url_creds,
        args=(
            push_clickable_user,
            push_clickable_pass,
        ),
        daemon=True,
    )
    thread_push_url_creds.start()
    g.ThreadPool["url_creds"] = thread_push_url_creds
    thread_event_data = Thread(
        name="get event data from ZM api",
        target=_api_event_data,
        args=[thread_api],
    )
    thread_event_data.start()
    g.ThreadPool["event_data"] = thread_event_data
    pushover_request_data: dict = {
        "token": g.config.get("push_token"),
        "user": g.config.get("push_key"),
        "device": g.config.get("push_device"),
        "priority": g.config.get("push_priority"),
        "sound": g.config.get("push_sounds", {}) or {},
        "title": f"({g.eid}) {g.mon_name}->{g.event_cause}",
        "timestamp": time.time(),
    }
    g.log.dbg(f'DBG => pushover request data: {pushover_request_data}')
    if g.event_type.lower() == "end":
        pushover_request_data["title"] = f"Ended-> {pushover_request_data['title']}"
    g.pushover = po = Pushover(options=pushover_request_data)
    # see if we need to skip doing any detections, this is a more configurable option as no need to restart the ES
    # zmeventnotification.pl to update this list, objectconfig.yml is re-evaluated if it changes
    skip: Optional[Union[str, list]] = g.config.get("skip_mons")
    skip = (
        [int(x) for x in skip.strip().split(",")]
        if skip and isinstance(skip, str)
        else []
    )
    # Only skip if configured, and it's a live event, not a forced live logic event
    if g.mid in skip and g.args.get("live"):
        g.log.info(
            f"{lp} event {g.eid} from monitor: '{g.mon_name}' ID: {g.mid}"
            f" which is in the list of monitors to skip (skip_mons) -> {skip}  "
        )
        raise Exception(f"Monitor ID: {g.mid} is in the skip_mons list!")

    ml_options = g.config.get("ml_sequence", {})
    stream_options = g.config.get("stream_sequence", {})
    # Past event passed in stream options in the case of sending to a remote host
    stream_options["past_event"] = g.past_event
    stream_options["fps"] = Decimal(g.mon_fps)
    stream_options["precount"] = g.mon_pre
    stream_options["postcount"] = g.mon_post
    polygons: list = []
    import_zm_zones_ = g.config.get(
        "import_zm_zones",
        zmes_config.built_monitors.get(g.mid, {}).get("import_zm_zones"),
    )
    if str2bool(import_zm_zones_):
        g.log.info(f"{lp} importing ZM zones for monitor {g.mid}")
        polygons = import_zm_zones(polygons)
    if g.mid in zmes_config.polygons:
        # extend will add defined zones to imported zones
        polygons.extend(zmes_config.polygons[g.mid])
    stream_options["polygons"] = polygons

    perf_total_time_start_to_detect = time.perf_counter() - perf_start
    # Send to mlapi or do local
    if str2bool(g.config["ml_enable"]):
        mlapi_success: bool = False
        remote_response: dict = {}
        ml_routes: Optional[Union[list, str]] = g.config.get("ml_routes")
        if ml_routes:
            if isinstance(ml_routes, str):
                ml_routes = literal_eval(ml_routes)
            weighted_routes = sorted(ml_routes, key=lambda _route: _route["weight"])
            perf_start_of_remote_detection = time.perf_counter()
            for tries in range(len(weighted_routes)):
                tries += 1
                route = weighted_routes.pop(0)
                if remote_response:
                    break
                elif not str2bool(route.get("enabled", "yes")):
                    g.log.debug(
                        f"{lp} route #{tries} ({route['name']}) is disabled, skipping"
                    )
                    continue
                try:
                    if tries > 1:
                        g.log.debug(
                            f"{lp} switching to the next route '{route['name']}'"
                        )
                    remote_response = remote_detect(
                        stream_options=stream_options, route=route
                    )

                except requests.exceptions.HTTPError as http_ex:
                    if http_ex.response.status_code == 400:
                        if g.args.get("file"):
                            g.log.error(
                                f"There seems to be an error (400) trying to send an image from zmes to mlapi,"
                                f" looking into it. Please open an issue with sanitized logs!"
                            )
                        g.log.error(f"{lp} EXCEPTION -> {http_ex}")
                    elif http_ex.response.status_code == 500:
                        g.log.error(
                            f"There seems to be an Internal Error (500) with the mlapi host, check mlapi logs!"
                        )
                    elif http_ex.response.status_code == 422:
                        if http_ex.response.content == b'{"message": "Invalid token"}':
                            g.log.error(
                                f"Invalid JWT AUTH token (422), trying to delete and re create token..."
                            )
                            _file_name = _get_jwt_filename(route["gateway"])
                            jwt_file: Path = g.cache_dir.joinpath(f"{_file_name}")
                            if jwt_file.is_file():
                                try:
                                    jwt_file.unlink(missing_ok=True)
                                except Exception as e:
                                    g.log.error(
                                        f"{lp} error removing {jwt_file} -> {e}"
                                    )
                                else:
                                    g.log.debug(f"{lp} Removed JWT file: {jwt_file}")

                        else:
                            g.log.error(
                                f"There seems to be an Authentication Error (422) with the mlapi host, check mlapi logs!"
                            )
                    else:
                        g.log.error(
                            f"{lp} 'HTTP' ERR CODE={http_ex.response.status_code}  {http_ex.response.content=}"
                        )

                except urllib3.exceptions.NewConnectionError as urllib3_ex:
                    g.log.error(
                        f"There seems to be an error while trying to start a new connection to the mlapi host -> {urllib3_ex}"
                    )
                except requests.exceptions.ConnectionError as req_conn_ex:
                    g.log.error(
                        f"There seems to be an error while trying to start a connection to the mlapi host (is mlapi running?) "
                        f"-> {req_conn_ex.response}"
                    )
                except urllib3.exceptions.ReadTimeoutError as urllib3_ex:
                    g.log.error(
                        f"Timeout error while trying to read from the mlapi host -> {urllib3_ex}"
                    )
                    # urllib3.exceptions.ReadTimeoutError: HTTPConnectionPool(host='10.0.1.8', port=5000): Read timed out. (read timeout=300)
                except Exception as custom_push_exc:
                    g.log.debug(
                        f"DEBUG <<<INSIDE `except Exception` of mlapi remote request try block"
                    )
                    if not weighted_routes:
                        perf_end_of_remote_detection = (
                            time.perf_counter() - perf_start_of_remote_detection
                        )
                    g.log.warning(
                        f"{lp} there was an error during remote detection! -> {custom_push_exc}"
                    )
                    g.log.debug(format_exc())
                # Successful mlapi post
                else:
                    lp = "zmes:"
                    perf_end_of_remote_detection = (
                        time.perf_counter() - perf_start_of_remote_detection
                    )

                    mlapi_success = True
                    if remote_response is not None:
                        matched_data = remote_response.get("matched_data")
                        remote_sanitized: dict = copy.deepcopy(remote_response)
                        remote_sanitized["matched_data"]["image"] = (
                            "<JPEG Image>"
                            if matched_data.get("image") is not None
                            else "<No Image Returned>"
                        )
                    mon_name = f"'Monitor': {g.mon_name} ({g.mid})->'Event': "
                    g.log.debug(
                        f"perf:{lp}mlapi: {f'{mon_name}' if not g.args.get('file') else ''}{g.eid}"
                        f" mlapi detection took: {perf_end_of_remote_detection}"
                    )
                    break
        else:
            g.log.warning(f"{lp} no routes configured for remote MLAPI detection!")
        lp = "zmes:"
        if str2bool(g.config.get("ml_fallback_local")) and not mlapi_success:
            perf_start_of_local_fallback_detection = time.perf_counter()
            g.log.error(f"{lp} mlapi error, falling back to local detection")
            m = DetectSequence(options=ml_options)
            matched_data, all_data, all_frames = m.detect_stream(
                stream=g.eid,
                options=stream_options,
                in_file=True if g.args.get("file") else False,
            )
            perf_end_of_local_fallback_detection = (
                time.perf_counter() - perf_start_of_local_fallback_detection
            )

    else:  # mlapi not configured, local detection
        lp = "zmes:"
        perf_start_of_local_detection = time.perf_counter()
        sleep_wait: float = float(g.config.get("wait", 0))
        if not g.args.get("file") and sleep_wait > 0.0:
            g.log.info(
                f"{lp}local: 'wait' configured! sleeping for {g.config['wait']} seconds before running models"
            )
            time.sleep(float(g.config["wait"]))
        try:

            m = DetectSequence(options=ml_options)
            matched_data, all_data, all_frames = m.detect_stream(
                stream=g.eid,
                options=stream_options,
                in_file=True if g.args.get("file") else False,
            )
        except Exception as custom_push_exc:
            # todo: pushover error
            g.log.debug(f"{lp}local: TPU and GPU in ZM DETECT? --> {custom_push_exc}")
        else:
            perf_end_of_local_detection = (
                time.perf_counter() - perf_start_of_local_detection
            )
            g.log.debug(
                f"perf:{lp}local: detection took: {perf_end_of_local_detection}"
            )

    # This is everything after a detection has been run
    perf_start_after_detection = time.perf_counter()
    wrote_objdetect: bool = False
    send_push: bool = False
    push_auth: str = ""
    timestamp_image: dict = g.config.get("picture_timestamp", {})
    pc_user: Optional[str] = None
    pc_pass: Optional[str] = None
    if matched_data.get("frame_id") and matched_data["image"] is not None:
        if str2bool(g.config.get("push_url")):
            # Prepare user & pass for url
            pc_pass = urllib.parse.quote(g.config.get("push_pass", ""), safe="")
            pc_user = urllib.parse.quote(g.config.get("push_user"), safe="")

            push_url_opts: dict = g.config.get("push_url_opts")
            _mode = push_url_opts.get("mode", "jpeg")
            _scale = push_url_opts.get("scale", "50")
            _max_fps = push_url_opts.get("max_fps", "15")
            _buffer = push_url_opts.get("buffer", "1000")
            _replay = push_url_opts.get("replay", "single")
            po.request_data.url = (
                f"{g.api.get_portalbase()}/cgi-bin/nph-zms?mode={_mode}&scale="
                f"{_scale}&maxfps={_max_fps}&buffer={_buffer}&replay={_replay}&"
                f"monitor={g.mid}&event={g.eid}"
            )
        # Format the frame ID (if it contains s- for snapshot conversion)
        # _frame_id = grab_frame_id(matched_data.get("frame_id"))
        _frame_id = matched_data.get("frame_id")
        obj_json = {
            "frame_id": _frame_id,
            "labels": matched_data.get("labels"),
            "confidences": matched_data.get("confidences"),
            "boxes": matched_data.get("boxes"),
            "image_dimensions": matched_data.get("image_dimensions"),
        }
        # Print returned data nicely if there are any detections returned
        if matched_data.get("labels") and len(matched_data["labels"]):
            pretty_print(matched_data=matched_data, remote_sanitized=remote_sanitized)

        prefix = f"[{str(_frame_id).strip('-')}] "
        model_names: list = matched_data.get("model_names")
        for idx, l in enumerate(
            matched_data.get("labels")
        ):  # add the label, confidence and model name if configured
            if l not in seen:
                label_txt = f"{l}"
                model_txt = ""
                if len(model_names) == len(matched_data["labels"]):
                    label_txt = f"{label_txt} ({model_names[idx]})"
                    model_txt = (
                        model_names[idx]
                        if str2bool(g.config.get("show_models"))
                        else ""
                    )
                if not str2bool(g.config.get("show_percent")):
                    label_txt = f"{l}{f'({model_txt})' if model_txt != '' else ''}, "
                else:
                    label_txt = (
                        f"{l}({matched_data['confidences'][idx]:.0%}"
                        f"{f'-{model_txt}' if model_txt != '' else ''}), "
                    )
                pred = f"{pred}{label_txt}"
                seen.append(l)

        goti_token: Optional[str] = g.config.get("goti_token")
        goti_host: Optional[str] = g.config.get("goti_host")
        goti_portal = g.config.get("goti_portal", g.api.get_portalbase())
        # building the keyword that zm_event_start/end and the perl script look for -> detected:
        pred = pred.strip().rstrip(",")  # remove trailing comma
        pred_out = f"{prefix}:detected:{pred}"
        pred = f"{prefix}{pred}"
        new_notes = pred_out
        display_param_dict: Optional[dict] = None
        g.log.info(f"{lp}prediction: '{pred}'")
        jos = json.dumps(obj_json)
        g.log.debug(f"{lp}prediction:JSON: {jos}")
        if g.args.get("file") and matched_data.get("image_dimensions"):
            dimensions = matched_data["image_dimensions"]
            # image_dimensions -->{'original': [2160, 3840], 'resized': [450, 800]}]
            old_h, old_w = dimensions["original"]
            new_h, new_w = dimensions["resized"]
            old_x_factor = new_w / old_w
            old_y_factor = new_h / old_h
            g.log.debug(
                2,
                f"{lp} image was resized and needs scaling of bounding boxes "
                f"using factors of x={old_x_factor} y={old_y_factor}",
            )
            for box in matched_data["boxes"]:
                box[0] = round(box[0] / old_x_factor)
                box[1] = round(box[1] / old_y_factor)
                box[2] = round(box[2] / old_x_factor)
                box[3] = round(box[3] / old_y_factor)
        # ---------------------------------------------------------------------------------
        # create an output image that will have the red bounding boxes around detections that were filtered out
        # by confidence/zone/match pattern/etc. (if configured), a timestamp in upper left corner, random color
        # bounding box' around matched detections with their label, confidence level and the model name that
        # detected it (if configured)
        # debug image will have the red bounding boxes (if configured)
        # ---------- CREATE THE ANNOTATED IMAGE (objdetect.jpg) ----------------------------
        # show confidence percent in annotated images
        show_percent = str2bool(g.config.get("show_percent"))
        # show red bounding boxes around objects that were detected but filtered out
        errors_ = (
            matched_data["error_boxes"]
            if str2bool(g.config.get("show_filtered_detections"))
            else None
        )
        # draw polygon/zone
        draw_poly = (
            matched_data["polygons"]
            if str2bool(g.config.get("draw_poly_zone"))
            else None
        )
        objdetect_jpeg_image = draw_bbox(
            image=matched_data["image"],
            boxes=matched_data["boxes"],
            labels=matched_data["labels"],
            confidences=matched_data["confidences"],
            polygons=draw_poly,
            poly_thickness=g.config["poly_thickness"],
            poly_color=g.config["poly_color"],
            write_conf=show_percent,
            errors=errors_,
            write_model=str2bool(g.config.get("show_models")),
            models=matched_data.get("model_names"),
        )

        # this is what sends the detection back to the perl script, --SPLIT-- is what splits the data structures
        # for the Perl script to differentiate
        if not g.args.get("file"):
            g.log.dbg(f"{g.Event = } -- {g.Event.get('Notes') = }")
            if not g.past_event:
                print(f"\n{pred_out.rstrip()}--SPLIT--{jos}\n")
            if g.Event:
                old_notes: str = g.Event.get("Notes") or ""
                notes_zone = old_notes.split(":")[-1]
                # if notes_zone:
                g.log.debug(
                    f"DEBUG <>>> NOTES triggered Zone(s) {notes_zone} -- event cause {g.event_cause}"
                )
                new_notes = f"{new_notes} {g.event_cause}"
                g.log.dbg(
                    f"DBG <>>> {old_notes = } -- {new_notes = }"
                )
                # Create animations as soon as we have a frame ready to process,

            if str2bool(g.config.get("create_animation")) and (
                (not g.past_event)
                or (g.past_event and str2bool(g.config.get("force_animation")))
                or (g.past_event and objdet_force)
            ):
                g.log.debug(
                    f"{lp}animation: gathering data to start animation creation in background...",
                )
                try:
                    # how long it takes to create the animations
                    perf_animation_start = time.perf_counter()
                    opts = {
                        "fid": _frame_id,
                    }
                    thread_animations = Thread(
                        target=create_animation,
                        kwargs={
                            "image": objdetect_jpeg_image.copy(),
                            "options": opts,
                            "perf": perf_animation_start,
                        },
                    )
                except Exception as animation_exc:
                    g.log.error(
                        f"{lp}animation: CREATING THREAD err_msg-> {animation_exc}"
                    )
                else:
                    thread_animations.start()

            # Create and draw timestamp on image
            if timestamp_image and str2bool(timestamp_image.get("enabled", False)):
                objdetect_image_w, objdetect_image_h = objdetect_jpeg_image.shape[:2]
                grab_frame = int(_frame_id) - 1  # convert to index
                ts_format = timestamp_image.get("date format", "%Y-%m-%d %H:%M:%S.%f")
                try:
                    image_ts_text = (
                        f"{datetime.strptime(g.Frame[grab_frame].get('TimeStamp'), ts_format)}"
                        if g.Frame and g.Frame[grab_frame]
                        else datetime.now().strftime(ts_format)
                    )
                    if str2bool(timestamp_image.get("monitor id")):
                        image_ts_text = f"{image_ts_text} - {g.mon_name} ({g.mid})"
                except IndexError:  # frame ID converted to index isn't there? make the timestamp now()
                    image_ts_text = datetime.now().strftime(ts_format)
                ts_text_color = timestamp_image.get("text color")
                ts_bg_color = timestamp_image.get("bg color")
                ts_bg = str2bool(timestamp_image.get("background"))
                objdetect_jpeg_image = write_text(
                    frame=objdetect_jpeg_image,
                    text=image_ts_text,
                    text_color=ts_text_color,
                    x=5,
                    y=30,
                    w=objdetect_image_w,
                    h=objdetect_image_h,
                    adjust=True,
                    bg=ts_bg,
                    bg_color=ts_bg_color,
                )

            if g.api.auth_enabled:
                g.log.dbg(f"DBG push_url creds, API auth is enabled")
                if not push_auth:
                    g.log.dbg(f"DBG: push_auth is not set yet!")
                    if pc_user:
                        g.log.dbg(f"DBG: pc_user is set, using it")
                        if pc_pass:
                            g.log.dbg(f"dbg: push_user and push_pass supplied")
                            if goti_portal and goti_portal.startswith("https"):
                                g.log.dbg(f"dbg: push_user/pass HTTPS detected")
                                push_auth = f"user={pc_user}&pass={pc_pass}"
                            elif goti_portal and not goti_portal.startswith("https"):
                                g.log.warn(
                                    f"dbg: push_user/pass http detected using token -> {goti_portal = }"
                                )
                                push_auth = ""
                                if thread_push_creds := g.ThreadPool.get("url_creds"):
                                    if (
                                        thread_push_creds
                                        and thread_push_creds.is_alive()
                                    ):
                                        g.log.dbg(
                                            f"dbg: thread_push_creds is alive, joining thread..."
                                        )
                                        thread_push_creds.join(10)
                                        push_auth = f"token={g.extras.get('pushover_url_creds')}"
                                else:
                                    g.log.dbg(
                                        f"dbg: thread_push_creds is not alive, using creds from ZM API"
                                    )
                                    push_auth = g.api.get_auth()
                        else:
                            g.log.warn(
                                f"{lp} push_pass not set while push_user is set, using creds from ZM API"
                            )
                            # need password with username!
                            push_auth = g.api.get_auth()
                    else:
                        g.log.dbg(f"dbg: push_url NO USER set, using creds from ZM API")
                        push_auth = g.api.get_auth()
                    po.request_data.url = f"{po.request_data.url}&{push_auth}"
                    po.request_data.url_title = "View event in browser"

            if (str2bool(g.config.get("push_enable"))) and (
                _check_past_force([str2bool(g.config.get("force_pushover"))])
            ):
                time_since_last_push = po.pickle("load")
                g.log.debug(
                    f"DEBUG<>>> Pushover is enabled, checking emergency: "
                    f"{_check_past_force([str2bool(g.config.get('force_pushover_emerg'))])}"
                )
                if str2bool(g.config.get("push_emergency")) and (
                    _check_past_force([str2bool(g.config.get("force_pushover_emerg"))])
                ):
                    send_push = do_po_emerg(matched_data["labels"], po)
                else:
                    g.log.dbg(f"DBG<>>> EMERGENCY alerts are not enabled for pushover")
                if not send_push:
                    ha_verified = verify_items(
                        g.config, {"hass_enable", "hass_server", "hass_token"}
                    )
                    g.log.dbg(
                        f"DBG<>>> CHECKING HASS stuff -> enabled={g.config.get('hass_enable')} verified={ha_verified}"
                    )
                    if ha_verified and str2bool(g.config.get("hass_enable")):
                        send_push = do_hass(time_since_last_push)
                    else:
                        if g.config.get("push_cooldown"):
                            g.log.debug(
                                f"{lp} push_cooldown found -> {g.config.get('push_cooldown')}, EVALUATING"
                            )
                            try:
                                cooldown = float(g.config.get("push_cooldown"))
                            except TypeError or ValueError as ex:
                                g.log.error(
                                    f"{lp} 'push_cooldown' malformed, sending push..."
                                )
                                send_push = True
                            else:
                                if time_since_last_push:
                                    differ = (
                                        datetime.now() - time_since_last_push
                                    ).total_seconds()
                                    if differ < cooldown:
                                        g.log.debug(
                                            f"{lp} COOLDOWN elapsed-> {differ} / {cooldown} "
                                            f"skipping notification..."
                                        )
                                        send_push = False
                                    else:
                                        g.log.debug(
                                            f"{lp} COOLDOWN elapsed-> {differ} / {cooldown} "
                                            f"sending notification..."
                                        )
                                        send_push = True
                                    del cooldown
                                else:
                                    g.log.debug(
                                        f"{lp} no previous Pushover notification data found, sending notification..."
                                    )
                                    send_push = True
                        else:
                            g.log.debug(
                                f"{lp} push_cooldown not found, sending push..."
                            )
                            send_push = True
                g.log.dbg(f"DEBUG <>>> after cooldown eval {send_push = }")
                # Use user (& pass) if a push_user or push_pass is configured
                # Use token if API 2.0+ else user and pass
                # SHOULD BE USING HTTPS for user&pass !!!
                if g.api.auth_enabled:
                    if not push_auth:
                        if pc_user:
                            if pc_pass:
                                g.log.dbg(f"dbg: push_user and push_pass supplied")
                                if goti_portal and goti_portal.startswith("https"):
                                    g.log.dbg(f"dbg: push_user/pass HTTPS detected")
                                    push_auth = f"user={pc_user}&pass={pc_pass}"
                                elif goti_portal and not goti_portal.startswith(
                                    "https"
                                ):
                                    g.log.warn(
                                        f"dbg: push_user/pass http detected using token -> {goti_portal = }"
                                    )
                                    push_auth = ""
                                    if thread_push_creds := g.ThreadPool.get(
                                        "url_creds"
                                    ):
                                        if (
                                            thread_push_creds
                                            and thread_push_creds.is_alive()
                                        ):
                                            g.log.dbg(
                                                f"dbg: thread_push_creds is alive, joining thread..."
                                            )
                                            thread_push_creds.join(10)
                                            push_auth = f"token={g.extras.get('pushover_url_creds')}"
                                    else:
                                        g.log.dbg(
                                            f"dbg: thread_push_creds is not alive, using creds from ZM API"
                                        )
                                        push_auth = g.api.get_auth()
                            else:
                                g.log.warn(
                                    f"{lp} push_pass not set while push_user is set, using creds from ZM API"
                                )
                                # need password with username!
                                push_auth = g.api.get_auth()
                        else:
                            g.log.dbg(
                                f"dbg: push_url NO USER set, using creds from ZM API"
                            )
                            push_auth = g.api.get_auth()
                        po.request_data.url = f"{po.request_data.url}&{push_auth}"
                        po.request_data.url_title = "View event in browser"
                # FIXME: create a threaded function that handles JPEG and GIF
                # SEND JPEG
                if send_push:
                    pushover_image = objdetect_jpeg_image.copy()
                    is_succ, pushover_image = cv2.imencode(".jpg", pushover_image)
                    if not is_succ:
                        g.log.warning(
                            f"{lp} cv2 failed to encode matching frame to an image"
                        )
                        raise Exception(
                            "cv2 failed to encode matching frame to an image"
                        )
                    bio = BytesIO(pushover_image)

                    old_device = po.request_data.device
                    po.request_data.message = f"{pred_out.strip()}"
                    po.request_data.device = (
                        g.config.get("push_debug_device")
                        if g.past_event
                        else old_device
                    )

                    display_param_dict = dict(po.request_data.__dict__)
                    old_key = po.request_data.user
                    old_token = po.request_data.token

                    if g.config.get("push_jpg"):
                        po.request_data.token = g.config.get("push_jpg")
                        display_param_dict["token"] = "<push_jpg app token>"
                    else:
                        po.request_data.token = old_token
                        display_param_dict["token"] = "<default app token>"
                    if g.config.get("push_jpg_key"):
                        po.request_data.user = g.config.get("push_jpg_key")
                        display_param_dict["user"] = "<push_jpg_key user/group key>"
                    else:
                        po.request_data.user = old_key
                        display_param_dict["user"] = "<default user/group key>"

                    display_auth = (
                        f"user={g.config['sanitize_str']}&pass={g.config['sanitize_str']}"
                        if push_clickable_user and push_clickable_pass
                        else f"{'<api 2.0+ auth token>' if not g.config.get('basic_user') else '<basic creds>'}"
                    )
                    display_url = (
                        f"{g.config.get('portal') if not str2bool(g.config.get('sanitize_logs')) else '{}'.format(g.config['sanitize_str'])}"
                        f"/cgi-bin/nph-zms?mode=jpeg&scale=50&maxfps=15&buffer=1000&replay=single&monitor="
                        f"{g.mid}&event={g.eid}&{display_auth}"
                    )
                    display_param_dict["url"] = display_url

                    try:
                        # do custom sound
                        po.parse_sounds(matched_data["labels"])
                    except Exception as exc:
                        g.log.err(f"{lp} failed to parse sounds: {exc}")
                    else:
                        g.log.dbg(f"PROPERLY parsed sounds for JPEG pushover?")
                        display_param_dict["sounds"] = po.request_data.sound

                    po.optionals.attachment = (
                        "objdetect.jpg",
                        bio.getbuffer(),
                        "image/jpeg",
                    )
                    g.log.debug(
                        f"{lp}pushover:JPG: data={display_param_dict} files='<objdetect.jpg>'"
                    )
                    po.optionals.cache_write = (
                        False
                        if str2bool(g.config.get("create_animation")) or g.past_event
                        else True
                    )
                    thread_pushover_jpg = Thread(name="Pushover JPEG", target=po.send)
                    thread_pushover_jpg.start()
                    po.request_data.token = old_token
                    po.request_data.device = old_device
                    po.request_data.user = old_key

                    # GIF
                    if str2bool(g.config.get("create_animation")):
                        g.log.dbg(f"DBG<>>> Pushover GIF logic running")
                        po.request_data.device = (
                            g.config.get("push_debug_device")
                            if g.past_event
                            else po.request_data.device
                        )
                        po.request_data.message = f"{pred_out.strip()}"
                        display_param_dict = po.request_data.dict()
                        if g.config.get("push_gif"):
                            po.request_data.token = g.config.get("push_gif")
                            display_param_dict["token"] = "<push_gif token>"
                        else:
                            po.request_data.token = g.config.get("push_token")
                            display_param_dict["token"] = "<push_token token>"
                        if g.config.get("push_gif_key"):
                            po.request_data.user = g.config.get("push_gif_key")
                            display_param_dict["user"] = "<push_gif_key user key>"
                        else:
                            po.request_data.user = g.config.get("push_key")
                            display_param_dict["user"] = "<push_key user key>"
                        try:
                            # do custom sound
                            po.parse_sounds(matched_data.get("labels"))
                        except Exception as exc:
                            g.log.err(f"{lp} failed to parse sounds: {exc}")
                        else:
                            g.log.dbg(f"PROPERLY parsed sounds for GIF pushover?")
                            display_param_dict["sounds"] = po.request_data.sound

                        # -----------------------------------------------------------
                        display_auth = (
                            f"user={g.config['sanitize_str']}&pass={g.config['sanitize_str']}"
                            if push_clickable_user and push_clickable_pass
                            else f"{'<api 2.0+ auth token>' if not g.config.get('basic_user') else '<basic credentials>'}"
                        )
                        display_url = (
                            f"{g.config.get('portal') if not str2bool(g.config.get('sanitize_logs')) else '{}'.format(g.config['sanitize_str'])}"
                            f"/cgi-bin/nph-zms?mode=jpeg&scale=50&maxfps=15&buffer=1000&replay=single&"
                            f"monitor={g.mid}&event={g.eid}&{display_auth}"
                        )
                        display_param_dict["url"] = display_url
                        if thread_animations and thread_animations.is_alive():
                            g.log.warning(
                                f"{lp} waiting for animation thread to finish... (timeout: 10)"
                            )
                            thread_animations.join(10)
                        po.optionals.attachment = (
                            f"objdetect-{g.mid}.gif",
                            open(f"{g.event_path}/objdetect.gif", "rb"),
                            "image/jpeg",
                        )

                        g.log.debug(
                            f"{lp}pushover:gif: data={display_param_dict} files=<'objdetect-{g.mid}.gif'>"
                        )
                        po.optionals.cache_write = False if g.past_event else True
                        thread_pushover_gif = Thread(
                            name="Pushover GIF", target=po.send
                        )
                        thread_pushover_gif.start()

            elif g.past_event and (
                str2bool(g.config.get("push_enable"))
                and not str2bool(g.config.get("force_pushover"))
            ):
                g.log.debug(
                    f"{lp}pushover: this is a past event and 'force_pushover'"
                    f"={str2bool(g.config.get('force_pushover'))}, skipping pushover notifications..."
                )
            else:
                g.log.dbg(
                    f"this is the ELSE: statement for pushover notifications - {g.past_event = } "
                    f"{str2bool(g.config.get('push_enable')) = } -- "
                    f"{str2bool(g.config.get('force_pushover')) = }"
                )

            # TODO: create websocket server to get tokens from zmninja clients if zmeventnotification.pl
            if str2bool(g.config.get("fcm_push")) and (
                _check_past_force(str2bool(g.config.get("force_fcm")))
            ):
                g.log.dbg(f"DBG<>>> FCM is enabled")
                from pyzm.helpers.pyzm_utils import FCMsend

                try:
                    fcm_tokens_file = g.config.get("fcm_tokens")
                    fcm_obj = FCMsend(pred, tokens_file=fcm_tokens_file)
                except Exception as fcm_exc:
                    g.log.error(
                        f"{lp} ERROR while trying to send an FCM to zmNinja! -> {fcm_exc}"
                    )

            if g.event_path:
                skip_write: bool = False
                object_file = Path(f"{g.event_path}/objects.json")
                if object_file.is_file():
                    try:
                        eval_me = json.load(object_file.open())
                    except json.JSONDecodeError:
                        # If there's an issue, use a value that will not match
                        eval_me = {"test": 123, 321: "test", 420: 69}
                    if eval_me == obj_json:
                        # the previously saved detection is the same as the current one
                        skip_write = True

                if not skip_write or (skip_write and objdet_force):
                    g.log.debug(
                        f"{lp} writing objects.json and objdetect.jpg to '{g.event_path}'"
                    )
                    try:
                        cv2.imwrite(
                            f"{g.event_path}/objdetect.jpg", objdetect_jpeg_image
                        )
                        json.dump(obj_json, object_file.open("w"))
                    except Exception as custom_push_exc:
                        g.log.error(
                            f"{lp} Error trying to save objects.json or objdetect.jpg err_msg-> \n{custom_push_exc}\n"
                        )
                    else:
                        wrote_objdetect = True
                else:
                    g.log.debug(
                        f"{lp} not writing objdetect.jpg or objects.json as monitor {g.mid}->'{g.mon_name}' "
                        f"event: {g.eid} has a previous detection and it matches the current one"
                    )

            # Gotify
            if str2bool(g.config.get("goti_enable")):
                try:
                    goti_url_opts: dict = g.config.get("goti_url_opts", {})
                    _mode = goti_url_opts.get("mode", "jpeg")
                    _scale = goti_url_opts.get("scale", "50")
                    _max_fps = goti_url_opts.get("max_fps", "15")
                    _buffer = goti_url_opts.get("buffer", "1000")
                    _replay = goti_url_opts.get("replay", "single")

                    goti_event_url = (
                        f"{goti_portal}/cgi-bin/nph-zms?mode={_mode}&scale="
                        f"{_scale}&maxfps={_max_fps}&buffer={_buffer}&replay={_replay}&"
                        f"monitor={g.mid}&event={g.eid}"
                    )

                    goti_event_url = f"{goti_event_url}&{push_auth}"
                    # goti_image_url: str = f"{g.api.portal_url}/index.php?view=image&eid={g.eid}&fid=objdetect&{push_zm_tkn}"
                    image_auth = push_auth.replace("user=", "username=").replace(
                        "pass=", "password="
                    )
                    goti_image_url: str = (
                        f"{goti_portal}/index.php?view=image&eid={g.eid}&fid="
                        f"objdetect&{image_auth}"
                    )
                    g.log.dbg(f"{goti_image_url = } -- | --- {goti_event_url = }")
                    test_ = "https://placekitten.com/400/200"
                    goti_msg: str = (
                        f"{pred_out}\n\n[View event in browser]({goti_event_url}) "
                        f"![objectdetect.jpeg]({goti_image_url})"
                    )
                    # \n![Embedded event video for gotify web app]({goti_event_url})

                    data = {
                        "title": f"({g.eid}) {g.mon_name}->{g.event_cause}",
                        "message": goti_msg,
                        "priority": 100,
                        "extras": {
                            "client::display": {"contentType": "text/markdown"},
                            "client::notification": {
                                # "bigImageUrl": f"{goti_image_url}",
                                "bigImageUrl": f"{goti_image_url}",
                                # "click": {
                                #     "url": f"{goti_event_url}",
                                # },
                            },
                        },
                    }
                    resp = requests.post(
                        f"{goti_host}/message?token={goti_token}", json=data
                    )
                except Exception as custom_push_exc:
                    g.log.error(
                        f"{lp} ERROR while executing Gotify notification "
                        f"{g.config.get('custom_push_script')}"
                    )
                    g.log.debug(f"{lp} EXCEPTION>>> {custom_push_exc}")
                else:
                    # g.logger.debug(f"{lp} response from gotify -> {resp.status_code=} - {resp.text = }")
                    if resp and resp.status_code == 200:
                        g.log.debug(f"{lp} Gotify returned SUCCESS")
                    elif resp and resp.status_code != 200:
                        g.log.debug(
                            f"{lp} Gotify FAILURE STATUS_CODE: {resp.status_code} -> {resp.json()}"
                        )

            # Custom shell script for notifications
            if (
                str2bool(g.config.get("custom_push"))
                and Path(g.config.get("custom_push_script")).is_file()
            ):
                try:
                    custom_script_output = subprocess.check_output(
                        [
                            which("bash"),
                            g.config["custom_push_script"],
                            str(g.eid).strip("'").strip('"'),
                            repr(g.mid),
                            g.mon_name,
                            new_notes,
                            g.event_type,
                            f"{push_auth}",
                            f"{g.api.portal_url}",
                            g.event_path,
                        ]
                    ).decode("ascii")
                except Exception as custom_push_exc:
                    g.log.error(
                        f"{lp} ERROR while executing the custom push script "
                        f"{g.config['custom_push_script']} -> \n{custom_push_exc}"
                    )
                else:
                    # g.logger.debug(f"{lp} custom shell script returned -> {custom_success}")
                    if str(custom_script_output).strip() == "0":
                        g.log.debug(f"{lp} custom push script returned SUCCESS")
                    else:
                        g.log.debug(f"{lp} custom push script returned FAILURE")

            if str2bool(g.config.get("save_image_train")):
                train_dir = g.config.get(
                    "save_image_train_dir", f"{g.config.get('base_data_path')}/images"
                )
                if Path(train_dir).exists() and Path(train_dir).is_dir():
                    filename_training = (
                        f"{train_dir}/{g.eid}-training-frame-{_frame_id}.jpg"
                    )
                    filename_train_obj_det = (
                        f"{train_dir}/{g.eid}-compare-frame-{_frame_id}.jpg"
                    )
                    write_compare = False
                    train_file = Path(filename_training)
                    if not train_file.exists():
                        g.log.debug(
                            2,
                            f"{lp} saving ML model training and compare images to: '{train_dir}'",
                        )
                        write_compare = True
                    elif train_file.exists() and objdet_force:
                        g.log.debug(
                            2,
                            f"{lp} saving ML model training and compare images to: '{train_dir}'",
                        )
                        write_compare = True

                    if write_compare:
                        try:
                            cv2.imwrite(filename_training, matched_data["image"])
                            cv2.imwrite(filename_train_obj_det, objdetect_jpeg_image)
                        except Exception as custom_push_exc:
                            g.log.error(
                                f"{lp} writing {filename_training} and {filename_train_obj_det} "
                                f"\nerr_msg -> {custom_push_exc}"
                            )
                    else:
                        g.log.debug(
                            2,
                            f"{lp} {filename_training}"
                            f"{' and {}'.format(filename_train_obj_det) if Path(filename_train_obj_det).exists() else ''} "
                            f" already exists, skipping writing....",
                        )
                else:
                    g.log.error(
                        f"{lp}training images: the directory '{train_dir}' does not exist! "
                        f"can't save the model training and compare images! Please re-configure..."
                    )

            # ---------------------------------------------------------------
            # if we didn't write a new object detect image then there's no point doing all this, UNLESS we force it.
            # ---------------------------------------------------------------
            if wrote_objdetect or (not wrote_objdetect and objdet_force):
                g.log.dbg(
                    f"DEBUG: WROTE object.json to file. {g.args.get('notes') = } -- {str2bool(g.config.get('write_notes')) = }"
                )
                # only update notes if it's a past event or --notes was passed
                if old_notes is not None and (
                    g.past_event
                    or (g.args.get("notes") or str2bool(g.config.get("write_notes")))
                ):
                    if old_notes.find("detected:") == -1 and new_notes != old_notes:
                        try:
                            events_url = f"{g.api.api_url}/events/{g.eid}.json"
                            g.api.make_request(
                                url=events_url,
                                payload={"Event[Notes]": new_notes},
                                type_action="put",
                                # quiet=True,
                            )
                        except Exception as custom_push_exc:
                            g.log.error(
                                f"{lp} error during notes update API put request-> {str(custom_push_exc)}"
                            )
                        else:
                            g.log.debug(
                                f"{lp} replaced old note -> '{old_notes}' with new note -> '{new_notes}'",
                            )
                    elif old_notes.find("detected:") > -1 and new_notes != old_notes:
                        g.log.debug(
                            f"{lp} old note -> '{old_notes}' already contains 'detected:' so not updating notes..."
                        )
                    elif new_notes == old_notes:
                        g.log.debug(
                            f"{lp} {'PAST EVENT ->' if g.past_event else ''} new notes are the same as old notes"
                            f" -> {new_notes}"
                        )

                # TODO: split up like pushover to send jpg first then gif, so its fast
                #  also add ability to pass an image instead of it just looking for objdetect.jpg or .gif on disk
                if str2bool(g.config.get("mqtt_enable")) and (
                    not g.past_event
                    or (g.past_event and str2bool(g.config.get("mqtt_force")))
                ):
                    thread_mqtt = Thread(
                        target=do_mqtt,
                        args=[
                            g.event_type,
                            pred,
                            pred_out,
                            notes_zone,
                            matched_data,
                            objdetect_jpeg_image,
                        ],
                    )
                    thread_mqtt.start()

    elif matched_data.get("image") is None:
        g.log.debug(f"{lp} no predictions returned from detections")

    detection_time: Optional[time.perf_counter] = None
    if perf_end_of_remote_detection and perf_end_of_local_fallback_detection:
        detection_time = perf_end_of_local_fallback_detection
    elif perf_end_of_remote_detection and not perf_end_of_local_fallback_detection:
        detection_time = perf_end_of_remote_detection
    elif perf_end_of_local_detection:
        detection_time = perf_end_of_local_detection
    fid_str_ = "-->'Frame ID':"
    fid_evtype = "'PAST' event" if g.past_event else "'LIVE' event"
    _mon_name = f"'Monitor': {g.mon_name} ({g.mid})->'Event': "
    final_msg = "perf:{lp}FINAL: {mid}{s}{f_id} [{match}] {tot}{before}{det}{ani_gif}{extras}".format(
        lp=lp,
        match="{}".format(fid_evtype if not g.args.get("file") else "INPUT FILE"),
        mid="{}".format(_mon_name) if not g.args.get("file") else "",
        s=g.eid,
        f_id="{}{}".format(
            fid_str_ if _frame_id and not g.args.get("file") else "",
            f'{_frame_id if _frame_id and not g.args.get("file") else ""}',
        ),
        tot=f"[total:{time.perf_counter() - perf_start}] " if perf_start else "",
        before=f"[pre detection:{perf_total_time_start_to_detect}]"
        if perf_total_time_start_to_detect
        else "",
        det=f"[detection:{detection_time}] " if detection_time else "",
        ani_gif="{}".format(
            "[processing {}".format(
                f"image/animation:{g.animation_seconds}] "
                if str2bool(g.config.get("create_animation"))
                else f"image:{g.animation_seconds}] "
            )
        )
        if g.animation_seconds
        else "",
        extras=f"[after detection: {time.perf_counter() - perf_start_after_detection}] "
        if perf_start_after_detection
        else "",
    )

    if thread_pushover_jpg and thread_pushover_jpg.is_alive():
        g.log.debug(f"{lp} waiting for the JPEG pushover thread to finish")
        thread_pushover_jpg.join(10)
    if thread_pushover_gif and thread_pushover_gif.is_alive():
        g.log.debug(f"{lp} waiting for the GIF pushover thread to finish")
        thread_pushover_gif.join(10)
    if thread_animations and thread_animations.is_alive():
        g.log.debug(f"{lp} waiting for the Animation Creation thread to finish")
        thread_animations.join(10)
    if thread_mqtt and thread_mqtt.is_alive():
        g.log.debug(f"{lp} waiting for the MQTT thread to finish")
        thread_mqtt.join(10)
    if thread_api and thread_api.is_alive():
        g.log.debug(f"{lp} waiting for the API thread to finish")
        thread_api.join(10)
    if thread_push_creds and thread_push_creds.is_alive():
        g.log.debug(f"{lp} waiting for the push credentials thread to finish")
        thread_push_creds.join(10)
    if thread_cause and thread_cause.is_alive():
        g.log.debug(f"{lp} waiting for the cause thread to finish")
        thread_cause.join(10)
    if thread_push_url_creds and thread_push_url_creds.is_alive():
        g.log.debug(f"{lp} waiting for the push URL credentials thread to finish")
        thread_push_url_creds.join(10)
    if thread_event_data and thread_event_data.is_alive():
        g.log.debug(f"{lp} waiting for the event data thread to finish")
        thread_event_data.join(10)

    # write to cache
    g.cache_data["conf_obj"] = zmes_config
    g.cache_data["api_obj"] = g.api
    g.cache_data["hash_data"] = cached_hash_data
    g.cache_data["event_data"] = current_event_data
    _write_cache_data(g.cache_data, g.cache_dir / cached_data_filename)
    return final_msg


if __name__ == "__main__":
    g = GlobalConfig()
    g.log = LogBuffer()
    _db_create()
    try:
        output_message: str = main()
    except Exception as e:
        g.log.error(f"zmes: err_msg->{e}")
        g.log.error(f"zmes: traceback: {format_exc()}")
    else:
        g.log.debug(output_message) if output_message else None
    finally:
        g.log.log_close()
# WHAT THE FUCK
