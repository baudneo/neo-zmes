#!/usr/bin/python3
import argparse
from typing import Optional
import pyzm.ml.face_train_dlib as train
from pyzm.helpers.pyzm_utils import LogBuffer
from pyzm.helpers.pyzm_utils import start_logs
from pyzm.helpers.GlobalConfig import GlobalConfig
from pyzm.interface import ZMESConfig, ZMES_DEFAULT_CONFIG as DEFAULT_CONFIG


if __name__ == "__main__":
    g: GlobalConfig = GlobalConfig()
    g.log = LogBuffer()
    args: dict
    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--config", default="/etc/zm/objectconfig.yml", help="config file with path")
    ap.add_argument("-s", "--size", type=int, help="resize amount (if you run out of memory)")
    ap.add_argument("-d", "--debug", help="enables debug with console ouput", action="store_true")
    ap.add_argument("-bd", "--baredebug", help="enables debug without console output", action="store_true")
    args, u = ap.parse_known_args()
    args = vars(args)
    args["from_face_train"] = True
    zmes: ZMESConfig = ZMESConfig(args['config'], DEFAULT_CONFIG, "zmes")
    g.config = zmes.config
    # start the logger (you can Thread this if you want)
    start_logs(type_="zmes")
    train.FaceTrain().train(size=args.get("size"))
