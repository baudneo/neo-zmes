#! /usr/bin/python3
__doc__="""
A script that will scan ZM SHM looking for raw images. Currently the frames are misaligned.
The offsets need to be calculated properly. 

------------------------------------------------------

Need OpenCV, numpy, sqlalchemy, python-dotenv. Used with CLI arguments
Pass --help for help.
"""
import mmap
import sys
import os
import struct
from ctypes import c_long, c_int64, Structure

from argparse import ArgumentParser
from collections import namedtuple
from typing import Optional, IO, Union
import dotenv

from sqlalchemy import MetaData
from sqlalchemy.engine import CursorResult
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.future import Engine, create_engine, Connection, select


__version__ = "0.0.1a"
VERSION = __version__

if sys.platform.startswith("linux"):
    time_t = c_long
elif sys.platform.startswith("netbsd"):
    # NetBSD uses function renaming for ABI versioning.  While the proper
    # way to get the appropriate version is of course "#include <time.h>",
    # it is difficult with ctypes.  The following is appropriate for
    # recent versions of NetBSD, including NetBSD-6.
    time_t = c_int64
elif sys.platform.startswith("freebsd"):
    time_t = c_int64
else:
    raise Exception
class TimeVal(Structure):
    _fields_ = [
        ('tv_sec', time_t),
        ('tv_nsec', c_long),
    ]


IMAGE_BUFFER_COUNT: int = 5
MID: Union[str, int] = "1"
WIDTH: int = 0
HEIGHT: int = 0
COLORS: int = 4
DB_ENGINE: Optional[Engine] = None
ENV: str = ".env"
# This will compensate for 32/64 bit
TIMEVAL_SIZE: int = TimeVal.tv_sec.size
# TIMEVAL_SIZE = 8
DB_USER: str = 'zmuser'
DB_PASS: str = 'zmpass'
DB_HOST: str = 'localhost'
DB_NAME: str = 'zm'


def _db_create() -> Engine:
    lp: str = "ZM-DB:"
    db_config = {
        "dbuser": f"{DB_USER}",
        "dbpassword": f"{DB_PASS}",
        "dbhost": f"{DB_HOST}",
        "dbname": f"{DB_NAME}",
        "driver": "mysql+mysqlconnector",
    }
    connection_str = (
        f"{db_config['driver']}://{db_config['dbuser']}"
        f":{db_config['dbpassword']}@{db_config['dbhost']}"
        f"/{db_config['dbname']}"
    )

    try:
        engine: Optional[Engine] = create_engine(connection_str, pool_recycle=3600)
        meta: MetaData = MetaData(engine)
        # New reflection method, only reflect the Events and Monitors tables
        meta.reflect(only=["Monitors"])
        conn: Optional[Connection] = engine.connect()
    except SQLAlchemyError as e:
        conn = None
        engine = None
        print(f"DB configs - {connection_str}")
        print(f"Could not connect to DB, message was: {e}")
    else:

        # Get Monitor 'ImageBufferCount'
        buffer_select: select = select(meta.tables["Monitors"].c.ImageBufferCount).where(meta.tables["Monitors"].c.Id == MID)
        width_select: select = select(meta.tables["Monitors"].c.Width).where(meta.tables["Monitors"].c.Id == MID)
        height_select: select = select(meta.tables["Monitors"].c.Height).where(meta.tables["Monitors"].c.Id == MID)
        colours_select: select = select(meta.tables["Monitors"].c.Colours).where(meta.tables["Monitors"].c.Id == MID)

        buffer_result: CursorResult = conn.execute(buffer_select)
        global IMAGE_BUFFER_COUNT, WIDTH, HEIGHT, COLORS
        for mon_row in buffer_result:
            IMAGE_BUFFER_COUNT = mon_row[0]
        buffer_result.close()

        width_result: CursorResult = conn.execute(width_select)
        for mon_row in width_result:
            WIDTH = mon_row[0]
        width_result.close()
        # height
        height_result: CursorResult = conn.execute(height_select)
        for mon_row in height_result:
            HEIGHT = mon_row[0]
        height_result.close()
        # colours
        colours_result: CursorResult = conn.execute(colours_select)
        for mon_row in colours_result:
            COLORS = mon_row[0]
        colours_result.close()

        if IMAGE_BUFFER_COUNT:
            print(f"Got ImageBufferCount for monitor {MID} -=> {IMAGE_BUFFER_COUNT = }")
        else:
            print(
                f"{lp} the database query did not return ImageBufferCount for monitor {MID}"
            )
        if WIDTH:
            print(f"Got Width for monitor {MID} -=> {WIDTH = }")
        else:
            print(
                f"{lp} the database query did not return Width for monitor {MID}"
            )
        if HEIGHT:
            print(f"Got Height for monitor {MID} -=> {HEIGHT = }")
        else:
            print(
                f"{lp} the database query did not return Height for monitor {MID}"
            )
        if COLORS:
            print(f"Got Colours for monitor {MID} -=> {COLORS = }")
        else:
            print(
                f"{lp} the database query did not return Colours for monitor {MID}"
            )
        conn.close()
        return engine




class ZMMemory:
    def __init__(
        self, path: Optional[str] = None, mid: Optional[Union[int, str]] = None
    ):
        global MID
        if mid:
            MID = mid
        if path is None:
            path = f"/dev/shm"

        self.alarm_state_stages = {
            "STATE_IDLE": 0,
            "STATE_PREALARM": 1,
            "STATE_ALARM": 2,
            "STATE_ALERT": 3,
            "STATE_TAPE": 4,
            "ACTION_GET": 5,
            "ACTION_SET": 6,
            "ACTION_RELOAD": 7,
            "ACTION_SUSPEND": 8,
            "ACTION_RESUME": 9,
            "TRIGGER_CANCEL": 10,
            "TRIGGER_ON": 11,
            "TRIGGER_OFF": 12,
        }
        self.fhandle: Optional[IO] = None
        self.mhandle: Optional[mmap.mmap] = None

        self.fname = f"{path}/zm.mmap.{mid}"
        self.reload()

    def reload(self):
        """Reloads monitor information. Call after you get
        an invalid memory report

        Raises:
            ValueError: if no monitor is provided
        """
        # close file handler
        self.close()
        # open file handler in read binary mode
        self.fhandle = open(self.fname, "r+b")
        # geta rough size of the memory consumed by object (doesn't follow links or weak ref)
        sz = os.path.getsize(self.fname)
        if not sz:
            raise ValueError(f"Invalid size: {sz} of {self.fname}")

        self.mhandle = mmap.mmap(self.fhandle.fileno(), 0, access=mmap.ACCESS_READ)
        self.sd = None
        self.td = None
        self._read()

    def is_valid(self):
        """True if the memory handle is valid

        Returns:
            bool: True if memory handle is valid
        """
        try:
            d = self._read()
            return not d["shared_data"]["size"] == 0
        except Exception as e:
            print(f"ERROR!!!! =-> Memory: {e}")
            return False

    def _read(self):
        self.mhandle.seek(0)  # goto beginning of file
        SharedData = namedtuple(
            "SharedData",
            "size last_write_index last_read_index state capture_fps analysis_fps last_event_id action brightness "
            "hue colour contrast alarm_x alarm_y valid capturing analysing recording signal format imagesize "
            "last_frame_score audio_frequency audio_channels startup_time zmc_heartbeat_time last_write_time "
            "last_read_time last_viewed_time control_state alarm_cause video_fifo_path audio_fifo_path",
        )

        # old_shared_data = r"IIIIQIiiiiii????IIQQQ256s256s"
        struct_shared_data = r"IiiIddQIiiiiiiccccccIIIIqqqqq256s256s64s64s"  # July 2022
        shared_data_bytes = 776  # bytes in SharedData - July 2022

        TriggerData = namedtuple(
            "TriggerData",
            "size trigger_state trigger_score padding trigger_cause trigger_text trigger_showtext",
        )
        struct_trigger_data = r"IIII32s256s256s"
        trigger_data_bytes = 560  # bytes in TriggerData

        VideoStoreData = namedtuple(
            "VideoStoreData",
            "size padding current_event event_file recording",
        )
        struct_video_store = r"IIQ4096sq"
        video_store_bytes = 4120  # bytes in VideoStoreData - July 2022

        TimeStampData = namedtuple(
            "TimeStampData",
            "time_t",
        )
        struct_time_stamp = r"q"
        time_stamp_bytes = TIMEVAL_SIZE  # bytes in TimeStampData

        s = SharedData._make(
            struct.unpack(struct_shared_data, self.mhandle.read(shared_data_bytes))
        )
        t = TriggerData._make(
            struct.unpack(struct_trigger_data, self.mhandle.read(trigger_data_bytes))
        )
        v = VideoStoreData._make(
            struct.unpack(struct_video_store, self.mhandle.read(video_store_bytes))
        )

        ts = TimeStampData._make(
            struct.unpack(struct_time_stamp, self.mhandle.read(time_stamp_bytes))
        )
        written_images = s.last_write_index + 1

        self.images_offset = self.mhandle.tell()
        print(f"{written_images = } - {self.images_offset = }")
        print(f"\nSharedData = {s}\n")
        # grab available images? - make context manager to yield images form the ring buffer?
        sh_img_data = self.mhandle.read(written_images * s.imagesize)
        import cv2
        import numpy as np
        print(f"Converting images into cv2")
        self.shared_images = []
        # grab total image buffer
        image_buffer = self.mhandle.read(s.imagesize * written_images)
        # convert bytes to numpy array to cv2 images
        for i in range(written_images):
            img = np.frombuffer(image_buffer[i * s.imagesize : (i + 1) * s.imagesize], dtype=np.uint8)
            if img.size == s.imagesize:
                print(f"Image index {i} is of the correct size, reshaping and converting")
                img = img.reshape((HEIGHT, WIDTH, COLORS))
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                self.shared_images.append(img)
            else:
                print(f"Invalid image size: {img.size}")
        # show images
        for img in self.shared_images:
            cv2.imshow("img", img)
            cv2.waitKey(2000)

        self.sd = s._asdict()
        self.td = t._asdict()
        self.vsd = v._asdict()
        self.tsd = ts._asdict()

        self.sd["alarm_cause"] = self.sd["alarm_cause"].split(b"\0", 1)[0].decode()
        self.sd["control_state"] = self.sd["control_state"].split(b"\0", 1)[0].decode()
        self.td["trigger_cause"] = self.td["trigger_cause"].split(b"\0", 1)[0].decode()
        self.td["trigger_text"] = self.td["trigger_text"].split(b"\0", 1)[0].decode()
        self.td["trigger_showtext"] = (
            self.td["trigger_showtext"].split(b"\0", 1)[0].decode()
        )
        return {
            "shared_data": self.sd,
            "trigger_data": self.td,
            "video_store_data": self.vsd,
            "time_stamp_data": self.tsd,
            "shared_images": self.shared_images,
        }

    def close(self):
        """Closes the handle"""
        try:
            if self.mhandle:
                self.mhandle.close()
            if self.fhandle:
                self.fhandle.close()
        except Exception:
            pass


def parse_cli_args():
    """Parse CLI arguments into a dict"""
    global MID, ENV, DB_USER, DB_PASS, DB_HOST, DB_NAME
    ap = ArgumentParser()

    ap.add_argument(
        "-m",
        "--mid",
        "--monitor-id",
        type=int,
        dest="mid",
        help="monitor id - For use by the PERL script (Automatically found)",
    )
    ap.add_argument(
        "-e",
        "--env-file",
        dest="ENV",
        help="environment file to parse"
    )
    ap.add_argument(
        '--db-user',
        dest='DB_USER',
        help='database user',
        type=str,
    )
    ap.add_argument(
        '--db-pass',
        dest='DB_PASS',
        help='database password',
        type=str,
    )
    ap.add_argument(
        '--db-host',
        dest='DB_HOST',
        help='database host',
        type=str,
    )
    ap.add_argument(
        '--db-name',
        dest='DB_NAME',
        help='database name',
        type=str,
    )
    args, u = ap.parse_known_args()

    args = vars(args)
    if args.get("DB_USER"):
        DB_USER = args["DB_USER"]
        print(f'DB_USER passed via CLI, using {DB_USER}')
    if args.get("DB_PASS"):
        DB_PASS = args["DB_PASS"]
        print(f'DB_PASS passed via CLI, using {DB_PASS}')
    if args.get("DB_HOST"):
        DB_HOST = args["DB_HOST"]
        print(f'DB_HOST passed via CLI, using {DB_HOST}')
    if args.get("DB_NAME"):
        DB_NAME = args["DB_NAME"]
        print(f'DB_NAME passed via CLI, using {DB_NAME}')
    _mid_passed = 'passed'
    if args.get("monitor_id"):
        _mid_passed = 'not passed' if args["monitor_id"] is None else f'passed as {args["monitor_id"]} (type {type(args["monitor_id"])}'
    else:
        MID = args["monitor_id"]
    print(f'MONITOR ID {_mid_passed} via CLI, using {MID}')
    _env_passed = 'passed'
    if not args.get("ENV"):
        _env_passed = 'not passed'
    else:
        ENV = args["ENV"]
    print(f'ENVIRONMENT FILE {_env_passed} via CLI, using {ENV}')
    if not args:
        print(f"ERROR-FATAL -> no args!")
        exit(1)
    return args


if __name__ == "__main__":
    print('script started')
    args = parse_cli_args()
    ENV_VARS = dotenv.dotenv_values(ENV, verbose=True)
    print(f'ENVIRONMENT VARIABLES: {ENV_VARS}')
    print(f"About to do DB stuff")
    engine = _db_create()
    print(f"{TIMEVAL_SIZE = }")
    print('doing shm stuff')
    zm_mem = ZMMemory(mid=MID)
